using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using ResourceManager;
using UnityEngine;
using UnityEngine.TestTools;

public class CoinTest
{

    private ResourceBank _resourceBank;
    
    [SetUp]
    public void Setup()
    {
        _resourceBank = new ResourceBank();
        _resourceBank.Init(10, 10);
    }

    [Test]
    public void CoinTestSimplePasses()
    {
        _resourceBank.Coins -= 10;
        Assert.AreEqual(0, _resourceBank.Coins);
    }

    [Test]
    public void HealthTestPasses()
    {
        _resourceBank.PlayerLives.Value -= 5;
        Assert.AreEqual(5, _resourceBank.PlayerLives.Value);
        _resourceBank.PlayerLives.Value += 10;
        Assert.AreEqual(10, _resourceBank.PlayerLives.Value);
    }

    // A UnityTest behaves like a coroutine in Play Mode. In Edit Mode you can use
    // `yield return null;` to skip a frame.
    [UnityTest]
    public IEnumerator CoinTestWithEnumeratorPasses()
    {
        // Use the Assert class to test conditions.
        // Use yield to skip a frame.
        yield return null;
    }
}
