using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using ResourceManager;
using Towers;
using UnityEngine;
using UnityEngine.TestTools;

public class GameplayTest
{
    private Tower _tower;

    [SetUp]
    public void Setup()
    {
        GameObject gameObject = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Towers/SimpleTower"));
        _tower = gameObject.GetComponent<Tower>();
        ResourceBank resourceBank = new ResourceBank();
        resourceBank.Init(10, 10);
    }
    
    [Test]
    public void GameplayTestSimplePasses()
    {
        _tower.Upgrade();
        Assert.AreEqual(1, _tower.Level);
        _tower.Upgrade();
        Assert.AreEqual(1, _tower.Level);
        
    }

    [Test]
    public void UpgradeCostTest()
    {
        var cost = _tower.GetUpgradeCost(0);
        Assert.AreEqual(5, cost);
        cost = _tower.GetUpgradeCost(1);
        Assert.AreEqual(10, cost);
    }

    // A UnityTest behaves like a coroutine in Play Mode. In Edit Mode you can use
    // `yield return null;` to skip a frame.
    [UnityTest]
    public IEnumerator GameplayTestWithEnumeratorPasses()
    {
        // Use the Assert class to test conditions.
        // Use yield to skip a frame.
        yield return null;
    }
}
