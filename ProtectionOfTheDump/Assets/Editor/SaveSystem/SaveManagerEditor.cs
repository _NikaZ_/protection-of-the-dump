﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core;
using SaveSystem;
using UnityEditor;
using UnityEngine;

// ReSharper disable once CheckNamespace
namespace EditorScript.SaveSystem
{
    [CustomEditor(typeof(SaveManager))]
    public class SaveManagerEditor : UnityEditor.Editor
    {
        private SaveManager _saveManager;
        private static bool _isOpen = false;

        private static string _keyForSet = "";

        private static bool _boolValueForSet;
        private static int _intValueForSet;
        private static float _floatValueForSet;
        private static string _stringValueForSet = "";
        
        private static SaveTypeEditor _saveType;
        

        private void OnEnable()
        {
            _saveManager = (SaveManager) target;
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            if (!Application.isPlaying)
            {
                return;
            }

            if (GUILayout.Button("Load"))
            {
                _saveManager.Load();
            }

            if (GUILayout.Button("Save"))
            {
                _saveManager.Save();
            }
            
            GUILayout.Label(_saveManager.FilePath);
            
            EditorGUILayout.Space();
            _saveType = (SaveTypeEditor)EditorGUILayout.EnumPopup("Type", _saveType);
            EditorGUILayout.BeginHorizontal();
            GUILayout.Label("Key value");
            _keyForSet = EditorGUILayout.TextField(_keyForSet);
            

            switch (_saveType)
            {
                case SaveTypeEditor.Bool:
                    _boolValueForSet = EditorGUILayout.Toggle(_boolValueForSet);
                    break;
                case SaveTypeEditor.Integer:
                    _intValueForSet = EditorGUILayout.IntField(_intValueForSet);
                    break;
                case SaveTypeEditor.Float:
                    _floatValueForSet = EditorGUILayout.FloatField(_floatValueForSet);
                    break;
                case SaveTypeEditor.String:
                    _stringValueForSet = EditorGUILayout.TextField(_stringValueForSet);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            EditorGUILayout.EndHorizontal();
            
            EditorGUILayout.BeginHorizontal();
            
            if (GUILayout.Button("Set value"))
            {
                SetValue();
            }
            if (GUILayout.Button("Remove value"))
            {
                if (!string.IsNullOrEmpty(_keyForSet))
                {
                    _saveManager.RemoveValue(_keyForSet);
                }
            }

            EditorGUILayout.EndHorizontal();
            
            _isOpen = EditorGUILayout.Foldout(_isOpen, "Save Data");
            if (!_isOpen)
            {
                return;
            }
            Rect keyRect = EditorGUILayout.GetControlRect();
            keyRect.width = EditorGUIUtility.labelWidth;
            Rect valueRect = keyRect;
            valueRect.width = EditorGUIUtility.currentViewWidth - EditorGUIUtility.labelWidth;
            valueRect.x += EditorGUIUtility.labelWidth;

            GUI.Label(keyRect, "Key");
            GUI.Label(valueRect, "Value");
            EditorGUILayout.Separator();

            PrintData(ref keyRect, ref valueRect, _saveManager.BoolKeys, SaveTypeEditor.Bool);
            PrintData(ref keyRect, ref valueRect, _saveManager.IntKeys, SaveTypeEditor.Integer);
            PrintData(ref keyRect, ref valueRect, _saveManager.FloatKeys, SaveTypeEditor.Float);
            PrintData(ref keyRect, ref valueRect, _saveManager.StringKeys, SaveTypeEditor.String);
        }

        private void PrintData(ref Rect keyRect, ref Rect valueRect, IEnumerable<string> keys, SaveTypeEditor saveType)
        {
            foreach (string key in keys)
            {
                keyRect = EditorGUILayout.GetControlRect();
                keyRect.width = EditorGUIUtility.labelWidth;
                valueRect = keyRect;
                valueRect.width = EditorGUIUtility.currentViewWidth - EditorGUIUtility.labelWidth;
                valueRect.x += EditorGUIUtility.labelWidth;
                
                GUI.Label(keyRect, key);
                string value;
                switch (saveType)
                {
                    case SaveTypeEditor.Bool:
                        value = _saveManager.GetBool(key).ToString();
                        break;
                    case SaveTypeEditor.Integer:
                        value = _saveManager.GetInteger(key).ToString();
                        break;
                    case SaveTypeEditor.Float:
                        value = _saveManager.GetFloat(key).ToString();
                        break;
                    case SaveTypeEditor.String:
                        value = _saveManager.GetString(key);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException(nameof(saveType), saveType, null);
                }
                GUI.Label(valueRect, value);
            }
        }

        private void SetValue()
        {
            if (string.IsNullOrWhiteSpace(_keyForSet))
            {
                return;
            }
            switch (_saveType)
            {
                case SaveTypeEditor.Bool:
                    _saveManager.SetBool(_keyForSet, _boolValueForSet);
                    break;
                case SaveTypeEditor.Integer:
                    _saveManager.SetInteger(_keyForSet, _intValueForSet);
                    break;
                case SaveTypeEditor.Float:
                    _saveManager.SetFloat(_keyForSet, _floatValueForSet);
                    break;
                case SaveTypeEditor.String:
                    if (_stringValueForSet != null)
                    {
                        _saveManager.SetString(_keyForSet, _stringValueForSet);
                    }
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }

    internal enum SaveTypeEditor
    {
        Bool,
        Integer,
        Float,
        String,
    }
}