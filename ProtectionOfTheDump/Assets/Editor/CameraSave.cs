﻿using System.IO;
using UnityEditor;
using UnityEngine;

namespace EditorScript
{
    public static class CameraSave
    {
        
        private static int _fileCounter = 0;

        [MenuItem("Tools/SaveImageFromCamera")]
        static void CamCapture()
        {
            Camera cam = Selection.activeGameObject.GetComponent<Camera>();
 
            RenderTexture currentRT = RenderTexture.active;
            RenderTexture.active = cam.targetTexture;
            
            cam.Render();

            Texture2D image = new Texture2D(cam.targetTexture.width, cam.targetTexture.height);
            image.ReadPixels(new Rect(0, 0, cam.targetTexture.width, cam.targetTexture.height), 0, 0);
            image.Apply();
            RenderTexture.active = currentRT;
 
            var bytes = image.EncodeToPNG();
            Object.DestroyImmediate(image);
 
            Debug.Log("Saving...");
            File.WriteAllBytes(Application.dataPath + "/SaveImages/" + _fileCounter + ".png", bytes);
            Debug.Log("Saved");
            _fileCounter++;
        }
        
        [MenuItem("Tools/SaveImageFromCamera", true)]
        static bool CamCaptureValidate()
        {
            return Selection.activeGameObject != null && Selection.activeGameObject.GetComponent<Camera>();
        }
    }
}