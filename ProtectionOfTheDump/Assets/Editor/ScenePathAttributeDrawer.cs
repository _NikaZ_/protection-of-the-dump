using Attributes;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;


[CustomPropertyDrawer(typeof(ScenePathAttribute))]
public class ScenePathAttributeDrawer : PropertyDrawer
{

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        ScenePathAttribute scenePathAttribute = (ScenePathAttribute) attribute;

        if (property.propertyType != SerializedPropertyType.String)
        {
            EditorGUI.PropertyField(position, property, label);
            return;
        }

        string path = property.stringValue;

        SceneAsset current = AssetDatabase.LoadAssetAtPath<SceneAsset>(path);

        if (scenePathAttribute.InBuildRequire && current != null &&
            SceneUtility.GetBuildIndexByScenePath(path) == -1)
        {
            property.stringValue = "";
            Debug.LogWarning("Field require in build scenes!");
        }

        EditorGUI.BeginChangeCheck();

        SceneAsset newScene =
            EditorGUI.ObjectField(position, label, current, typeof(SceneAsset), false) as SceneAsset;

        if (EditorGUI.EndChangeCheck())
        {
            if (newScene == null)
            {
                property.stringValue = "";
                return;
            }

            string newPath = AssetDatabase.GetAssetPath(newScene);
            if (scenePathAttribute.InBuildRequire)
            {
                if (SceneUtility.GetBuildIndexByScenePath(newPath) >= 0)
                {
                    property.stringValue = newPath;
                }
                else
                {
                    Debug.LogWarning("Field require in build scenes!");
                }
            }
            else
            {
                property.stringValue = newPath;
            }
        }
    }
}
