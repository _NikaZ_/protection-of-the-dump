﻿using System.Linq;
using Enemies.EnemyNavigation;
using UnityEditor;
using UnityEngine;

// ReSharper disable once CheckNamespace
namespace EditorScript
{
    public static class PathLineEffectGenerator
    {
        [MenuItem("Tools/Generate Path Line Effect")]
        public static void Generate()
        {
            GameObject gameObject = Selection.activeGameObject;
            if (gameObject == null)
            {
                return;
            }

            Path path = gameObject.GetComponent<Path>();
            if (path == null)
            {
                return;
            }

            LineRenderer line = new GameObject().AddComponent<LineRenderer>();
            SerializedObject serializedObject = new SerializedObject(path);
            SerializedProperty serializedProperty = serializedObject.FindProperty("_checkPoints");

            line.transform.position = path.transform.position;
            
            int arraySize = serializedProperty.arraySize;
            line.positionCount = arraySize + 1;
            line.transform.LookAt(line.transform.position + -path.transform.up);
            
            for (int i = 0; i < arraySize; i++)
            {
                Vector3 pathVector = serializedProperty.GetArrayElementAtIndex(i).vector3Value;
                Vector3 lineVector = new Vector3(pathVector.x, pathVector.z, pathVector.y);
                line.SetPosition(i+1, lineVector);
            }

            line.useWorldSpace = false;
            line.alignment = LineAlignment.TransformZ;
            line.textureMode = LineTextureMode.Tile;
            line.numCornerVertices = 3;

            Material mat = AssetDatabase.LoadAssetAtPath<Material>("Assets/Materials/Defaults/Arrow.mat");
            line.material = mat;

            line.gameObject.name = GenerateName("Path Line Effect");

            Selection.activeGameObject = line.gameObject;
        }

        private static string GenerateName(string name)
        {
            string generateName = name;
            int counter = 1;
            while (GameObject.Find(generateName))
            {
                generateName = $"{generateName} ({counter})";
            }

            return generateName;
        }

        [MenuItem("Tools/Generate Path Line Effect", true)]
        public static bool ValidateGenerator()
        {
            GameObject gameObject = Selection.activeGameObject;
            if (gameObject == null)
            {
                return false;
            }

            Path path = gameObject.GetComponent<Path>();
            if (path == null)
            {
                return false;
            }

            return true;
        }
    }
}