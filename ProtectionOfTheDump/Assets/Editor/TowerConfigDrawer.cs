using System;
using System.IO;
using System.Reflection;
using Towers;
using UnityEditor;
using UnityEngine;

// ReSharper disable once CheckNamespace
namespace EditorScript
{
    [CustomPropertyDrawer(typeof(TowerConfig))]
    public class TowerConfigDrawer : PropertyDrawer
    {
        private static bool _opened = true;
        
        private readonly string[] _props =
        {
            "_displayName", "_description", "_preview", "_maxLevel", "_damage", "_shootingSpeed",
            "_minShootingDistance", "_shootingDistanceAdditionByLevel", "_cost",
            "_upgradeCost", "_removeCost"
        };
        
        private readonly float _heightMargin = EditorGUIUtility.standardVerticalSpacing;
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            //base.OnGUI(position, property, label);

            #region Rects

            Rect foldoutPosition = position;
            foldoutPosition.height = EditorGUIUtility.singleLineHeight;
            
            Rect propertyPosition = position;
            propertyPosition.width *= 0.8f;
            propertyPosition.height = EditorGUIUtility.singleLineHeight;
            
            Rect buttonPosition = position;
            float margin = 4f;
            buttonPosition.x = buttonPosition.x + propertyPosition.width + margin;
            buttonPosition.width = buttonPosition.width * 0.2f - margin;
            buttonPosition.height = EditorGUIUtility.singleLineHeight;

            #endregion

            #region Layout

            EditorGUI.BeginChangeCheck();
            EditorGUI.PropertyField(propertyPosition, property, label);
            EditorGUI.EndChangeCheck();
            
            if (property.objectReferenceValue != null)
            {
                _opened = EditorGUI.Foldout(foldoutPosition, _opened, "");
            }
            
            if (GUI.Button(buttonPosition, "New"))
            {
                var newTowerConfig = CreateTowerConfig();
                property.objectReferenceValue = newTowerConfig;
            }

            #endregion

            #region PropertiesLayout

            if (!_opened || property.objectReferenceValue == null)
            {
                return;
            }

            EditorGUI.indentLevel++;


            SerializedObject scriptableObject = new SerializedObject(property.objectReferenceValue);
            
            Rect towerPropertyPosition = position;
            
            towerPropertyPosition.height = EditorGUIUtility.singleLineHeight;

            foreach (var prop in _props)
            {
                towerPropertyPosition.y += EditorGUIUtility.singleLineHeight + _heightMargin;
                CreatePropertyField(towerPropertyPosition, scriptableObject, prop);
            }
            
            scriptableObject.ApplyModifiedProperties();
            
            EditorGUI.indentLevel--;

            #endregion

        }

        private static void CreatePropertyField(Rect position, SerializedObject scriptableObject, string propertyName)
        {
            SerializedProperty property = scriptableObject.FindProperty(propertyName);
            EditorGUI.PropertyField(position, property, new GUIContent(property.displayName));
        }

        private static TowerConfig CreateTowerConfig()
        {
            var newTowerConfig = ScriptableObject.CreateInstance<TowerConfig>();
            var currentFolder = GetCurrentFolder();
            string assetPath = Path.Combine(currentFolder, "NewTowerConfig.asset");
            AssetDatabase.CreateAsset(newTowerConfig, assetPath);
            return newTowerConfig;
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            if (!_opened || property.objectReferenceValue == null)
            {
                return base.GetPropertyHeight(property, label);
            }
            return base.GetPropertyHeight(property, label) + (EditorGUIUtility.singleLineHeight + _heightMargin) * _props.Length + 10;
        }

        private static string GetCurrentFolder()
        {
            Type projectWindowUtilType = typeof(ProjectWindowUtil);
            MethodInfo getActiveFolderPath =
                projectWindowUtilType.GetMethod("GetActiveFolderPath", BindingFlags.Static | BindingFlags.NonPublic);
            object obj = getActiveFolderPath.Invoke(null, new object[0]);
            string pathToCurrentFolder = obj.ToString();
            return pathToCurrentFolder;
        }
    }
}
