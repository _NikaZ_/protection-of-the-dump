using System;
using UnityEngine;

namespace _2DEntityUtility
{
    public class Orientation : MonoBehaviour
    {
        private Transform _target;
        

        private void Awake()
        {
            if (Camera.main == null)
            {
                gameObject.SetActive(false);
                return;
            }
            _target = Camera.main.transform;
        }

        void Update()
        {
            transform.eulerAngles = new Vector3(0, -90, 0);
        }
        
        
    }
}
