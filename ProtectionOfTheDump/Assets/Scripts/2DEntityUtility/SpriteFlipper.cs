using System;
using UnityEngine;
using UnityEngine.AI;

namespace _2DEntityUtility
{
    public class SpriteFlipper : MonoBehaviour
    {
        [SerializeField] private SpriteRenderer _spriteRenderer;
        [SerializeField] private NavMeshAgent _navMeshAgent;

        private void Update()
        {
            if (_navMeshAgent.velocity.z > 0.2f)
            {
                _spriteRenderer.flipX = false;
            }
            else if (_navMeshAgent.velocity.z < -0.2f)
            {
                _spriteRenderer.flipX = true;
            }
        }
    }
}
