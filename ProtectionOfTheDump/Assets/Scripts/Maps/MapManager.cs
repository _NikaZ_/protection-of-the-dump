using System;
using System.Collections;
using System.Collections.Generic;
using Attributes;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Maps
{
    public class MapManager : MonoBehaviour
    {
        public event Action OnSceneLoading;

        [SerializeField, ScenePath] private string _loadScenePath;
        [SerializeField, ScenePath] private string _mainMenuScenePath;

        [SerializeField] private List<ScenePath> _scenePaths;

        private int _currentLevel;

        public int CurrentLevel
        {
            get => _currentLevel;
            set
            {
                if (value < 0 || value >= _scenePaths.Count)
                {
                    throw new ArgumentOutOfRangeException();
                }

                _currentLevel = value;
            }
        }

        public int LevelsCount => _scenePaths.Count;

        public void LoadMainMenu()
        {
            if (SceneManager.GetActiveScene() == SceneManager.GetSceneByPath(_mainMenuScenePath))
            {
                return;
            }
            StartCoroutine(LoadScene(_mainMenuScenePath));
        }

        public void LoadCurrentLevel()
        {
            StartCoroutine(LoadScene(_scenePaths[_currentLevel].Path));
        }

        private IEnumerator LoadScene(string path)
        {
            SceneManager.LoadScene(_loadScenePath);
            var sceneLoadOperation = SceneManager.LoadSceneAsync(path);
        
            while (!sceneLoadOperation.isDone)
            {
                OnSceneLoading?.Invoke();
                yield return null;
            }
        }
    }
}
