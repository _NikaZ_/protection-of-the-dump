﻿using System;
using Attributes;
using UnityEngine;

namespace Maps
{
    [Serializable]
    public class ScenePath
    {
        [SerializeField, ScenePath] private string path;

        public string Path => path;
    }
}