using SelectionSystem;
using Towers;
using UI.Controllers;
using UnityEngine;
using Zenject;

namespace Installers
{
    public class GameplayInstaller : MonoInstaller
    {
        [SerializeField] private Selection _selection;
        public override void InstallBindings()
        {
            TowerPlatformsSelectionInstall();
            TowerShopInstall();
        }

        private void TowerPlatformsSelectionInstall()
        {
            Container.BindInstance(_selection).AsSingle();
        }

        private void TowerShopInstall()
        {
            Container.Bind<TowerShopController>().AsSingle();
        }
    }
}