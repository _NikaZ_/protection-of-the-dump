using Maps;
using ResourceManager;
using UnityEngine;
using Zenject;

public class ProjectInstaller : MonoInstaller
{
    public override void InstallBindings()
    {
        Debug.Log("Project Installer Binding");
        ResourceBankInstall();
        MapManagerInstall();
    }

    private void ResourceBankInstall()
    {
        Container.Bind<ResourceBank>().AsSingle();
    }

    private void MapManagerInstall()
    {
        Container.Bind<MapManager>().AsSingle();
    }
}