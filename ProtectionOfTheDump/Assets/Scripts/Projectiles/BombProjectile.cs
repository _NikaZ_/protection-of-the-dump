using System;
using Enemies;
using Towers;
using UnityEngine;

namespace Projectiles
{
    public class BombProjectile : MonoBehaviour
    {
        public event Action<BombProjectile> OnExplode;

        [SerializeField] private Rigidbody _rigidbody;
        [SerializeField] private int _maxTargets;
        [SerializeField] private LayerMask _layerMask;
        [SerializeField] private float _forceHeight = 5;
        [SerializeField] private Vector2 _forceScale = Vector2.one;
        
        private int _damage;
        private float _radius;

        private void OnCollisionEnter(Collision collision)
        {
            _rigidbody.velocity = Vector3.zero;
            _rigidbody.isKinematic = true;

            Collider[] colliders = new Collider[_maxTargets];
            Physics.OverlapSphereNonAlloc(transform.position, _radius, colliders, _layerMask);
            foreach (Collider collider in colliders)
            {
                if (collider != null && collider.TryGetComponent(out ITowerTarget target))
                {
                    target.OnTargetAttacked(_damage);
                }
            }
            
            OnExplode?.Invoke(this);
        }

        public void Init(int damage, Vector3 targetPosition, float radius)
        {
            _damage = damage;
            _radius = radius;
            _rigidbody.isKinematic = false;
            Vector3 direction = targetPosition - transform.position;
            Vector3 force = new Vector3(direction.x * _forceScale.x, _forceHeight, direction.z * _forceScale.y);
            _rigidbody.AddForce(force, ForceMode.Impulse);
        }
    }
}
