﻿using System;
using System.Collections;
using Towers;
using UnityEngine;

namespace Projectiles
{
    public class Projectile : MonoBehaviour
    {
        
        public event Action<Projectile> OnDied;
        [SerializeField] private float _lifetime;
        [SerializeField] private float _speed;

        private int _damage;

        private Coroutine _killAfterCoroutine;
        private void OnEnable()
        {
            _killAfterCoroutine = StartCoroutine(KillAfterRoutine());
        }

        public void Init(int damage)
        {
            _damage = damage;
        }

        private void Update()
        {
            transform.Translate(Vector3.forward * Time.deltaTime * _speed);
        }

        private IEnumerator KillAfterRoutine()
        {
            yield return new WaitForSeconds(_lifetime);
            OnDied?.Invoke(this);
        }
        
        private void OnTriggerEnter(Collider other)
        {
            if (other.TryGetComponent(out ITowerTarget towerTarget))
            {
                towerTarget.OnTargetAttacked(_damage);
            }

            if (_killAfterCoroutine != null)
            {
                StopCoroutine(_killAfterCoroutine);
            }
            OnDied?.Invoke(this);
        }
    }
}