using Core;
using UnityEditor;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace UI
{
    public class UISounds : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerEnterHandler, IPointerExitHandler
    {
        [SerializeField] private string _press;
        [SerializeField] private string _release;
        [SerializeField] private string _hoverIn;
        [SerializeField] private string _hoverOut;

        public void OnPointerDown(PointerEventData eventData)
        {
            if (string.IsNullOrEmpty(_press))
            {
                return;
            }
            Bootstrapper.Instance.AudioManager.Play(_press);
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            if (string.IsNullOrEmpty(_release))
            {
                return;
            }
            Bootstrapper.Instance.AudioManager.Play(_release);
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            if (string.IsNullOrEmpty(_hoverIn))
            {
                return;
            }
            Bootstrapper.Instance.AudioManager.Play(_hoverIn);
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            if (string.IsNullOrEmpty(_hoverOut))
            {
                return;
            }
            Bootstrapper.Instance.AudioManager.Play(_hoverOut);
        }
    }
}
