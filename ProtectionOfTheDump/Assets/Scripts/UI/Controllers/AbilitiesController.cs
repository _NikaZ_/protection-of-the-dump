﻿using Abilities;
using UI.Views.AbilitiesViews;

namespace UI.Controllers
{
    public class AbilitiesController
    {
        public AbilitiesController(AbilitiesView abilitiesView, AbilityManager abilityManager)
        {
            DiscountAbility discountAbility = abilityManager.GetAbility<DiscountAbility>() as DiscountAbility;
            CheeseAbility cheeseAbility = abilityManager.GetAbility<CheeseAbility>() as CheeseAbility;
            EnergyAbility energyAbility = abilityManager.GetAbility<EnergyAbility>() as EnergyAbility;
            abilitiesView.SetAbilities(abilityManager.AbilityConfig, discountAbility, cheeseAbility, energyAbility);
        }
    }
}