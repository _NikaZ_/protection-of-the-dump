﻿using Core;
using UI.Views;

namespace UI.Controllers
{
    public class PauseController
    {
        private readonly PauseView _pauseView;
        private readonly Gameplay _game;

        public PauseController(PauseView pauseView, Gameplay game)
        {
            _pauseView = pauseView;
            _game = game;
            game.OnPauseStateChanged += OnPaused;
            pauseView.OnResume += OnResumeClicked;
            pauseView.OnMainMenuClicked += OnMainMenu;
            pauseView.OnPause += OnPause;
            pauseView.OnRestart += OnRestart;
            OnPaused(game.IsPaused);
        }

        private void OnRestart()
        {
            Bootstrapper.Instance.Game.LoadMap(Bootstrapper.Instance.Game.CurrentLevel);
        }

        private void OnPause()
        {
            if (!_game.IsPaused)
            {
                _game.Pause();
            }
            else
            {
                _game.Resume();
            }
        }

        private void OnMainMenu()
        {
            Bootstrapper.Instance.Game.LoadMainMenu();
        }

        private void OnResumeClicked()
        {
            _game.Resume();
        }

        private void OnPaused(bool isPaused)
        {
            _pauseView.SetPause(isPaused);
        }
    }
}