﻿using Core;
using StateMachineSystem;
using StateMachineSystem.States.GameplayStates;
using UI.Views;

namespace UI.Controllers
{
    public class LoseController
    {
        private readonly LoseView _loseView;

        public LoseController(LoseView loseView, Gameplay gameplay)
        {
            _loseView = loseView;
            Game game = Bootstrapper.Instance.Game;
            loseView.OnMainMenu += () => game.LoadMainMenu();
            loseView.OnReplay += () => game.LoadMap(game.CurrentLevel);
            gameplay.StateMachine.OnStateChanged += OnStateChanged;
        }

        private void OnStateChanged(IStateExitable state)
        {
            if (state is LoseState)
            {
                _loseView.IsActive = true;
            }
        }
    }
}