﻿using BuildingSystem;
using Farming;
using SelectionSystem;
using Towers;
using UI.Views;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Controllers
{
    public class TowerPlatformSettingsController
    {

        private readonly TowerPlatformSettingsView _view;
        private readonly Selection _selection;

        private Platform _selectedPlatform;
        
        public TowerPlatformSettingsController(TowerPlatformSettingsView towerPlatformSettingsView, Selection selection)
        {
            _view = towerPlatformSettingsView;
            _selection = selection;


            selection.OnSelected += OnSelected;
            selection.OnDeselected += OnDeselected;
            _selectedPlatform = selection.SelectedObject as Platform;
            
            
            _view.gameObject.SetActive(_selectedPlatform is { });
            towerPlatformSettingsView.OnCloseButtonClicked += OnCloseClick;
        }

        private void OnDeselected(ISelectable obj)
        {
            _view.gameObject.SetActive(false);
        }

        private void OnSelected(ISelectable obj)
        {
            if (!(obj is Platform platform))
            {
                return;
            }

            _selectedPlatform = platform;
            _view.gameObject.SetActive(true);
            _view.Type = platform.CurrentBuilding == null
                ? TowerPlatformSettingsView.ViewType.Shop
                : TowerPlatformSettingsView.ViewType.Parameters;

            _selectedPlatform.OnBuildingPlaced += OnPlaced;
            _selectedPlatform.OnBuildingRemoved += OnRemoved;
        }

        private void OnRemoved(IRemovableBuilding obj)
        {
            _view.Type = TowerPlatformSettingsView.ViewType.Shop;
        }

        private void OnPlaced(IBuilding building)
        {
            _view.Type = TowerPlatformSettingsView.ViewType.Parameters;
        }

        private void OnCloseClick()
        {
            _selection.Deselect();
            _selectedPlatform = null;
            
        }
    }
}