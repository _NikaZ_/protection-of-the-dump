using System;
using System.Linq;
using BuildingSystem;
using ResourceManager;
using SelectionSystem;
using ShoppingSystem;
using Towers;
using UI.Commands;
using UI.Views;
using UnityEngine;
using Zenject;
using Object = System.Object;

namespace UI.Controllers
{
    public class TowerShopController
    {
        public event Action<bool> OnTowerShopActiveChanged;
        
        private readonly TowerShopView _towerShopView;
        private TowerShop _towerShop;
        private ResourceBank _resourceBank;
        private readonly TowerShop _shop;
        private readonly Selection _selection;

        private bool isActive;

        public bool IsActive
        {
            get => isActive;
            private set
            {
                if (isActive == value)
                {
                    return;
                }
                isActive = value;
                OnTowerShopActiveChanged?.Invoke(value);
            }
        }

        public TowerShopController(TowerShopView towerShopView, ResourceBank resourceBank, TowerShop shop)
        {
            _towerShopView = towerShopView;
            _resourceBank = resourceBank;
            _shop = shop;
            foreach (Tower tower in _resourceBank.Towers.OrderBy((tower) => tower.Cost))
            {
                towerShopView.AddProduct(tower.DisplayName, tower.Description, tower.Preview, tower.Cost, new TowerBuyCommand(this, tower));
            }
            resourceBank.DiscountManager.OnDiscountSet+= OnDiscountSet;
        }

        private void OnDiscountSet(float value)
        {
            _towerShopView.SetDiscount(value);
        }

        public void BuyTower(Tower tower)
        {
            _shop.BuyTower(tower);
        }
    }
}
