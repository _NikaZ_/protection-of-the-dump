﻿using Core;
using ResourceManager;
using StateMachineSystem;
using StateMachineSystem.States.GameplayStates;
using UI.Views;

namespace UI.Controllers
{
    public class WinPanelController
    {
        private readonly WinPanelView _winPanelView;
        private readonly ResourceBank _resourceBank;

        public WinPanelController(WinPanelView winPanelView, Gameplay gameplay, ResourceBank resourceBank)
        {
            _winPanelView = winPanelView;
            _resourceBank = resourceBank;
            gameplay.StateMachine.OnStateChanged += OnStateChanged;
            Game game = Bootstrapper.Instance.Game;
            _winPanelView.NextLevelActive = game.CurrentLevel+1 < game.LevelsCount;
            
            _winPanelView.OnMainMenu += () => game.LoadMainMenu();
            _winPanelView.OnRestart += () => game.LoadMap(game.CurrentLevel);
            _winPanelView.OnNextLevel += () => game.LoadMap(game.CurrentLevel + 1);
        }

        private void OnStateChanged(IStateExitable obj)
        {
            if (!(obj is WinState))
            {
                return;
            }

            int rating = (int) ((float) _resourceBank.PlayerLives.Value / _resourceBank.PlayerLives.MaxValue * 3);
            
            _winPanelView.SetWinPanel(rating);
        }
    }
}