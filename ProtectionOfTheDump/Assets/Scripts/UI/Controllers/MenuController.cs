﻿using Core;
using StateMachineSystem;
using StateMachineSystem.States.MainMenuStates;
using UI.Views;

namespace UI.Controllers
{
    public class MenuController
    {
        private readonly Game _game;
        private readonly MainMenu _mainMenu;
        private readonly MenuView _menuView;

        public MenuController(Game game, MainMenu mainMenu, MenuView menuView)
        {
            _game = game;
            _mainMenu = mainMenu;
            _menuView = menuView;
            _menuView.OnPlayClicked += OnPlay;
            _menuView.OnQuitClicked += OnQuit;
            _menuView.OnOptionsClicked += OnOptionsClicked;
            _mainMenu.StateMachine.OnStateChanged += OnStateChanged;
        }

        private void OnOptionsClicked()
        {
            _mainMenu.OpenOptions();
        }

        private void OnStateChanged(IStateExitable state)
        {
            _menuView.SetActive(state is MenuState);
        }

        private void OnQuit()
        {
            _game.Quit();
        }

        private void OnPlay()
        {
            _mainMenu.OpenLevelSelection();
        }
    }
}