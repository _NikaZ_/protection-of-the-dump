﻿using System;
using Farming;
using ResourceManager;
using Towers;
using UI.Views;
using UnityEngine;

namespace UI.Controllers
{
    public class FarmParametersController
    {
        private readonly FarmParametersView _farmParametersView;

        private Farm _farm;

        public FarmParametersController(FarmParametersView farmParametersView, FarmSelection farmSelection, ResourceBank resourceBank)
        {
            _farmParametersView = farmParametersView;
            _farmParametersView.OnUpgrade += OnUpgradeCall;
            farmSelection.OnFarmSelected += OnFarmSelected;
            farmSelection.OnFarmDeselected += OnFarmDeselected;
            resourceBank.OnCoinsCountChanged += OnCoinsCountChanged;
            resourceBank.DiscountManager.OnDiscountSet += OnDiscountSet;
        }

        private void OnDiscountSet(float obj)
        {
            if (_farm == null)
            {
                return;
            }

            _farmParametersView.UpgradeButtonActive = _farm.CanUpgrade();
            _farmParametersView.SetUpgradeText(_farm.UpgradeCost, _farm.NonDiscountUpgradeCost);
        }

        private void OnCoinsCountChanged(int obj)
        {
            if (_farm == null)
            {
                return;
            }
            _farmParametersView.UpgradeButtonActive = _farm.CanUpgrade();
        }

        private void OnUpgradeCall()
        {
            if (_farm != null)
            {
                _farm.Upgrade();
            }
        }

        private void OnFarmDeselected(Farm farm)
        {
            _farm.Upgraded -= OnUpgrade;
            _farm = null;
        }

        private void OnFarmSelected(Farm farm)
        {
            _farm = farm;
            _farm.Upgraded += OnUpgrade;
            
            _farmParametersView.ShowProperty(farm.CoinsPerWave.ToString());
            _farmParametersView.SetLevelText(farm.Level + 1);
            SetUpgradeText(farm);

            _farmParametersView.UpgradeButtonActive = _farm.CanUpgrade();
            
        }

        private void SetUpgradeText(Farm farm)
        {
            if (farm.Level < farm.MaxLevel)
            {
                _farmParametersView.SetUpgradeText(farm.NonDiscountUpgradeCost, farm.UpgradeCost);
            }
            else
            {
                _farmParametersView.SetMaxUpgradeText();
            }
        }

        private void OnUpgrade(OnUpgradeEventArgs upgradeEventArgs)
        {
            _farmParametersView.ShowProperty(_farm.CoinsPerWave.ToString());
            _farmParametersView.SetLevelText(_farm.Level + 1);
            SetUpgradeText(_farm);
            _farmParametersView.UpgradeButtonActive = _farm.CanUpgrade();
        }
    }
}