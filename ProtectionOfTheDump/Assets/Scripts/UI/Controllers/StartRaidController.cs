﻿using Core;
using StateMachineSystem;
using UI.Views;

namespace UI.Controllers
{
    public class StartRaidController
    {
        private readonly StartRaidView _startRaidView;
        private readonly Gameplay _gameplay;

        public StartRaidController(StartRaidView startRaidView, Gameplay gameplay)
        {
            _startRaidView = startRaidView;
            _gameplay = gameplay;
            startRaidView.OnStartRaid += gameplay.StartRaid;
            gameplay.StateMachine.OnStateChanged += OnStateChanged;
            _startRaidView.Interactable = _gameplay.CanStartRaid();
        }

        private void OnStateChanged(IStateExitable state)
        {
             _startRaidView.Interactable = _gameplay.CanStartRaid();
        }
    }
}