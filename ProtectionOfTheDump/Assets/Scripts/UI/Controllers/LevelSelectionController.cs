﻿using Core;
using StateMachineSystem;
using StateMachineSystem.States.MainMenuStates;
using UI.Views;

namespace UI.Controllers
{
    public class LevelSelectionController
    {
        private readonly LevelSelectionView _levelSelectionView;
        private readonly MainMenu _mainMenu;
        private readonly Game _game;

        public LevelSelectionController(LevelSelectionView levelSelectionView, MainMenu mainMenu, Game game)
        {
            _levelSelectionView = levelSelectionView;
            _mainMenu = mainMenu;
            _game = game;
            _levelSelectionView.SetLevelViews(game);
            mainMenu.StateMachine.OnStateChanged += OnStateChanged;
            _levelSelectionView.OnBackClicked += OnBack;
        }

        private void OnBack()
        {
            _mainMenu.OpenMenu();
        }

        private void OnStateChanged(IStateExitable stateExitable)
        {
            _levelSelectionView.SetActive(stateExitable is LevelSelectionState);
        }
    }
}