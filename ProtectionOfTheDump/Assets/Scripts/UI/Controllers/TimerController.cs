using Core;
using StateMachineSystem;
using StateMachineSystem.States.GameplayStates;
using TimeManagement;
using UI.Views;
using UnityEngine;

namespace UI.Controllers
{
    public class TimerController
    {
        private readonly TimerView _timerView;

        public TimerController(TimerView timerView, Timer timer, Gameplay gameplay)
        {
            _timerView = timerView;
            timer.OnTimeUpdate += OnTimerUpdate;
            gameplay.StateMachine.OnStateChanged += OnStateChanged;
        }

        private void OnStateChanged(IStateExitable state)
        {
            _timerView.SetOpen(state is PrepareState);
        }

        private void OnTimerUpdate(float time)
        {
            _timerView.SetTime(time);
        }
    }
}
