﻿using BuildingSystem;
using GameProperties;
using ResourceManager;
using SelectionSystem;
using ShoppingSystem;
using Towers;
using UI.Views;
using UnityEngine;
using BuildingProperty = Towers.BuildingProperty;

namespace UI.Controllers
{
    public class TowerParametersController
    {
        private readonly TowerParametersView _towerParametersView;
        private readonly Selection _selection;
        private readonly ResourceBank _resourceBank;

        private Platform _selectedPlatform;

        public TowerParametersController(TowerParametersView towerParametersView, Selection selection, ResourceBank resourceBank)
        {
            _towerParametersView = towerParametersView;
            _selection = selection;
            _resourceBank = resourceBank;
            
            _towerParametersView.OnTowerUpgradeRequest += OnUpgradeRequest;
            _towerParametersView.OnTowerRemoveRequest += OnRemoveRequest;

            _selection.OnSelected += OnPlatformSelected;
            _selection.OnDeselected += OnPlatformDeselected;

            _resourceBank.OnCoinsCountChanged += OnCoinsCountChanged;
            _resourceBank.DiscountManager.OnDiscountSet += OnDiscountSet;
        }

        private void UpdateView()
        {
            
        }

        private void OnDiscountSet(float value)
        {
            if (_selectedPlatform == null)
            {
                return;
            }
            IBuilding tower = _selectedPlatform.CurrentBuilding;
            if (tower is IUpgradable upgradable)
            {
                _towerParametersView.UpgradeButtonActive = CanUpgradeBuilding(upgradable);
                    
                if (upgradable.Level < upgradable.MaxLevel)
                {
                    _towerParametersView.SetUpgradeText(upgradable.NonDiscountUpgradeCost, upgradable.UpgradeCost);
                }
            }
            if (tower is IRemovableBuilding removableBuilding)
            {
                _towerParametersView.RemoveButtonActive = CanRemoveBuilding(removableBuilding);
                _towerParametersView.SetRemoveText(removableBuilding.NonDiscountRemoveCost,removableBuilding.RemoveCost);
            }
        }

        private void OnCoinsCountChanged(int value)
        {
            if (_selectedPlatform == null)
            {
                return;
            }
            IBuilding tower = _selectedPlatform.CurrentBuilding;
            if (tower is IUpgradable upgradable)
            {
                _towerParametersView.UpgradeButtonActive = CanUpgradeBuilding(upgradable);
            }
            if (tower is IRemovableBuilding removableBuilding)
            {
                _towerParametersView.RemoveButtonActive = CanRemoveBuilding(removableBuilding);
            }
        }


        private void OnPlatformSelected(ISelectable selectable)
        {
            if (!(selectable is Platform platform))
            {
                return;
            }

            _selectedPlatform = platform;
            
            platform.OnBuildingPlaced += OnTowerPlaced;
            if (platform.CurrentBuilding == null)
            {
                _towerParametersView.gameObject.SetActive(false);
                return;
            }
            _towerParametersView.gameObject.SetActive(true);
            
            OnTowerPlaced(platform.CurrentBuilding);
        }

        public bool CanUpgradeBuilding(IUpgradable upgradable) => _resourceBank.Coins >= upgradable.UpgradeCost && upgradable.CanUpgrade();
        public bool CanRemoveBuilding(IRemovableBuilding removable) => _resourceBank.Coins >= removable.RemoveCost;

        private void OnPlatformDeselected(ISelectable selectable)
        {
            if ((Platform)selectable != _selectedPlatform)
            {
                return;
            }

            _towerParametersView.gameObject.SetActive(false);
            _selectedPlatform.OnBuildingPlaced -= OnTowerPlaced;
            if (_selectedPlatform.CurrentBuilding is IUpgradable upgradable)
            {
                upgradable.Upgraded -= OnLevelChanged;
            }
            _towerParametersView.ClearProperties();
        }

        public void SetUpgradeText(IUpgradable tower)
        {
            if (tower.Level < tower.MaxLevel)
            {
                _towerParametersView.SetUpgradeText(tower.NonDiscountUpgradeCost,tower.UpgradeCost);
            }
            else
            {
                _towerParametersView.SetUpgradeButtonMaxUpgrade();
            }
        }

        private void SetProperties(IBuildingProperties tower)
        {
            _towerParametersView.ClearProperties();
            foreach (BuildingProperty towerProperty in tower.GetBuildingProperties())
            {
                _towerParametersView.SetProperty(towerProperty.Sprite, towerProperty.Message);
            }
        }

        private void OnLevelChanged(OnUpgradeEventArgs value)
        {
            _towerParametersView.UpdateLevel(value.Level);
            if (_selectedPlatform.CurrentBuilding is IUpgradable upgradable)
            {
                SetUpgradeText(upgradable);
            }

            if (_selectedPlatform.CurrentBuilding is IBuildingProperties buildingProperties)
            {
                SetProperties(buildingProperties);
            }
        }

        private void OnRemoveRequest()
        {
            if (_selectedPlatform.CurrentBuilding is IRemovableBuilding removableBuilding 
                && CanRemoveBuilding(removableBuilding) 
                && _selectedPlatform.RemoveBuilding())
            {
                _resourceBank.Coins -= removableBuilding.RemoveCost;
            }
        }

        private void OnUpgradeRequest()
        {
            if (_selectedPlatform.CurrentBuilding is IUpgradable upgradable
                && CanUpgradeBuilding(upgradable))
            {
                upgradable.Upgrade();
            }
        }

        private void OnTowerPlaced(IBuilding tower)
        {
            _towerParametersView.SetTowerParameters(tower);

            if (tower is IUpgradable upgradable)
            {
                _towerParametersView.UpgradeButtonEnabled = true;
                _towerParametersView.UpgradeButtonActive = CanUpgradeBuilding(upgradable);
                upgradable.Upgraded += OnLevelChanged;
                SetUpgradeText(upgradable);
            }
            else
            {
                _towerParametersView.UpgradeButtonEnabled = false;
            }

            

            if (tower is IRemovableBuilding removableBuilding)
            {
                _towerParametersView.RemoveButtonEnabled = true;
                _towerParametersView.RemoveButtonActive = CanRemoveBuilding(removableBuilding);
                _towerParametersView.SetRemoveText(removableBuilding.NonDiscountRemoveCost,removableBuilding.RemoveCost);
            }
            else
            {
                _towerParametersView.RemoveButtonEnabled = false;
            }
            
            _selectedPlatform.OnBuildingRemoved += OnTowerRemoved;
            if (tower is IBuildingProperties buildingProperties)
            {
                SetProperties(buildingProperties);
            }
        }

        private void OnTowerRemoved(IRemovableBuilding tower)
        {
            if (tower is IUpgradable upgradable)
            {
                upgradable.Upgraded -= OnLevelChanged;
            }
        }
    }
}