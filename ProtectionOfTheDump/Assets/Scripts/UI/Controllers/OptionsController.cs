﻿using Core;
using Options;
using StateMachineSystem;
using StateMachineSystem.States.MainMenuStates;
using UI.Views;
using UnityEngine;

namespace UI.Controllers
{
    public class OptionsController
    {
        private readonly OptionsManager _optionsManager;
        private readonly OptionsView _optionsView;
        private readonly MainMenu _mainMenu;

        public OptionsController(OptionsManager optionsManager, OptionsView optionsView, MainMenu mainMenu)
        {
            _optionsManager = optionsManager;
            _optionsView = optionsView;
            _mainMenu = mainMenu;
            optionsView.OnSettingsChanged += OnSettingsChanged;
            optionsView.OnBackClicked += OnBackClicked;
            optionsView.OnAudioSettingsChanged += OnAudioChanged;
            
            optionsView.SetResolutions(optionsManager.ResolutionsPreset);
            optionsView.SetFullScreenList(optionsManager.FullScreenModePreset);
            optionsView.SetResolutionValue(_optionsManager.CurrentResolution);

            optionsView.MasterVolume = _optionsManager.MasterVolume;
            optionsView.FXVolume = _optionsManager.FXVolume;
            optionsView.MusicVolume = _optionsManager.MusicVolume;
            
            mainMenu.StateMachine.OnStateChanged += OnStateChanged;
            OnStateChanged(mainMenu.StateMachine.CurrentState);
        }

        private void OnAudioChanged(AudioChangedEventArgs eventArgs)
        {
            _optionsManager.MasterVolume = eventArgs.Master;
            _optionsManager.FXVolume = eventArgs.FX;
            _optionsManager.MusicVolume = eventArgs.Music;
        }

        private void OnBackClicked()
        {
            _mainMenu.OpenMenu();
            Bootstrapper.Instance.SaveManager.Save();
        }

        private void OnSettingsChanged(SettingsChangedEventArgs eventArgs)
        {
            _optionsManager.SetFullResolution(eventArgs.Resolution, eventArgs.WindowType);
            _optionsManager.MasterVolume = eventArgs.Master;
            _optionsManager.FXVolume = eventArgs.FX;
            _optionsManager.MusicVolume = eventArgs.Music;
            
            Bootstrapper.Instance.SaveManager.Save();
        }

        private void OnStateChanged(IStateExitable obj)
        {
            _optionsView.gameObject.SetActive(obj is OptionsState);
        }
    }
}