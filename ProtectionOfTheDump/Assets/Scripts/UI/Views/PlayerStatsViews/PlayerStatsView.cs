using System;
using ResourceManager;
using UnityEngine;
using Zenject;

namespace UI.Views.PlayerStatsViews
{
    public class PlayerStatsView : MonoBehaviour
    {
        [SerializeField] private CoinsView _coinsView;
        [SerializeField] private LivesView _livesView;
        [SerializeField] private GameObject _discountPanel;

        private ResourceBank _resourceBank;
        
        [Inject]
        public void Construct(ResourceBank resourceBank)
        {
            _resourceBank = resourceBank;
        }
        
        private void Awake()
        {
            _livesView.MaxValue = _resourceBank.PlayerLives.MaxValue;
            _resourceBank.OnCoinsCountChanged += UpdateCoinsView;
            _resourceBank.PlayerLives.OnHealthChanged += UpdateLivesView;
            _resourceBank.DiscountManager.OnDiscountSet += SetDiscountPanel;
            UpdateCoinsView(_resourceBank.Coins);
            UpdateLivesView(_resourceBank.PlayerLives.Value);
            SetDiscountPanel(_resourceBank.DiscountManager.Discount);
        }

        private void SetDiscountPanel(float discount)
        {
            _discountPanel.SetActive(discount > 0);
        }

        private void UpdateLivesView(int value)
        {
            _livesView.Value = value;
        }


        private void UpdateCoinsView(int value)
        {
            _coinsView.Value = value;
        }

        private void OnDestroy()
        {
            _resourceBank.OnCoinsCountChanged -= UpdateCoinsView;
            _resourceBank.PlayerLives.OnHealthChanged -= UpdateLivesView;
            _resourceBank.DiscountManager.OnDiscountSet -= SetDiscountPanel;
        }
    }
}
