using TMPro;
using UnityEngine;

namespace UI.Views.PlayerStatsViews
{
    public class CoinsView : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI _textField;
        private int _value;

        public int Value
        {
            get => _value;
            set
            {
                _value = value;
                _textField.text = $"{value}";
            }
        }
    }
}
