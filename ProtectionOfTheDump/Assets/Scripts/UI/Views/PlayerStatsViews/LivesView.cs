using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Views.PlayerStatsViews
{
    public class LivesView : MonoBehaviour
    {
        [SerializeField] private Color _filledColor;
        [SerializeField] private Color _emptyColor;

        [SerializeField] private Image[] _images;
        private int _value;

        public int MaxValue { get; set; }
        
        public int Value
        {
            get => _value;
            set
            {
                _value = value;
                UpdateImages(value);
            }
        }

        private void UpdateImages(int value)
        {
            for (int i = 0; i < _images.Length; i++)
            {
                _images[i].color = i < _value ? _filledColor : _emptyColor;
            }
        }
    }
}
