﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Views
{
    public class LoseView : MonoBehaviour
    {
        public event Action OnReplay;
        public event Action OnMainMenu;
        
        [SerializeField] private GameObject _panel;
        [SerializeField] private Button _replay;
        [SerializeField] private Button _mainMenu;

        public bool IsActive
        {
            get => _panel.gameObject.activeSelf;
            set => _panel.gameObject.SetActive(value);
        }
        
        private void Awake()
        {
            _replay.onClick.AddListener((() => OnReplay?.Invoke()));
            _mainMenu.onClick.AddListener((() => OnMainMenu?.Invoke()));
        }

        private void OnDestroy()
        {
            _replay.onClick.RemoveAllListeners();
            _mainMenu.onClick.RemoveAllListeners();
        }
    }
}