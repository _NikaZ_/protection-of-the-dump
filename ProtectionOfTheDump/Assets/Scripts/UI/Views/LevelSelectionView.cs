﻿using System;
using Core;
using UI.Commands;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Views
{
    public class LevelSelectionView : MonoBehaviour
    {
        public event Action OnBackClicked;
        
        [SerializeField] private GameObject _levelSelectionPanel;
        [SerializeField] private LevelPanelView[] _levelPanelViews;
        [SerializeField] private Button _backButton;

        private void Awake()
        {
            _backButton.onClick.AddListener(() => OnBackClicked?.Invoke());
        }

        private void OnDestroy()
        {
            _backButton.onClick.RemoveAllListeners();
        }

        public void SetActive(bool isActive) => _levelSelectionPanel.SetActive(isActive);

        public void SetLevelViews(Game game)
        {
            for (var i = 0; i < _levelPanelViews.Length; i++)
            {
                _levelPanelViews[i].SetLevel($"Level {i+1}", Bootstrapper.Instance.SaveManager.GetInteger($"level_{i}", 0), new LevelPlayCommand(i, game));
            }
        }
    }
}