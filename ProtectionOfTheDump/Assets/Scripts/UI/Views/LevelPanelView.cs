using System;
using Core;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Views
{
    public class LevelPanelView : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI _nameField;
        [SerializeField] private RatingView _rating;
        [SerializeField] private Button _playButton;

        private ICommand _onPlayClicked;

        private void Awake()
        {
            _playButton.onClick.AddListener(PlayClicked);
        }

        private void PlayClicked()
        {
            _onPlayClicked?.Execute();
        }

        public void SetLevel(string levelName, int rating, ICommand onPlayClicked)
        {
            _nameField.text = levelName;
            _rating.SetRating(rating);
            _onPlayClicked = onPlayClicked;
        }
    }
}
