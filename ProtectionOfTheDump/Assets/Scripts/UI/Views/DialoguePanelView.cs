﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Views
{
    public class DialoguePanelView : MonoBehaviour
    {
        public event Action<bool> OnSubmit;
        
        [SerializeField] private GameObject _dialoguePanel;
        [SerializeField] private TextMeshProUGUI _message;
        [SerializeField] private Button _noButton;
        [SerializeField] private Button _yesButton;

        private void Awake()
        {
            _noButton.onClick.AddListener(NoClicked);
            _yesButton.onClick.AddListener(YesClicked);
            _dialoguePanel.SetActive(false);
        }

        private void YesClicked()
        {
            Hide();
            OnSubmit?.Invoke(true);
        }

        private void NoClicked()
        {
            Hide();
            OnSubmit?.Invoke(false);
        }

        public void Show()
        {
            _dialoguePanel.SetActive(true);
        }
        public void Show(string message)
        {
            _message.text = message;
            Show();
        }

        public void Hide()
        {
            _dialoguePanel.SetActive(false);
        }
    }
}