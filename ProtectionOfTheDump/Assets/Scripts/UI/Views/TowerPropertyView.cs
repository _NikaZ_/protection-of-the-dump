﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Views
{
    public class TowerPropertyView : MonoBehaviour
    {
        [SerializeField] private Image _image;
        [SerializeField] private TextMeshProUGUI _text;

        private void Awake()
        {
            Hide();
        }

        public void Show(Sprite sprite, string value)
        {
            _image.sprite = sprite;
            _text.text = value;
            gameObject.SetActive(true);
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }
    }
}