using System;
using Core;
using ResourceManager;
using TMPro;
using Towers;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace UI.Views
{
    public class TowerShopButtonView : MonoBehaviour
    {
        [SerializeField] private Button _buyButton;
        [SerializeField] private TextMeshProUGUI _titleField;
        [SerializeField] private TextMeshProUGUI _descriptionField;
        [SerializeField] private TextMeshProUGUI _costField;
        [SerializeField] private Image _iconImage;
        [SerializeField] private int _coinSpriteAtlasId;
        private string _title;
        private string _description;
        private int _cost;
        private Sprite _icon;
        private float _discount;
        private ResourceBank _resourceBank;

        private string _coinTag => $"<sprite={_coinSpriteAtlasId}>";

        public string Title
        {
            get => _title;
            set
            {
                _title = value;
                _titleField.text = value;
            }
        }

        public string Description
        {
            get => _description;
            set
            {
                _description = value;
                _descriptionField.text = value;
            }
        }

        public int Cost
        {
            get => _cost;
            set
            {
                _cost = value;
                UpdateCostText();
            }
        }

        public float Discount
        {
            get => _discount;
            set
            {
                _discount = value;
                UpdateCostText();
            }
        }

        public Sprite Icon
        {
            get => _icon;
            set
            {
                _icon = value;
                _iconImage.sprite = value;
            }
        }

        public ICommand OnBuyCommand { get; set; }

        private void Awake()
        {
            _buyButton.onClick.AddListener(OnBuyButtonClicked);
            _resourceBank.OnCoinsCountChanged += OnCoinsCountChanged;
        }

        public void Init(ResourceBank resourceBank)
        {
            _resourceBank = resourceBank;
            _buyButton.interactable = _resourceBank.Coins >= (int)Mathf.Round(Cost * (1-_discount));
        }

        private void OnCoinsCountChanged(int obj)
        {
            UpdateCostText();
        }

        private void OnDestroy()
        {
            _buyButton.onClick.RemoveListener(OnBuyButtonClicked);
            _resourceBank.OnCoinsCountChanged -= OnCoinsCountChanged;
            
        }

        private void OnBuyButtonClicked()
        {
            OnBuyCommand?.Execute();
        }

        private void UpdateCostText()
        {
            if (_resourceBank != null && _buyButton)
            {
                _buyButton.interactable = _resourceBank.Coins >= (int)Mathf.Round(Cost * (1-_discount));
            }
            _costField.text = _discount == 0 ? $"{Cost}{_coinTag}" : $"{(int)Mathf.Round(Cost * (1-_discount))}{_coinTag}";
        }
    }
}
