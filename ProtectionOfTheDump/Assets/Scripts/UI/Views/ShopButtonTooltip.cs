using System;
using Michsky.UI.ModernUIPack;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;

namespace UI.Views
{
    public class ShopButtonTooltip : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    {
        [SerializeField] private TowerShopButtonView _towerShopButtonView;
        [SerializeField] private GameObject _tooltip;
        [SerializeField] private TextMeshProUGUI _tooltipText;

        private void Awake()
        {
            if (_towerShopButtonView != null)
            {
                _tooltipText.text = _towerShopButtonView.Description;
            }
        }

        private void Update()
        {
            _tooltip.transform.position = Mouse.current.position.ReadValue();
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            if (_tooltip != null)
            {
                _tooltip.gameObject.SetActive(true);
            }
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            if (_tooltip != null)
            {
                _tooltip.gameObject.SetActive(false);
            }
        }
    }
}
