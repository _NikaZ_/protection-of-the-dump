﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Views
{
    public class PauseView : MonoBehaviour
    {
        public event Action OnPause;
        public event Action OnResume;
        public event Action OnMainMenuClicked;

        public event Action OnRestart;
        
        [SerializeField] private GameObject _pausePanel;
        [SerializeField] private Button _resumeButton;
        [SerializeField] private Button _restartButton;
        [SerializeField] private Button _mainMenuButton;
        [SerializeField] private DialoguePanelView _mainMenuSubmit;

        [SerializeField] private Button _pauseButton;

        private void Awake()
        {
            _resumeButton.onClick.AddListener(OnResumeClicked);
            _mainMenuButton.onClick.AddListener(OnMainMenu);
            _restartButton.onClick.AddListener(OnRestartClicked);
            _pauseButton.onClick.AddListener((() => OnPause?.Invoke()));
        }

        private void OnRestartClicked()
        {
            OnRestart?.Invoke();
        }

        private void OnDestroy()
        {
            _resumeButton.onClick.RemoveListener(OnResumeClicked);
            _mainMenuButton.onClick.RemoveAllListeners();
            _pauseButton.onClick.RemoveAllListeners();
        }
        
        private void OnResumeClicked()
        {
            OnResume?.Invoke();
        }

        public void SetPause(bool isPaused)
        {
            _pausePanel.SetActive(isPaused);
            
        }

        private void OnMainMenu()
        {
            
            _mainMenuSubmit.OnSubmit += OnSubmit;
            _mainMenuSubmit.Show();
        }

        private void OnSubmit(bool yesClicked)
        {
            _mainMenuSubmit.OnSubmit -= OnSubmit;
            if (yesClicked)
            {
                OnMainMenuClicked?.Invoke();
            }
        }
    }
}