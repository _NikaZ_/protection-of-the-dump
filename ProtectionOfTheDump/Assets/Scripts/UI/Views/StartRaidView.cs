using System;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Views
{
    public class StartRaidView : MonoBehaviour
    {
        public event Action OnStartRaid; 

        [SerializeField] private Button _startRaidButton;

        public bool Interactable
        {
            get => _startRaidButton.interactable;
            set => _startRaidButton.interactable = value;
        }

        private void Awake()
        {
            _startRaidButton.onClick.AddListener(OnStartRaidButtonClicked);
        }

        private void OnStartRaidButtonClicked()
        {
            OnStartRaid?.Invoke();
        }

        private void OnDestroy()
        {
            _startRaidButton.onClick.RemoveListener(OnStartRaidButtonClicked);
        }
    }
}
