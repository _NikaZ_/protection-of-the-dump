using System.Collections.Generic;
using Core;
using ResourceManager;
using Towers;
using UI.Commands;
using UI.Controllers;
using UnityEngine;
using Zenject;

namespace UI.Views
{
    public class TowerShopView : MonoBehaviour
    {

        private readonly List<TowerShopButtonView> _buttonViews = new List<TowerShopButtonView>();
        [SerializeField] private TowerShopButtonView _towerShopButtonViewPrefab;
        [SerializeField] private Transform _container;

        [Inject] private ResourceBank _resourceBank;
        
        public void AddProduct(string title, string description, Sprite preview, int cost, ICommand onBuyCommand)
        {
            TowerShopButtonView button = Instantiate(_towerShopButtonViewPrefab, _container);
            button.Icon = preview;
            button.Title = title;
            button.Description = description;
            button.Cost = cost;
            button.OnBuyCommand = onBuyCommand;
            button.Init(_resourceBank);
            _buttonViews.Add(button);
        }

        public void SetDiscount(float discount)
        {
            foreach (var buttonView in _buttonViews)
            {
                buttonView.Discount = discount;
            }
        }
    }
}
