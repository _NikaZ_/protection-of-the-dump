﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Views
{
    public class WinPanelView : MonoBehaviour
    {
        public event Action OnNextLevel;
        public event Action OnRestart;
        public event Action OnMainMenu;
        
        [SerializeField] private GameObject _panel;
        [SerializeField] private RatingView _ratingView;

        [SerializeField] private Button _nextLevel;
        [SerializeField] private Button _restart;
        [SerializeField] private Button _mainMenu;

        public bool NextLevelActive
        {
            get => _nextLevel.gameObject.activeSelf;
            set => _nextLevel.gameObject.SetActive(value);
        }
        
        private void Awake()
        {
            _nextLevel.onClick.AddListener(() => OnNextLevel?.Invoke());
            _restart.onClick.AddListener(() => OnRestart?.Invoke());
            _mainMenu.onClick.AddListener(() => OnMainMenu?.Invoke());
        }

        private void OnDestroy()
        {
            _nextLevel.onClick.RemoveAllListeners();
            _restart.onClick.RemoveAllListeners();
            _mainMenu.onClick.RemoveAllListeners();
        }

        public void SetWinPanel(int rating)
        {
            _panel.SetActive(true);
            _ratingView.SetRating(rating);
            
        }
    }
}