﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Michsky.UI.ModernUIPack;
using UnityEngine.UI;

namespace UI.Views
{
    public class OptionsView : MonoBehaviour
    {
        public event Action<SettingsChangedEventArgs> OnSettingsChanged;
        public event Action<AudioChangedEventArgs> OnAudioSettingsChanged;
        public event Action OnBackClicked;

        [SerializeField] private SliderManager _masterSlider;
        [SerializeField] private SliderManager _fxSlider;
        [SerializeField] private SliderManager _musicSlider;

        [SerializeField] private CustomDropdown _resolution;
        [SerializeField] private CustomDropdown _windowType;
        
        [SerializeField] private Button _apply;
        [SerializeField] private Button _back;
        
        [SerializeField] private Sprite _listIcon;

        public float MasterVolume
        {
            get => _masterSlider.mainSlider.value;
            set => _masterSlider.mainSlider.value = value;
        }

        public float FXVolume
        {
            get => _fxSlider.mainSlider.value;
            set => _fxSlider.mainSlider.value = value;
        }

        public float MusicVolume
        {
            get => _musicSlider.mainSlider.value;
            set => _musicSlider.mainSlider.value = value;
        }


        private void Awake()
        {
            _apply.onClick.AddListener(OnApply);
            _back.onClick.AddListener(OnBack);
            
            _masterSlider.mainSlider.onValueChanged.AddListener(OnAudioChanged);
            _fxSlider.mainSlider.onValueChanged.AddListener(OnAudioChanged);
            _musicSlider.mainSlider.onValueChanged.AddListener(OnAudioChanged);
        }

        private void OnAudioChanged(float arg0)
        {
            OnAudioSettingsChanged?.Invoke(new AudioChangedEventArgs(_masterSlider.mainSlider.value, _fxSlider.mainSlider.value, _musicSlider.mainSlider.value));
        }

        private void OnBack()
        {
            OnBackClicked?.Invoke();
        }

        private void OnApply()
        {
            OnSettingsChanged.Invoke(new SettingsChangedEventArgs(_masterSlider.mainSlider.value, _fxSlider.mainSlider.value, _musicSlider.mainSlider.value,_resolution.selectedItemIndex, _windowType.selectedItemIndex));
        }

        public void SetResolutions(IEnumerable<Resolution> resolutions)
        {
            _resolution.ClearItems();
            foreach (var resolution in resolutions)
            {
                _resolution.CreateNewItem($"{resolution.width}x{resolution.height}", _listIcon);
            }
        }

        public void SetFullScreenList(IEnumerable<FullScreenMode> fullScreenModes)
        {
            _windowType.ClearItems();
            foreach (var fullScreenMode in fullScreenModes)
            {
                _windowType.CreateNewItem($"{GetFullScreenModeName(fullScreenMode)}", _listIcon);
            }
        }

        public void SetResolutionValue(int id)
        {
#if !UNITY_WEBGL
            _resolution.ChangeDropdownInfo(id);
#endif
        }

        public string GetFullScreenModeName(FullScreenMode fullScreenMode)
        {
            switch (fullScreenMode)
            {
                case FullScreenMode.ExclusiveFullScreen:
                    return "Borderless fullscreen";
                case FullScreenMode.FullScreenWindow:
                    return "Fullscreen";
                case FullScreenMode.MaximizedWindow:
                    return "Maximized window";
                case FullScreenMode.Windowed:
                    return "Windowed";
                default:
                    throw new ArgumentOutOfRangeException(nameof(fullScreenMode), fullScreenMode, null);
            }
        }

    }

    public class AudioChangedEventArgs : EventArgs
    {
        public float Master { get; private set; }
        public float FX { get; private set; }
        public float Music { get; private set; }

        public AudioChangedEventArgs(float master, float fx, float music)
        {
            Master = master;
            FX = fx;
            Music = music;
        }
    }

    public class SettingsChangedEventArgs : EventArgs
    {
        public float Master { get; private set; }
        public float FX { get; private set; }
        public float Music { get; private set; }
        
        public int Resolution { get; private set; }
        
        public int WindowType { get; private set; }

        public SettingsChangedEventArgs(float master, float fx, float music, int resolution, int windowType)
        {
            Master = master;
            FX = fx;
            Music = music;
            Resolution = resolution;
            WindowType = windowType;
        }
    }
}