﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Views
{
    public class MenuView : MonoBehaviour
    {
        public event Action OnPlayClicked;
        public event Action OnOptionsClicked;
        public event Action OnQuitClicked;
        
        [SerializeField] private GameObject _menuPanel;
        [SerializeField] private Button _playButton;
        [SerializeField] private Button _optionsButton;
        [SerializeField] private Button _quitButton;

        private void Awake()
        {
            _playButton.onClick.AddListener(() => OnPlayClicked?.Invoke());
            _optionsButton.onClick.AddListener(() => OnOptionsClicked?.Invoke());
            _quitButton.onClick.AddListener(() => OnQuitClicked?.Invoke());
        }

        public void SetActive(bool isActive)
        {
            _menuPanel.SetActive(isActive);
        }

        private void OnDestroy()
        {
            _playButton.onClick.RemoveAllListeners();
            _optionsButton.onClick.RemoveAllListeners();
            _quitButton.onClick.RemoveAllListeners();
        }
    }
}