﻿using Abilities;
using UnityEngine;

namespace UI.Views.AbilitiesViews
{
    public class AbilitiesView : MonoBehaviour
    {
        [SerializeField] private AbilityButtonView _discountAbilityButton;
        [SerializeField] private AbilityButtonView _cheeseAbilityButton;
        [SerializeField] private AbilityButtonView _energyAbilityButton;

        public void SetAbilities(AbilityConfig abilityConfig, DiscountAbility discount, CheeseAbility cheese, EnergyAbility energy)
        {
            _discountAbilityButton.SetAbility(discount);
            _discountAbilityButton.SetPreview(abilityConfig.DiscountConfig.Preview);
            _cheeseAbilityButton.SetAbility(cheese);
            _cheeseAbilityButton.SetPreview(abilityConfig.CheeseConfig.Preview);
            _energyAbilityButton.SetAbility(energy);
            _energyAbilityButton.SetPreview(abilityConfig.EnergyConfig.Preview);
        }
    }
}