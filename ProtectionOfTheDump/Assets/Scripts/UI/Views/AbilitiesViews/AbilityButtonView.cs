﻿using System;
using Abilities;
using Core;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Views.AbilitiesViews
{
    public class AbilityButtonView : MonoBehaviour
    {
        [SerializeField] private Button _button;
        [SerializeField] private TextMeshProUGUI _countField;
        [SerializeField] private Image _preview;
        
        private Ability _ability;
        
        public void SetAbility(Ability ability)
        {
            _ability = ability;

            _ability.OnAbilityCountChanged += OnCountChanged;
            _button.onClick.AddListener(OnButtonClick);
            
            OnCountChanged(_ability.Count);
        }

        public void SetPreview(Sprite sprite)
        {
            _preview.sprite = sprite;
        }

        private void Update()
        {
            _button.interactable = _ability.CanUse() && _ability.Count > 0;
        }

        private void OnButtonClick()
        {
            _ability.Use();
        }

        private void OnCountChanged(int count)
        {
            _countField.text = count.ToString();
        }
    }
}