﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Views
{
    public class RatingView : MonoBehaviour
    {
        [SerializeField] private Image[] _stars;
        [SerializeField] private Sprite _emptyStar;
        [SerializeField] private Sprite _fillStar;

        public void SetRating(int rating)
        {
            for (int i = 0; i < _stars.Length; i++)
            {
                _stars[i].sprite = i < rating ? _fillStar : _emptyStar;
            }
        }

    }
}