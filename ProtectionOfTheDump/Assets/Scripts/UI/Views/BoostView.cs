using System;
using Towers;
using UnityEngine;
using UnityEngine.InputSystem;

namespace UI.Views
{
    public class BoostView : MonoBehaviour
    {
        [SerializeField] private TowerBoostGiver _boostGiver;
        [SerializeField] private Transform _image;

        private bool _isMoving;

        private void Awake()
        {
            _boostGiver.OnStartGive += OnStartGive;
            _boostGiver.OnEndGive += OnEndGive;
            _image.gameObject.SetActive(false);
        }

        private void Update()
        {
            if (_isMoving)
            {
                _image.position = Mouse.current.position.ReadValue();
            }
        }

        private void OnEndGive()
        {
            _isMoving = false;
            _image.gameObject.SetActive(false);
        }

        private void OnStartGive()
        {
            _isMoving = true;
            _image.gameObject.SetActive(true);
        }
    }
}
