﻿using System;
using BuildingSystem;
using GameProperties;
using TMPro;
using Towers;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Views
{
    public class TowerParametersView : MonoBehaviour
    {
        public event Action OnTowerUpgradeRequest;
        public event Action OnTowerRemoveRequest;
        
        [SerializeField] private TextMeshProUGUI _nameField;
        [SerializeField] private TextMeshProUGUI _descriptionField;
        [SerializeField] private TextMeshProUGUI _levelField;
        [SerializeField] private Image _iconImage;
        
        [SerializeField] private Button _upgrade;
        [SerializeField] private TextMeshProUGUI _upgradeButtonText;
        [SerializeField] private Button _remove;
        [SerializeField] private TextMeshProUGUI _removeButtonText;

        [SerializeField] private TowerPropertyView[] _towerPropertyViews;
        [SerializeField] private int _coinSpriteAtlasId;

        private string CoinId => $"<sprite={_coinSpriteAtlasId}>";

        public bool UpgradeButtonActive
        {
            get => _upgrade.interactable;
            set => _upgrade.interactable = value;
        }

        public bool UpgradeButtonEnabled
        {
            get => _upgrade.gameObject.activeSelf;
            set => _upgrade.gameObject.SetActive(value);
        }

        public bool RemoveButtonActive
        {
            get => _remove.interactable;
            set => _remove.interactable = value;
        }
        
        public bool RemoveButtonEnabled
        {
            get => _remove.gameObject.activeSelf;
            set => _remove.gameObject.SetActive(value);
        }

        private void Awake()
        {
            if (_upgrade != null)
            {
                _upgrade.onClick.AddListener(OnUpgradeClicked);
            }
            if (_remove != null)
            {
                _remove.onClick.AddListener(OnRemoveClicked);
            }
        }

        public void SetTowerParameters(IBuilding tower)
        {
            _nameField.text = tower.DisplayName;
            _descriptionField.text = tower.Description;
            _iconImage.sprite = tower.Preview;

            if (tower is IUpgradable upgradable)
            {
                SetLevelText(upgradable.Level);
            }
        }

        public void UpdateLevel(int level)
        {
            SetLevelText(level);
        }

        public void SetUpgradeButtonMaxUpgrade()
        {
            _upgradeButtonText.text = "Max upgrade";
        }

        private void SetLevelText(int level)
        {
            _levelField.text = $"Lvl {level+1}";
        }

        public void SetUpgradeText(int cost, int discountCost)
        {
            _upgradeButtonText.text = discountCost == cost ? $"Upgrade -{cost}{CoinId}" : $"Upgrade -{discountCost}{CoinId}";
        }

        public void SetRemoveText(int cost, int discountCost)
        {
            _removeButtonText.text = discountCost == cost ? $"Remove -{cost}{CoinId}" : $"Remove -{discountCost}{CoinId}";
        }

        private void OnDestroy()
        {
            if (_upgrade != null)
            {
                _upgrade.onClick.RemoveListener(OnUpgradeClicked);
            }
            if (_upgrade != null)
            {
                _remove.onClick.RemoveListener(OnRemoveClicked);
            }
        }

        private void OnUpgradeClicked()
        {
            OnTowerUpgradeRequest?.Invoke();
        }
        private void OnRemoveClicked()
        {
            OnTowerRemoveRequest?.Invoke();
        }

        public void SetProperty(Sprite sprite, string message)
        {
            TowerPropertyView towerPropertyView = null;
            foreach (var propertyView in _towerPropertyViews)
            {

                if (!propertyView.gameObject.activeSelf)
                {
                    towerPropertyView = propertyView;
                    break;
                }
            }
            Debug.Log($"Set properties for {towerPropertyView}");
            if (towerPropertyView == null)
            {
                return;
            }
            
            towerPropertyView.Show(sprite, message);
        }

        public void ClearProperties()
        {
            foreach (var towerPropertyView in _towerPropertyViews)
            {
                towerPropertyView.Hide();
            }
        }
    }
}