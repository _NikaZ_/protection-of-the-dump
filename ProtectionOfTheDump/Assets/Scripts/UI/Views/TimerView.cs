using TMPro;
using UnityEngine;

namespace UI.Views
{
    public class TimerView : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI _secondsField;
        [SerializeField] private TextMeshProUGUI _millisecondsField;
        [SerializeField] private Animator _animator;
        private static readonly int Open = Animator.StringToHash("Open");

        public void SetTime(float time)
        {
            int p = (int) time;
            int t = (int) ((time - p) * 100);
            
            _secondsField.text = $"{p:00}.";
            _millisecondsField.text = $"{t:00}";
        }

        public void SetOpen(bool isOpen)
        {
            _animator.SetBool(Open, isOpen);
        }
    }
}
