using System;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Views
{
    public class TowerPlatformSettingsView : MonoBehaviour
    {
        public event Action OnCloseButtonClicked;

        [SerializeField] private TowerShopView _towerShopView;
        [SerializeField] private TowerParametersView _towerParametersView;
        [SerializeField] private Button _closeButton;

        public TowerShopView ShopView => _towerShopView;
        public TowerParametersView ParametersView => _towerParametersView;

        private ViewType _type;
        
        public ViewType Type
        {
            get => _type;
            set
            {
                _type = value;
                UpdateView();
            }
        }

        private void UpdateView()
        {
            _towerShopView.gameObject.SetActive(_type == ViewType.Shop);
            _towerParametersView.gameObject.SetActive(_type == ViewType.Parameters);
        }
        
        public enum ViewType
        {
            Shop,
            Parameters
        }

        private void Awake()
        {
            _closeButton.onClick.AddListener(OnCloseButtonClick);
        }

        private void OnDestroy()
        {
            _closeButton.onClick.RemoveListener(OnCloseButtonClick);
        }

        private void OnCloseButtonClick()
        {
            OnCloseButtonClicked?.Invoke();
        }
    }
}
