﻿using System;
using TMPro;
using Towers;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Views
{
    public class FarmParametersView : MonoBehaviour
    {
        public event Action OnUpgrade;
        
        [SerializeField] private GameObject _panel;
        [SerializeField] private Button _upgrade;
        [SerializeField] private TowerPropertyView _propertyView;
        [SerializeField] private TextMeshProUGUI _levelField;
        [SerializeField] private TextMeshProUGUI _upgradeButtonText;
        [SerializeField] private int _coinSpriteAtlasId;

        private Sprite _coinSprite;

        private string CoinId => $"<sprite={_coinSpriteAtlasId}>";

        public bool UpgradeButtonActive
        {
            get => _upgrade.interactable;
            set => _upgrade.interactable = value;
        }
        
        private void Awake()
        {
            _coinSprite = Resources.Load<TowerPropertySprites>("TowerPropertySprites").Coin;
            _upgrade.onClick.AddListener(() => OnUpgrade?.Invoke());
        }

        private void OnDestroy()
        {
            _upgrade.onClick.RemoveAllListeners();
        }

        public void SetLevelText(int level)
        {
            _levelField.text = $"Lvl {level}";
        }

        public void SetMaxUpgradeText()
        {
            _upgradeButtonText.text = "Max upgrade";
        }
        
        public void SetUpgradeText(int cost, int discountCost)
        {
            _upgradeButtonText.text = discountCost == cost ? $"Upgrade -{cost}{CoinId}" : $"Upgrade -{discountCost}{CoinId}";
        }

        public void ShowProperty(string message)
        {
            _propertyView.Show(_coinSprite, message);
        }

        public void ClearProperties()
        {
            _propertyView.Hide();
        }
    }
}