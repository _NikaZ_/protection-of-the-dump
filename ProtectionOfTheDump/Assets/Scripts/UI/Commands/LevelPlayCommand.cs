﻿using Core;

namespace UI.Commands
{
    public class LevelPlayCommand : ICommand
    {
        private readonly int _id;
        private readonly Game _game;

        public LevelPlayCommand(int id, Game game)
        {
            _id = id;
            _game = game;
        }
        
        public void Execute()
        {
            _game.LoadMap(_id);
        }
    }
}