﻿using Core;
using Towers;
using UI.Controllers;

namespace UI.Commands
{
    public class TowerBuyCommand : ICommand
    {
        private readonly TowerShopController _towerShop;
        private readonly Tower _towerBehaviour;
        
        public TowerBuyCommand(TowerShopController towerShop, Tower towerBehaviour)
        {
            _towerShop = towerShop;
            _towerBehaviour = towerBehaviour;
        }
        
        public void Execute()
        {
            _towerShop.BuyTower(_towerBehaviour);
        }
    }
}