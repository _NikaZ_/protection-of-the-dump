using Core;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace UI
{
    public class ButtonSounds : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerEnterHandler, IPointerExitHandler
    {
        [SerializeField] private Button _button;
        
        [SerializeField] private string _press;
        [SerializeField] private string _release;
        [SerializeField] private string _hoverIn;
        [SerializeField] private string _hoverOut;
        
        public void OnPointerDown(PointerEventData eventData)
        {
            if (string.IsNullOrEmpty(_press) || !_button.interactable)
            {
                return;
            }
            Bootstrapper.Instance.AudioManager.Play(_press);
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            if (string.IsNullOrEmpty(_release) || !_button.interactable)
            {
                return;
            }
            Bootstrapper.Instance.AudioManager.Play(_release);
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            if (string.IsNullOrEmpty(_hoverIn) || !_button.interactable)
            {
                return;
            }
            Bootstrapper.Instance.AudioManager.Play(_hoverIn);
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            if (string.IsNullOrEmpty(_hoverOut) || !_button.interactable)
            {
                return;
            }
            Bootstrapper.Instance.AudioManager.Play(_hoverOut);
        }
    }
}
