﻿using System;
using TMPro;
using UnityEngine;

namespace UI
{
    public class VersionDisplay : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI _versionField;

        private void OnEnable()
        {
            string version = Application.version;
            string buildGUID = Application.buildGUID;
            _versionField.text = $"{version}_{buildGUID}";
        }
    }
}