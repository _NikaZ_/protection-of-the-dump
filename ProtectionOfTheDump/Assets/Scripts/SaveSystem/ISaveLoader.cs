﻿using JetBrains.Annotations;

namespace SaveSystem
{
    public interface ISaveLoader
    {
        public bool IsSaveExists { get; }
        
        public void Save([NotNull] ISaveData saveData);
        [CanBeNull] public ISaveData Load();
    }
}