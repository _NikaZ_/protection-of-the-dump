﻿using System;
using System.Collections.Generic;
using JetBrains.Annotations;
using Newtonsoft.Json;
using UnityEngine;

namespace SaveSystem
{
    public interface ISaveData
    {
        [JsonIgnore]
        IEnumerable<string> Keys { get; }
        [JsonIgnore]
        IEnumerable<string> IntKeys { get; }
        [JsonIgnore]
        IEnumerable<string> StringKeys { get; }
        [JsonIgnore]
        IEnumerable<string> FloatKeys { get; }
        [JsonIgnore]
        IEnumerable<string> BoolKeys { get; }

        int GetInteger(string key, int defaultValue);
        string GetString(string key, string defaultValue);
        float GetFloat(string key, float defaultValue);
        bool GetBool(string key, bool defaultValue);
        
        void SetInteger(string key, int value);
        void SetString(string key, string value);
        void SetFloat(string key, float value);
        void SetBool(string key, bool value);

        bool RemoveValue(string key);
    }
}