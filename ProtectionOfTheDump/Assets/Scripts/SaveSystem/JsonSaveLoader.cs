﻿using System.IO;
using JetBrains.Annotations;
using Newtonsoft.Json;
using UnityEngine;

namespace SaveSystem
{
    public class JsonSaveLoader<T> : ISaveLoader where T : ISaveData
    {
        public bool IsSaveExists => File.Exists(FilePath);

        private static readonly string FileExtension = ".json";
        
        private readonly string _path;
        private readonly string _filename;
        private readonly JsonSerializer _jsonSerializer = new JsonSerializer();
        public string FilePath => Path.Combine(_path, _filename);

        public JsonSaveLoader([NotNull] string filename) : this(Application.persistentDataPath, filename) { }
        
        public JsonSaveLoader([NotNull] string path, [NotNull] string filename)
        {
            _path = path;
            _filename = $"{filename}{FileExtension}";
        }
        
        public void Save(ISaveData saveData)
        {
            using StreamWriter fileStream = File.CreateText(FilePath);
            _jsonSerializer.Serialize(fileStream, saveData);
            fileStream.Close();
        }

        public ISaveData Load()
        {
            if (!IsSaveExists)
            {
                return null;
            }
            using StreamReader fileStream = File.OpenText(FilePath);
            T saveData = (T)_jsonSerializer.Deserialize(fileStream, typeof(T));
            fileStream.Close();
            if (saveData == null)
            {
                throw new CantLoadSaveFileException($"Failed to load save file {FilePath}. Can't deserialize file.");
            }
            return saveData;
        }
    }
}