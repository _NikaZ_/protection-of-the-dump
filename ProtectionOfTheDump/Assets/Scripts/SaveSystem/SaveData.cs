﻿using System;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using Newtonsoft.Json;
using UnityEngine;

namespace SaveSystem
{
    [Serializable]
    public class SaveData : ISaveData
    {
        public Dictionary<string, int> DataInteger;
        public Dictionary<string, string> DataString;
        public Dictionary<string, bool> DataBool;
        public Dictionary<string, float> DataFloat;


        public SaveData()
        {
            DataInteger = new Dictionary<string, int>();
            DataString = new Dictionary<string, string>();
            DataBool = new Dictionary<string, bool>();
            DataFloat = new Dictionary<string, float>();
        }

        [JsonIgnore]
        public IEnumerable<string> Keys =>
            IntKeys.Concat(StringKeys).Concat(FloatKeys).Concat(BoolKeys);

        public IEnumerable<string> IntKeys => DataInteger.Keys;
        public IEnumerable<string> StringKeys => DataString.Keys;
        public IEnumerable<string> FloatKeys => DataFloat.Keys;
        public IEnumerable<string> BoolKeys => DataBool.Keys;
        
        
        public int GetInteger(string key, int defaultValue)
        {
            if (DataInteger.TryGetValue(key, out int value))
            {
                return value;
            }

            return defaultValue;
        }

        public string GetString(string key, string defaultValue)
        {
            if (DataString.TryGetValue(key, out string value))
            {
                return value;
            }

            return defaultValue;
        }

        public float GetFloat(string key, float defaultValue)
        {
            if (DataFloat.TryGetValue(key, out float value))
            {
                return value;
            }

            return defaultValue;
        }

        public bool GetBool(string key, bool defaultValue)
        {
            if (DataBool.TryGetValue(key, out bool value))
            {
                return value;
            }

            return defaultValue;
        }

        public void SetInteger(string key, int value)
        {
            if (Keys.Contains(key))
            {
                DataInteger[key] = value;
                return;
            }
            DataInteger.Add(key, value);
        }

        public void SetString(string key, string value)
        {
            if (Keys.Contains(key))
            {
                DataString[key] = value;
                return;
            }
            DataString.Add(key, value);
        }

        public void SetFloat(string key, float value)
        {
            if (Keys.Contains(key))
            {
                DataFloat[key] = value;
                return;
            }
            DataFloat.Add(key, value);
        }

        public void SetBool(string key, bool value)
        {
            if (Keys.Contains(key))
            {
                DataBool[key] = value;
                return;
            }
            DataBool.Add(key, value);
        }

        public bool RemoveValue(string key)
        {
            if (DataBool.Remove(key))
            {
                return true;
            }

            if (DataInteger.Remove(key))
            {
                return true;
            }

            if (DataFloat.Remove(key))
            {
                return true;
            }
            
            return DataString.Remove(key);
        }
    }
}