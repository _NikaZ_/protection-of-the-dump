﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using JetBrains.Annotations;
using UnityEngine;

namespace SaveSystem
{
    public class FileSaveLoader<T> : ISaveLoader where T : ISaveData
    {
        public bool IsSaveExists => File.Exists(FilePath);

        private static readonly string FileExtension = ".save";
        
        private readonly string _path;
        private readonly string _filename;
        readonly BinaryFormatter _binaryFormatter = new BinaryFormatter();

        public string FilePath => Path.Combine(_path, _filename);

        public FileSaveLoader([NotNull] string filename) : this(Application.persistentDataPath, filename) { }
        
        public FileSaveLoader([NotNull] string path, [NotNull] string filename)
        {
            _path = path;
            _filename = $"{filename}{FileExtension}";
        }
        
        public void Save(ISaveData saveData)
        {
            using FileStream fileStream = File.Create(FilePath);
            _binaryFormatter.Serialize(fileStream, saveData);
            fileStream.Close();
        }

        public ISaveData Load()
        {
            if (!IsSaveExists)
            {
                return null;
            }
            using FileStream fileStream = File.OpenRead(FilePath);
            T saveData = (T)_binaryFormatter.Deserialize(fileStream);
            fileStream.Close();
            if (saveData == null)
            {
                throw new CantLoadSaveFileException($"Failed to load save file {FilePath}. Can't deserialize file.");
            }
            return saveData;
        }
    }
}