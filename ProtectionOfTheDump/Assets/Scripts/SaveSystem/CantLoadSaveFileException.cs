﻿using System.IO;

namespace SaveSystem
{
    class CantLoadSaveFileException : IOException
    {
        public CantLoadSaveFileException() : this("Failed to load save file")
        {
        }

        public CantLoadSaveFileException(string message) : base(message)
        {
        }
    }
}