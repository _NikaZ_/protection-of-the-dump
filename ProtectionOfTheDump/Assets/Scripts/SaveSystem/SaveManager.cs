﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace SaveSystem
{
    public class SaveManager : MonoBehaviour
    {
        public event Action OnDataChanged;
        
        private ISaveData _saveData;
        private JsonSaveLoader<SaveData> _saveLoader;

        public IEnumerable<string> Keys => _saveData.Keys;
        public string FilePath => _saveLoader?.FilePath;

        public void Init()
        {
            _saveLoader = new JsonSaveLoader<SaveData>("save");
            Load();
        }
        
        public void Load()
        {
            try
            {
                _saveData = _saveLoader.Load();
            }
            catch (CantLoadSaveFileException e)
            {
                Debug.Log(e);
            }
            finally
            {
                _saveData ??= new SaveData();
                OnDataChanged?.Invoke();
            }
        }
        
        public void Save()
        {
            _saveLoader.Save(_saveData);
        }


        #region SaveController

        public IEnumerable<string> IntKeys    => _saveData.IntKeys;
        public IEnumerable<string> StringKeys => _saveData.StringKeys;
        public IEnumerable<string> FloatKeys  => _saveData.FloatKeys;
        public IEnumerable<string> BoolKeys   => _saveData.BoolKeys;

        public int GetInteger(string key, int defaultValue = default) => _saveData.GetInteger(key, defaultValue);
        public string GetString(string key, string defaultValue = default) => _saveData.GetString(key, defaultValue);
        public float GetFloat(string key, float defaultValue = default) => _saveData.GetFloat(key, defaultValue);
        public bool GetBool(string key, bool defaultValue = default) => _saveData.GetBool(key, defaultValue);

        public void SetInteger(string key, int value)
        {
            _saveData.SetInteger(key, value);
            OnDataChanged?.Invoke();
        }

        public void SetString(string key, string value)
        {
            _saveData.SetString(key, value);
            OnDataChanged?.Invoke();
        }

        public void SetFloat(string key, float value)
        {
            _saveData.SetFloat(key, value);
            OnDataChanged?.Invoke();
        }

        public void SetBool(string key, bool value)
        {
            _saveData.SetBool(key, value);
            OnDataChanged?.Invoke();
        }

        public bool RemoveValue(string key)
        {
            if (_saveData.RemoveValue(key))
            {
                OnDataChanged?.Invoke();
                return true;
            }

            return false;
        }

        #endregion

    }
}