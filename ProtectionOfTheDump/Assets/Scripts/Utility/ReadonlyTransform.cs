﻿using UnityEngine;

namespace Utility
{
    public readonly struct ReadonlyTransform
    {
        private readonly Transform _transform;
        
        // ReSharper disable once InconsistentNaming
        public Vector3 position => _transform ? _transform.position : Vector3.zero;

        // ReSharper disable once InconsistentNaming
        public Quaternion rotation => _transform ? _transform.rotation : Quaternion.identity;

        // ReSharper disable once InconsistentNaming
        public Vector3 localScale => _transform ? _transform.localScale : Vector3.one;

        // ReSharper disable once InconsistentNaming
        public Vector3 lossyScale => _transform ? _transform.lossyScale : Vector3.one;

        public ReadonlyTransform(Transform transform)
        {
            _transform = transform;
        }

        public void AddChild(Transform child)
        {
            child.SetParent(_transform);
        }
    }
}