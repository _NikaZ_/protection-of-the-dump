﻿using System.Collections.Generic;
using System.Linq;
using Core;
using UnityEngine;
using UnityEngine.Audio;

namespace Options
{
    public class OptionsManager
    {
        private readonly AudioMixer _audioMixer;
        private Resolution[] _resolutionsPreset;

        private FullScreenMode[] _fullScreenModePreset = new[]
        {
            FullScreenMode.Windowed,
            FullScreenMode.ExclusiveFullScreen,
            FullScreenMode.FullScreenWindow
        };

        public OptionsManager(AudioMixer audioMixer)
        {
            _audioMixer = audioMixer;
            _resolutionsPreset = Screen.resolutions;

            int recomend = 0;
            for (var i = 0; i < _resolutionsPreset.Length; i++)
            {
                Resolution resolution = _resolutionsPreset[i];
                if (resolution.height == Screen.currentResolution.height &&
                    resolution.width == Screen.currentResolution.width)
                {
                    recomend = i;
                }
            }

            _currentResolution = Bootstrapper.Instance.SaveManager.GetInteger("resolution", recomend);
            _fullScreenMode = Bootstrapper.Instance.SaveManager.GetInteger("windowType");
            
            MasterVolume = Bootstrapper.Instance.SaveManager.GetFloat("masterVolume", 1);
            FXVolume = Bootstrapper.Instance.SaveManager.GetFloat("fxVolume", 1);
            MusicVolume = Bootstrapper.Instance.SaveManager.GetFloat("musicVolume", 1);
            
            Bootstrapper.Instance.SaveManager.SetInteger("resolution", _currentResolution);
            Bootstrapper.Instance.SaveManager.SetInteger("windowType", _fullScreenMode);
            
            UpdateScreen();
            UpdateMixer();
        }

        public IEnumerable<Resolution> ResolutionsPreset => _resolutionsPreset;

        private int _currentResolution;
        private int _fullScreenMode;
        private float _masterVolume;
        private float _fxVolume;
        private float _musicVolume;

        public FullScreenMode[] FullScreenModePreset => _fullScreenModePreset;

        public int CurrentResolution => _currentResolution;

        public int WindowType => _fullScreenMode;

        public void SwitchResolution(int id)
        {
            _currentResolution = id;
            UpdateScreen();
            Bootstrapper.Instance.SaveManager.SetInteger("resolution", _currentResolution);
        }
        
        public void SetFullResolution(int resolution, int fullscreen)
        {
            _currentResolution = resolution;
            _fullScreenMode = fullscreen;
            UpdateScreen();
            Bootstrapper.Instance.SaveManager.SetInteger("resolution", _currentResolution);
            Bootstrapper.Instance.SaveManager.SetInteger("windowType", _fullScreenMode);
        }

        private void UpdateScreen()
        {
#if !UNITY_WEBGL
            Screen.SetResolution(_resolutionsPreset[_currentResolution].width, _resolutionsPreset[_currentResolution].height, _fullScreenModePreset[_fullScreenMode]);
#endif
        }
        
        public void SwitchWindowType(int id)
        {
            _fullScreenMode = id;
            UpdateScreen();
            Bootstrapper.Instance.SaveManager.SetInteger("windowType", _fullScreenMode);
        }

        public float MasterVolume
        {
            get => _masterVolume;
            set
            {
                _masterVolume = value <= 0 ? 0.0001f : value;
                UpdateMixer();
                Bootstrapper.Instance.SaveManager.SetFloat("masterVolume", _masterVolume);
            }
        }

        public float FXVolume
        {
            get => _fxVolume;
            set
            {
                _fxVolume = value <= 0 ? 0.0001f : value;
                UpdateMixer();
                Bootstrapper.Instance.SaveManager.SetFloat("fxVolume", _fxVolume);
            }
        }

        public float MusicVolume
        {
            get => _musicVolume;
            set
            {
                _musicVolume = value <= 0 ? 0.0001f : value;
                UpdateMixer();
                Bootstrapper.Instance.SaveManager.SetFloat("musicVolume", _musicVolume);
            }
        }

        public void UpdateMixer()
        {
            _audioMixer.SetFloat("MasterVolume", Mathf.Log10(MasterVolume) * 20);
            _audioMixer.SetFloat("FXVolume", Mathf.Log10(FXVolume) * 20);
            _audioMixer.SetFloat("MusicVolume", Mathf.Log10(MusicVolume) * 20);
        }
    }
}