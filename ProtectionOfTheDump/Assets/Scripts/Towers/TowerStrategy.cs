﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Towers
{
    public abstract class TowerStrategy : MonoBehaviour
    {
        public abstract IEnumerator Fire(ITowerTarget towerTarget, int damage);

        public virtual IEnumerable<BuildingProperty> GetTowerProperties()
        {
            return Array.Empty<BuildingProperty>();
        }
    }
}