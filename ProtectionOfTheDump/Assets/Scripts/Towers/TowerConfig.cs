using System.Linq;
using ShoppingSystem;
using UnityEngine;

namespace Towers
{
    [CreateAssetMenu(fileName = "New Tower Config", menuName = "Game Configs/Tower Config")]
    public class TowerConfig : ScriptableObject, IProduct
    {
        [SerializeField] private string _displayName;
        [SerializeField] private string _description;
        [SerializeField] private Sprite _preview;
        [SerializeField] private int _maxLevel;
        [SerializeField] private int[] _damage;
        [SerializeField] private float _shootingSpeed;
        [SerializeField] private float _minShootingDistance;
        [SerializeField] private float _shootingDistanceAdditionByLevel;
        [SerializeField] private int _cost;
        [SerializeField] private int[] _upgradeCost;
        [SerializeField] private int _removeCost;
        [SerializeField] private float _boostScale;

        public string DisplayName => _displayName;

        public string Description => _description;

        public Sprite Preview => _preview;

        public int MaxLevel => _maxLevel;

        public float ShootingSpeed => _shootingSpeed;

        public float MinShootingDistance => _minShootingDistance;

        public float ShootingDistanceAdditionByLevel => _shootingDistanceAdditionByLevel;

        public int Cost => _cost;

        public int[] UpgradeCost => _upgradeCost;

        public int RemoveCost => _removeCost;

        public float BoostScale => _boostScale;

        public int GetUpgradeCost(int level)
        {
            if (_upgradeCost == null || _upgradeCost.Length == 0)
            {
                return 0;
            }

            return level >= _upgradeCost.Length ? _upgradeCost.Last() : _upgradeCost[level];
        }

        public int GetDamage(int level)
        {
            if (_damage == null || _damage.Length == 0)
            {
                return 0;
            }

            return level >= _damage.Length ? _damage.Last() : _damage[level];
        }
    }
}
