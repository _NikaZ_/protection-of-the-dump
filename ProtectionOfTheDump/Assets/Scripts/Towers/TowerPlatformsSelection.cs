﻿using System;
using UnityEngine;

namespace Towers
{
    public class TowerPlatformsSelection
    {
        public event Action<TowerPlatform> OnTowerPlatformSelected;
        public event Action<TowerPlatform> OnTowerPlatformDeselected; 

        public TowerPlatform SelectedPlatform { get; private set; }

        public void Select(TowerPlatform towerPlatform)
        {
            if (SelectedPlatform != null)
            {
                OnTowerPlatformDeselected?.Invoke(SelectedPlatform);
            }
            SelectedPlatform = towerPlatform;
            OnTowerPlatformSelected?.Invoke(towerPlatform);
        }

        public void Deselect()
        {
            if (SelectedPlatform != null)
            {
                OnTowerPlatformDeselected?.Invoke(SelectedPlatform);
                SelectedPlatform = null;
            }
        }
    }
}