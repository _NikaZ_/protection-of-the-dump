using System;
using BuildingSystem;
using SelectionSystem;
using UnityEngine;
using UnityEngine.InputSystem;
using Zenject;

namespace Towers
{
    public class TowerBoostGiver : MonoBehaviour
    {
        public event Action OnStartGive;
        public event Action OnEndGive;
        public event Action OnCancelGive;
        
        [SerializeField] private PlayerInput _playerInput;
        private Selection _selection;
        private float _time;
        
        public bool IsGiving { get; private set; }

        [Inject]
        public void Construct(Selection towerPlatformsSelection)
        {
            _selection = towerPlatformsSelection;
        }

        private void Awake()
        {
            _playerInput.onActionTriggered += OnActionTriggered;
        }

        private void OnActionTriggered(InputAction.CallbackContext context)
        {
            if (!context.performed)
            {
                return;
            }

            if (context.action.name == "Cancel")
            {
                if (!IsGiving)
                {
                    return;
                }
                StopGiving();
                OnCancelGive?.Invoke();
                OnEndGive?.Invoke();
            }
        }

        private void StopGiving()
        {
            _selection.OnSelected -= OnTowerSelected;
            IsGiving = false;
        }

        public void GiveBoost(float time)
        {
            _time = time;
            if (_selection.SelectedObject is Platform { CurrentBuilding: Tower tower })
            {
                GiveBoostForTower(tower);
                return;
            }

            IsGiving = true;
            _selection.OnSelected += OnTowerSelected;
            OnStartGive?.Invoke();
        }

        private void OnTowerSelected(ISelectable towerPlatform)
        {
            if (towerPlatform is Platform {CurrentBuilding: Tower tower})
            {
                GiveBoostForTower(tower);
            }
            else
            {
                OnCancelGive?.Invoke();
            }
            StopGiving();
            OnEndGive?.Invoke();
        }

        private void GiveBoostForTower(Tower tower)
        {
            _selection.Deselect();
            tower.Boost(_time);
        }
    }
}
