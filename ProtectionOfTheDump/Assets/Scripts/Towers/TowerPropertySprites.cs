﻿using UnityEngine;

namespace Towers
{
    [CreateAssetMenu(fileName = "New Tower Property", menuName = "Game Configs/Tower Property Sprites")]
    public class TowerPropertySprites : ScriptableObject
    {
        [SerializeField] private Sprite _damage;
        [SerializeField] private Sprite _speed;
        [SerializeField] private Sprite _distance;
        [SerializeField] private Sprite _coin;

        public Sprite Damage => _damage;

        public Sprite Speed => _speed;

        public Sprite Distance => _distance;

        public Sprite Coin => _coin;
    }
}