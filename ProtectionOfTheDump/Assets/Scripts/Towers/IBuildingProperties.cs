﻿using System.Collections.Generic;

namespace Towers
{
    public interface IBuildingProperties
    {
        public IEnumerable<BuildingProperty> GetBuildingProperties();
    }
}