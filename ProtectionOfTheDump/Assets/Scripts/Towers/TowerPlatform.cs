using System;
using ResourceManager;
using SelectionSystem;
using UnityEngine;
using Zenject;
using Random = UnityEngine.Random;

namespace Towers
{
    public class TowerPlatform : MonoBehaviour, IDeselectionHandler, ISelectionHandler
    {
        public event Action<Tower> OnTowerPlaced;
        public event Action<Tower> OnTowerRemoved; 

        [SerializeField] private Transform _root;
        private Tower _currentTower;
        
        private TowerPlatformsSelection _selection;
        private ResourceBank _resourceBank;

        public Tower CurrentTower => _currentTower;

        [Inject]
        public void Construct(Selection selection, ResourceBank resourceBank)
        {
            //_selection = selection;
            _resourceBank = resourceBank;
        }

        

        public void PlaceTower(Tower tower)
        {
            if (_currentTower != null)
            {
                return;
            }
            Tower instance = Instantiate(tower, _root);
            _currentTower = instance;
            instance.transform.localPosition = Vector3.zero;
            //instance.transform.eulerAngles = new Vector3(0, Random.Range(0, 360), 0);
            OnTowerPlaced?.Invoke(instance);
        }

        public void RemoveTower()
        {
            
            if (_currentTower == null)
            {
                return;
            }

            int removeCost = GetRemoveCost();
            if (_resourceBank.Coins < removeCost)
            {
                return;
            }

            _resourceBank.Coins -= removeCost;

            Tower tower = _currentTower;
            _currentTower = null;
            Destroy(tower.gameObject);
            OnTowerRemoved?.Invoke(tower);
        }

        public int GetRemoveCost()
        {
            return (int)Mathf.Round(_currentTower.RemoveCost * (1 - _resourceBank.DiscountManager.Discount));
        }

        public int GetNonDiscountRemoveCost()
        {
            return _currentTower.RemoveCost;
        }

        public bool CanRemove()
        {
            return _resourceBank.Coins >= GetRemoveCost();
        }

        public void Select()
        {
            _selection.Select(this);
        }

        public void Deselect()
        {
            _selection.Deselect();
        }
    }
}
