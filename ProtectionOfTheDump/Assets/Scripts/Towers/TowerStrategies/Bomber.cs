using System.Collections;
using Projectiles;
using UnityEngine;

namespace Towers.TowerStrategies
{
    public class Bomber : TowerStrategy
    {
        [SerializeField] private BombProjectile _bombProjectile;
        [SerializeField] private PoolManager _projectilePool;
        [SerializeField] private int _poolInitialSize;
        [SerializeField] private float _despawnDelay;
        [SerializeField] private Transform _root;
        [SerializeField] private float _damageRadius;
        void Awake()
        {
            _bombProjectile.gameObject.SetActive(false);
            _projectilePool.warmPool(_bombProjectile.gameObject, _poolInitialSize);
        }

        public override IEnumerator Fire(ITowerTarget towerTarget, int damage)
        {
            BombProjectile bombProjectile = _projectilePool.spawnObject(_bombProjectile.gameObject).GetComponent<BombProjectile>();
            bombProjectile.gameObject.SetActive(true);
            bombProjectile.transform.position = _root.transform.position;
            bombProjectile.Init(damage, towerTarget.transform.position, _damageRadius);
            bombProjectile.OnExplode += OnExplode;
            yield return null;
        }

        private void OnExplode(BombProjectile projectile)
        {
            projectile.OnExplode -= OnExplode;
            Coroutine despawn = StartCoroutine(DespawnDelay(projectile));
        }

        IEnumerator DespawnDelay(BombProjectile projectile)
        {
            yield return new WaitForSeconds(_despawnDelay);
            projectile.gameObject.SetActive(false);
            _projectilePool.releaseObject(projectile.gameObject);
        }
    }
}
