﻿using System;
using System.Collections;
using Projectiles;
using UnityEngine;

namespace Towers.TowerStrategies
{
    public class Cannon : TowerStrategy
    {
        [SerializeField] private Projectile _projectile;
        [SerializeField] private PoolManager _projectilePool;
        [SerializeField] private Transform _root;

        private void Awake()
        {
            _projectile.gameObject.SetActive(false);
            _projectilePool.warmPool(_projectile.gameObject, 3);
        }

        public override IEnumerator Fire(ITowerTarget towerTarget, int damage)
        {
            Projectile projectile = _projectilePool.spawnObject(_projectile.gameObject).GetComponent<Projectile>();
            projectile.Init(damage);
            projectile.transform.position = _root.transform.position;
            projectile.transform.LookAt(towerTarget.transform.position, Vector3.up);
            projectile.gameObject.SetActive(true);
            projectile.OnDied += OnProjectileDied;
            yield return null;
        }

        private void OnProjectileDied(Projectile projectile)
        {
            projectile.OnDied -= OnProjectileDied;
            _projectilePool.releaseObject(projectile.gameObject);
        }

        
    }
}