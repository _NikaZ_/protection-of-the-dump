using System;
using System.Collections;
using Projectiles;
using UnityEngine;

namespace Towers.TowerStrategies
{
    public class Sniper : TowerStrategy
    {
        [SerializeField] private Projectile _projectile;
        [SerializeField] private PoolManager _projectilePool;
        [SerializeField] private int _poolInitialSize;
        [SerializeField] private float _aimTime = 2f;
        [SerializeField] private Transform _root;

        private void Awake()
        {
            _projectile.gameObject.SetActive(false);
            _projectilePool.warmPool(_projectile.gameObject, _poolInitialSize);
        }

        public override IEnumerator Fire(ITowerTarget towerTarget, int damage)
        {
            Projectile projectile = _projectilePool.spawnObject(_projectile.gameObject).GetComponent<Projectile>();
            projectile.Init(damage);
            projectile.transform.position = _root.transform.position;
            projectile.transform.LookAt(towerTarget.transform.position, Vector3.up);

            yield return AimRoutine(towerTarget, projectile);
        }

        private IEnumerator AimRoutine(ITowerTarget target, Projectile projectile)
        {
            yield return new WaitForSeconds(_aimTime);
            if (!target.IsDied)
            {
                projectile.gameObject.SetActive(true);
                projectile.OnDied += OnDied;
            }
        }

        private void OnDied(Projectile projectile)
        {
            projectile.OnDied -= OnDied;
            _projectilePool.releaseObject(projectile.gameObject);
        }
    }
}
