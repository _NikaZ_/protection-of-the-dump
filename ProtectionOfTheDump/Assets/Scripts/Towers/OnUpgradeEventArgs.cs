﻿using System;

namespace Towers
{
    public class OnUpgradeEventArgs : EventArgs
    {
        private readonly int _level;
        private readonly int _cost;
        private readonly int _nextCost;
        private readonly int _maxLevel;

        public int Level => _level;

        public int Cost => _cost;

        public int NextCost => _nextCost;

        public int MaxLevel => _maxLevel;
        
        public bool IsLast { get; private set; }

        public OnUpgradeEventArgs(int level, int cost, int nextCost, int maxLevel)
        {
            _level = level;
            _cost = cost;
            _nextCost = nextCost;
            _maxLevel = maxLevel;
            IsLast = Level >= MaxLevel;
        }
    }
}