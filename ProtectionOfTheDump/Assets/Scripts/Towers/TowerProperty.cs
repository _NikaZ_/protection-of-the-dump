﻿using UnityEngine;

namespace Towers
{
    public class BuildingProperty
    {
        public Sprite Sprite;
        public string Message;
    }
}