using UnityEngine;

namespace Towers
{
    public class TowerModelSwitcher : MonoBehaviour
    {
        [SerializeField] private Tower _tower;
        [SerializeField] private GameObject[] _towerModels;

        private GameObject _currentModel;

        private void Awake()
        {
            _tower.Upgraded += OnTowerUpgrade;
            foreach (var towerModel in _towerModels)
            {
                towerModel.SetActive(false);
            }
            SetTowerModel(_tower.Level);
        }

        private void OnTowerUpgrade(OnUpgradeEventArgs obj)
        {
            SetTowerModel(obj.Level);
        }

        private void SetTowerModel(int id)
        {
            if (_currentModel != null)
            {
                _currentModel.SetActive(false);
            }
        
            _currentModel = _towerModels.Length > id ? _towerModels[id] : null;
        
            if (_currentModel != null)
            {
                _currentModel.SetActive(true);
            }
        
        }
    }
}
