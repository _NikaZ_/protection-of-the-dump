﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Towers
{
    public class TargetRadar : MonoBehaviour
    {
        private List<ITowerTarget> _towerTargets = new List<ITowerTarget>();
        [SerializeField] private SphereCollider _trigger;

        public float RadarRadius
        {
            get => _trigger.radius;
            set => _trigger.radius = value;
        }
        
        public ITowerTarget GetFirst()
        {
            UpdateTargetsList();
            if (_towerTargets.Count == 0)
            {
                return null;
            }
            return _towerTargets.Aggregate((nearestTarget, nextTarget) =>
                Vector3.Distance(nextTarget.transform.position, transform.position) < Vector3.Distance(nearestTarget.transform.position, transform.position) ? nextTarget : nearestTarget);
        }

        private void UpdateTargetsList()
        {
            for (int i = 0; i < _towerTargets.Count; i++)
            {
                if (_towerTargets[i].IsDied)
                {
                    _towerTargets.RemoveAt(i);
                }
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.TryGetComponent(out ITowerTarget towerTarget))
            {
                _towerTargets.Add(towerTarget);
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (other.TryGetComponent(out ITowerTarget towerTarget))
            {
                _towerTargets.Remove(towerTarget);
            }
        }
    }
}