﻿using UnityEngine;

namespace Towers
{
    public interface ITowerTarget
    {
        // ReSharper disable once InconsistentNaming
        Transform transform { get; }
        void OnTargetAttacked(int damage);
        bool IsDied { get; }
    }
}