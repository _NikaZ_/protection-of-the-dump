﻿using System;
using ResourceManager;
using SelectionSystem;
using UnityEngine;
using Zenject;

namespace Towers
{
    public class TowersDebug : MonoBehaviour
    {
        private ResourceBank _resourceBank;
        private TowerPlatformsSelection _towerPlatformsSelection;
        
        [Inject]
        public void Construct(ResourceBank resourceBank, Selection towerPlatformsSelection)
        {
            _resourceBank = resourceBank;
            _towerPlatformsSelection.OnTowerPlatformSelected += OnPlatformSelected;
        }

        private void OnPlatformSelected(TowerPlatform towerPlatform)
        {
            towerPlatform.PlaceTower(_resourceBank.Towers[0]);
        }

        private void Awake()
        {
            Tower[] towers = _resourceBank.Towers;

            foreach (var tower in towers)
            {
                Debug.Log(tower.name);
            }
        }
    }
}