﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using BuildingSystem;
using GameProperties;
using ResourceManager;
using UnityEngine;
using Utility;
using Zenject;

namespace Towers
{
    public class Tower : MonoBehaviour, IUpgradable, IRemovableBuilding, IBuildingProperties
    {
        public event Action<OnUpgradeEventArgs> Upgraded;

        public event Action OnBoostStart;
        public event Action OnBoostEnd;
        
        private Level _level;
        [SerializeField] private TowerConfig _towerConfig;
        
        [SerializeField] private TargetRadar _targetRadar;
        [SerializeField] private TowerStrategy _towerStrategy;
        private ResourceBank _resourceBank;
        private float _boostScale = 1;
        private TowerPropertySprites _towerPropertySprites;

        public TowerConfig TowerConfig => _towerConfig;

        public string DisplayName => _towerConfig.DisplayName;
        public string Description => _towerConfig.Description;
        public Sprite Preview => _towerConfig.Preview;



        public int Damage => _towerConfig.GetDamage(Level);

        public float Speed => _towerConfig.ShootingSpeed;
        public float ShootingDistance => _targetRadar.RadarRadius;

        public int Cost => _towerConfig.Cost;

        public int RemoveCost => (int)Mathf.Round(NonDiscountRemoveCost * (1 - _resourceBank.DiscountManager.Discount));
        public int NonDiscountRemoveCost => _towerConfig.RemoveCost;

        public int Level => _level.Value;
        public int MaxLevel => _level.MaxValue;
        

        #region Upgrade

        public int UpgradeCost
        {
            get => GetUpgradeCost(_level.Value);
        }
        public int NonDiscountUpgradeCost
        {
            get => GetNonDiscountUpgradeCost(Level);
        }
        public int GetNonDiscountUpgradeCost(int level)
        {
            return _towerConfig.GetUpgradeCost(level);
        }
        public int GetUpgradeCost(int level) => (int)Mathf.Round(GetNonDiscountUpgradeCost(level) * (1 - _resourceBank.DiscountManager.Discount));

        public bool CanUpgrade()
        {
            return _level.Value < _level.MaxValue;
        }
        
        public bool Upgrade()
        {
            if (CanUpgrade())
            {
                _resourceBank.Coins -= UpgradeCost;
                _level.Upgrade();
                _resourceBank.DiscountManager.SetDiscount(0);
                Upgraded?.Invoke(new OnUpgradeEventArgs(_level.Value, GetUpgradeCost(_level.Value-1), UpgradeCost, _level.MaxValue));
                return true;
            }

            return false;
        }

        #endregion

        [Inject]
        public void Construct(ResourceBank resourceBank)
        {
            _resourceBank = resourceBank;
        }

        private void Awake()
        {
            _towerPropertySprites = Resources.Load<TowerPropertySprites>("TowerPropertySprites");
            _level = new Level(_towerConfig.MaxLevel);
            _level.OnLevelChanged += OnLevelChanged;
            UpdateRadius(_level.Value);
            StartCoroutine(ShootingRoutine());
        }

        private void OnLevelChanged(int level)
        {
            UpdateRadius(level);
        }

        private void UpdateRadius(int level)
        {
            _targetRadar.RadarRadius = _towerConfig.MinShootingDistance + _towerConfig.ShootingDistanceAdditionByLevel * level;
        }

        private void OnDestroy()
        {
            _level.OnLevelChanged -= OnLevelChanged;
        }

        IEnumerator ShootingRoutine()
        {
            while (true)
            {
                yield return new WaitForSeconds(1 / (_towerConfig.ShootingSpeed * _boostScale));
                ITowerTarget target = _targetRadar.GetFirst();
                if (target != null)
                {
                    yield return _towerStrategy.Fire(target, Damage);
                }
            }
        }

        public void Boost(float time)
        {
            StartCoroutine(BoostRoutine(time));
        }

        IEnumerator BoostRoutine(float time)
        {
            _boostScale = _towerConfig.BoostScale;
            OnBoostStart?.Invoke();
            yield return new WaitForSeconds(time);
            _boostScale = 1;
            OnBoostEnd?.Invoke();
        }

        public IEnumerable<BuildingProperty> GetBuildingProperties()
        {
            BuildingProperty[] properties = new[]
            {
                new BuildingProperty() {Sprite = _towerPropertySprites.Damage, Message = Damage.ToString()},
                new BuildingProperty() {Sprite = _towerPropertySprites.Speed, Message = $"{Speed:F2}"},
                new BuildingProperty() {Sprite = _towerPropertySprites.Distance,Message = $"{ShootingDistance:F2}"}
            };

            return properties.Concat(_towerStrategy.GetTowerProperties());
        }

        public void OnBuild(ReadonlyTransform origin, ResourceBank resourceBank)
        {
            origin.AddChild(transform);
            var transform1 = transform;
            transform1.localPosition = Vector3.zero;
            transform1.localRotation = Quaternion.identity;
            transform1.localScale = Vector3.one;
            _resourceBank = resourceBank;
        }

        public void Remove()
        {
            Destroy(gameObject);
        }
    }
}