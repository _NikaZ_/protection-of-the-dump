using System;
using BuildingSystem;
using SelectionSystem;
using Towers;
using UnityEngine;
using Zenject;

namespace VFX
{
    public class PlatformSelectionFX : MonoBehaviour, IHoverIn, IHoverOut
    {
        [SerializeField] private Platform _towerPlatform;
        [SerializeField] private GameObject _selection;
        [SerializeField] private GameObject _hover;

        [Inject, HideInInspector] public Selection _platformsSelection;

        private void Awake()
        {
            _platformsSelection.OnSelected += OnPlatformSelected;
            _platformsSelection.OnDeselected += OnPlatformDeselected;
        }

        private void OnPlatformDeselected(ISelectable selectable)
        {
            _selection.gameObject.SetActive(false);
        }

        private void OnPlatformSelected(ISelectable selectable)
        {
            if (selectable is Platform platform && _towerPlatform == platform)
            {
                _selection.gameObject.SetActive(true);
            }
        }


        public void HoverIn()
        {
            _hover.gameObject.SetActive(true);
        }

        public void HoverOut()
        {
            _hover.gameObject.SetActive(false);
        }
    }
}
