﻿using Core;
using Farming;
using Towers;
using UnityEngine;

namespace VFX
{
    public class FarmFX : MonoBehaviour
    {
        [SerializeField] private Farm _tower;
        [SerializeField] private ParticleSystem _particleSystem;
        private void Awake()
        {
            _tower.Upgraded += OnUpgraded;
        }

        private void OnUpgraded(OnUpgradeEventArgs obj)
        {
            Bootstrapper.Instance.AudioManager.Play("upgrade");
            _particleSystem.Play();
        }

        private void OnDestroy()
        {
            _tower.Upgraded -= OnUpgraded;
        }
    }
}