using System;
using Cinemachine;
using Projectiles;
using UnityEngine;

namespace VFX
{
    public class BombProjectileVFX : MonoBehaviour
    {
        [SerializeField] private BombProjectile _bombProjectile;
        [SerializeField] private ParticleSystem _explosionParticleSystem;
        [SerializeField] private ParticleSystem _smokeTrailParticleSystem;
        [SerializeField] private CinemachineImpulseSource _explosionImpulseSource;
        [SerializeField] private ParticleSystem _popcorn;

        private void Awake()
        {
            _bombProjectile.OnExplode += OnExplode;
        }

        private void OnEnable()
        {
            _popcorn.gameObject.SetActive(true);
        }

        private void OnExplode(BombProjectile bombProjectile)
        {
            _explosionImpulseSource.GenerateImpulse();
            _explosionParticleSystem.Play();
            _smokeTrailParticleSystem.Stop();
            _popcorn.gameObject.SetActive(false);
        }
    }
}
