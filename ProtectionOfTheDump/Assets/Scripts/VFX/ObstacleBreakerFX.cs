using System;
using Obstacles;
using UnityEngine;

namespace VFX
{
    public class ObstacleBreakerFX : MonoBehaviour
    {
        [SerializeField] private ObstacleBreaker _obstacleBreaker;
        [SerializeField] private ParticleSystem _particleSystem;
        [SerializeField] private Animator _animator;
        private static readonly int Eat = Animator.StringToHash("Eat");

        private void Awake()
        {
            _obstacleBreaker.OnStartBreaking += OnStartBreaking;
            _obstacleBreaker.OnEndBreaking += OnEndBreaking;
        }

        private void OnDestroy()
        {
            _obstacleBreaker.OnStartBreaking -= OnStartBreaking;
            _obstacleBreaker.OnEndBreaking -= OnEndBreaking;
        }

        private void OnEndBreaking()
        {
            _particleSystem.Stop();
            _animator.SetBool(Eat, false);
        }

        private void OnStartBreaking()
        {
            _particleSystem.Play();
            _animator.SetBool(Eat, true);
        }
    }
}
