﻿using System;
using Core;
using Towers;
using UnityEngine;

namespace VFX
{
    public class TowerFX : MonoBehaviour
    {
        [SerializeField] private Tower _tower;
        [SerializeField] private ParticleSystem _particleSystem;
        [SerializeField] private ParticleSystem _boostParticles;
        private void Awake()
        {
            _tower.Upgraded += OnUpgraded;
            _tower.OnBoostStart += OnBoostStart;
            _tower.OnBoostEnd += OnBoostEnd;
            Bootstrapper.Instance.AudioManager.Play("build");
            _particleSystem.Play();
        }

        private void OnBoostEnd()
        {
            _boostParticles.Stop();
        }

        private void OnBoostStart()
        {
            _boostParticles.Play();
        }


        private void OnUpgraded(OnUpgradeEventArgs obj)
        {
            Bootstrapper.Instance.AudioManager.Play("upgrade");
            _particleSystem.Play();
        }

        private void OnDestroy()
        {
            _tower.Upgraded -= OnUpgraded;
            _tower.OnBoostStart -= OnBoostStart;
            _tower.OnBoostEnd -= OnBoostEnd;
            //Bootstrapper.Instance.AudioManager.Play("destroy");
        }
    }
}