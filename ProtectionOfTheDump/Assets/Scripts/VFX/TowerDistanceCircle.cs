using System;
using BuildingSystem;
using SelectionSystem;
using Towers;
using UnityEngine;
using Zenject;

namespace VFX
{
    public class TowerDistanceCircle : MonoBehaviour
    {
        [SerializeField] private Platform _towerPlatform;
        [SerializeField] private GameObject _circle;
        private Selection _platformsSelection;

        [Inject]
        public void Construct(Selection towerPlatformsSelection)
        {
            _platformsSelection = towerPlatformsSelection;
        }
        
        private void Awake()
        {
            _platformsSelection.OnSelected += OnPlatformSelected;
            _platformsSelection.OnDeselected += OnPlatformDeselected;
            _towerPlatform.OnBuildingPlaced += TowerPlaced;
            _towerPlatform.OnBuildingRemoved += TowerRemoved;
        }

        private void OnDestroy()
        {
            _platformsSelection.OnSelected -= OnPlatformSelected;
            _platformsSelection.OnDeselected -= OnPlatformDeselected;
            _towerPlatform.OnBuildingPlaced -= TowerPlaced;
            _towerPlatform.OnBuildingRemoved -= TowerRemoved;
        }

        private void TowerRemoved(IBuilding building)
        {
            if (!(building is Tower tower))
            {
                return;
            }
            _circle.gameObject.SetActive(false);
            tower.Upgraded -= OnUpgraded;
        }

        private void TowerPlaced(IBuilding building)
        {
            if (!(building is Tower tower))
            {
                return;
            }
            if (ReferenceEquals(_platformsSelection.SelectedObject, _towerPlatform))
            {
                float shootingDistance = tower.ShootingDistance;
                _circle.transform.localScale = Vector3.one * shootingDistance;
                _circle.SetActive(true);
                tower.Upgraded += OnUpgraded;
            }
        }
        
        

        private void OnPlatformSelected(ISelectable selectable)
        {
            if (!(selectable is Platform platform))
            {
                return;
            }
            
            if (platform != _towerPlatform)
            {
                return;
            }

            if (!(platform.CurrentBuilding is Tower tower))
            {
                return;
            }

            float shootingDistance = tower.ShootingDistance;

            _circle.transform.localScale = Vector3.one * shootingDistance;
            _circle.gameObject.SetActive(true);
            tower.Upgraded += OnUpgraded;
        }

        private void OnUpgraded(OnUpgradeEventArgs upgradeEventArgs)
        {
            if (!(_towerPlatform.CurrentBuilding is Tower tower))
            {
                return;
            }
            float shootingDistance = tower.ShootingDistance;
            _circle.transform.localScale = Vector3.one * shootingDistance;
        }

        private void OnPlatformDeselected(ISelectable selectable)
        {
            _circle.SetActive(false);
            if (!(_towerPlatform.CurrentBuilding is Tower tower))
            {
                return;
            }
            tower.Upgraded -= OnUpgraded;
            
        }

        
    }
}
