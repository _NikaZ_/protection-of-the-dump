﻿using System;
using UnityEngine;

namespace Abilities.Configs
{
    [Serializable]
    public class DiscountAbilityParameter : AbilityConfigParameter
    {
        [SerializeField] [Range(0f, 1f)] private float _discountAmount;
        
        public float DiscountAmount => _discountAmount;
    }
}