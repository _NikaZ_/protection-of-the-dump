﻿using System;
using UnityEngine;

namespace Abilities.Configs
{
    [Serializable]
    public class EnergyAbilityParameter : AbilityConfigParameter
    {
        [SerializeField] private float _boostTime;

        public float BoostTime => _boostTime;
    }
}