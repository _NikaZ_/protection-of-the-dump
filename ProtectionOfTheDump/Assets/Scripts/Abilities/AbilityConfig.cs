﻿using Abilities.Configs;
using UnityEngine;

namespace Abilities
{
    [CreateAssetMenu(fileName = "New Ability Config", menuName = "Game Configs/Ability Config")]
    public class AbilityConfig : ScriptableObject
    {
        [SerializeField] private DiscountAbilityParameter _discountAbilityParameter;
        [SerializeField] private AbilityConfigParameter _cheeseAbilityParameter;
        [SerializeField] private EnergyAbilityParameter _energyAbilityParameter;

        public DiscountAbilityParameter DiscountConfig => _discountAbilityParameter;

        public AbilityConfigParameter CheeseConfig => _cheeseAbilityParameter;

        public EnergyAbilityParameter EnergyConfig => _energyAbilityParameter;
    }
}