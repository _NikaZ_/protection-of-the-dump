﻿using Towers;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Abilities
{
    public class EnergyAbility : Ability
    {
        private readonly TowerBoostGiver _towerBoostGiver;
        private readonly float _boostTime;

        public EnergyAbility(TowerBoostGiver towerBoostGiver, float boostTime, Sprite preview, float chance) : base(preview, chance)
        {
            _towerBoostGiver = towerBoostGiver;
            _towerBoostGiver.OnCancelGive += OnCancelGive;
            _boostTime = boostTime;
        }

        private void OnCancelGive()
        {
            Count += 1;
        }

        protected override void OnUsed()
        {
            _towerBoostGiver.GiveBoost(_boostTime);
        }

        public override bool CanUse()
        {
            return !_towerBoostGiver.IsGiving;
        }
    }
}