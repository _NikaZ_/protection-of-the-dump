﻿using System;
using UnityEngine;

namespace Abilities
{
    [Serializable]
    public class AbilityConfigParameter
    {
        [SerializeField] private Sprite _preview;
        [SerializeField] private float _chance;
        [SerializeField] private int _initialAmount;

        public Sprite Preview => _preview;

        public float Chance => _chance;

        public int InitialAmount => _initialAmount;
    }
}