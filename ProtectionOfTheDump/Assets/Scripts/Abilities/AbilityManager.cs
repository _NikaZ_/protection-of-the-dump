using System;
using System.Collections.Generic;
using System.Linq;
using Obstacles;
using ResourceManager;
using Towers;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Abilities
{
    public class AbilityManager
    {
        private readonly Dictionary<Type, IAbility> _abilities;

        private readonly AbilityConfig _abilityConfig;

        public AbilityConfig AbilityConfig => _abilityConfig;

        public AbilityManager(ResourceBank resourceBank, AbilityConfig abilityConfig, CheeseObstacleManager cheeseObstacleManager, TowerBoostGiver towerBoostGiver)
        {
            _abilityConfig = abilityConfig;
            _abilities = new Dictionary<Type, IAbility>()
            {
                [typeof(DiscountAbility)] = new DiscountAbility(abilityConfig.DiscountConfig.Preview, abilityConfig.DiscountConfig.Chance, resourceBank.DiscountManager, abilityConfig.DiscountConfig.DiscountAmount) { Count = abilityConfig.DiscountConfig.InitialAmount},
                [typeof(CheeseAbility)] = new CheeseAbility(cheeseObstacleManager, abilityConfig.CheeseConfig.Preview, abilityConfig.CheeseConfig.Chance) {Count = abilityConfig.CheeseConfig.InitialAmount},
                [typeof(EnergyAbility)] = new EnergyAbility(towerBoostGiver, abilityConfig.EnergyConfig.BoostTime, abilityConfig.EnergyConfig.Preview, abilityConfig.EnergyConfig.Chance) {Count = abilityConfig.EnergyConfig.InitialAmount}
            };
        }

        public IAbility GetAbility<T>() where T : IAbility
        {
            if (!_abilities.ContainsKey(typeof(T)))
            {
                throw new ArgumentException();
            }
            return _abilities[typeof(T)];
        }

        public IAbility GetRandomAbility()
        {
            IAbility[] abilities = _abilities.Values.ToArray();
            float chancesSum = abilities.Sum((a) => a.Chance);
            float counter = 0;
            float rand = Random.Range(0, chancesSum);
            foreach (IAbility ability in abilities)
            {
                counter += ability.Chance;
                if (counter > rand)
                {
                    return ability;
                }
            }

            return null;
        }
    }
}
