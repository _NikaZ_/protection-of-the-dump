﻿using Obstacles;
using UnityEngine;

namespace Abilities
{
    public class CheeseAbility : Ability
    {
        private readonly CheeseObstacleManager _cheeseObstacleManager;

        public CheeseAbility(CheeseObstacleManager cheeseObstacleManager, Sprite preview, float chance) : base(preview, chance)
        {
            _cheeseObstacleManager = cheeseObstacleManager;
            _cheeseObstacleManager.OnPlacingCanceled += OnCanceled;
        }

        private void OnCanceled()
        {
            Count += 1;
        }

        protected override void OnUsed()
        {
            _cheeseObstacleManager.PlaceObstacle();
        }

        public override bool CanUse()
        {
            return !_cheeseObstacleManager.IsPlacing;
        }
    }
}