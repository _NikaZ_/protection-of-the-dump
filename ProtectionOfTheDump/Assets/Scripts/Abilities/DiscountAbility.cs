﻿using System;
using ShoppingSystem;
using UnityEngine;

namespace Abilities
{
    public class DiscountAbility : Ability
    {
        private readonly DiscountManager _discountManager;
        private readonly float _discountAmount;

        public DiscountAbility(Sprite preview, float chance, DiscountManager discountManager, float discountAmount) : base(preview, chance)
        {
            if (discountAmount < 0 || discountAmount > 1)
            {
                throw new ArgumentOutOfRangeException(nameof(discountAmount), "Value must be between 0 and 1 inclusive.");
            }
            _discountManager = discountManager;
            _discountAmount = discountAmount;
        }
        
        protected override void OnUsed()
        {
            _discountManager.SetDiscount(_discountAmount);
        }

        public override bool CanUse()
        {
            return !_discountManager.HasDiscount;
        }
    }
}