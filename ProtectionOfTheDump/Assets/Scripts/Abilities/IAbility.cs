﻿namespace Abilities
{
    public interface IAbility
    {
        int Count { get; }
        void Use();
        bool CanUse();
        
        float Chance { get; }

    }
}