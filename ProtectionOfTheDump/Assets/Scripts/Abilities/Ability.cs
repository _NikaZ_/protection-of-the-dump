using System;
using UnityEngine;

namespace Abilities
{
    public abstract class Ability : IAbility
    {
        public event Action<int> OnAbilityCountChanged;
        public event Action OnAbilityUsed;

        private int _count;

        public Sprite Preview { get; }

        public float Chance { get; }

        protected Ability(Sprite preview, float chance)
        {
            Preview = preview;
            Chance = chance;
        }
        
        public int Count
        {
            get => _count;
            set
            {
                if (value < 0)
                {
                    throw new ArgumentOutOfRangeException();
                }
                _count = value;
                OnAbilityCountChanged?.Invoke(value);
            }
        }

        public void Use()
        {
            if (_count <= 0 || !CanUse())
            {
                return;
            }
            OnUsed();
            Count--;
            OnAbilityUsed?.Invoke();
        }

        protected abstract void OnUsed();

        public virtual bool CanUse()
        {
            return true;
        }
    }
}
