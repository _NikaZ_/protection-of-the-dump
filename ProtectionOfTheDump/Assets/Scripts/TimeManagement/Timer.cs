﻿using System;
using UnityEngine;

namespace TimeManagement
{
    public class Timer : MonoBehaviour
    {
        private float _time;
        public event Action OnTimeout;
        public event Action OnTimerStarted;
        public event Action OnTimerStopped;
        public event Action OnTimerPaused;
        public event Action OnTimerResumed;
        public event Action<float> OnTimeUpdate;

        public float Time
        {
            get => _time;
            private set
            {
                _time = value;
                OnTimeUpdate?.Invoke(value);
            }
        }

        public bool IsSet { get; private set; }
        public bool IsPaused { get; private set; }
    
        public void StartTimer(float time)
        {
            if (IsSet)
            {
                StopTimer();
            }
            Time = time;
            IsSet = true;
            IsPaused = false;
            OnTimerStarted?.Invoke();
        }

        public void PauseTimer()
        {
            if (IsPaused)
            {
                return;
            }
            IsPaused = true;
            OnTimerPaused?.Invoke();
        }

        public void ResumeTimer()
        {
            if (!IsPaused)
            {
                return;
            }
            IsPaused = false;
            OnTimerResumed?.Invoke();
        }

        public void StopTimer()
        {
            Time = 0;
            IsSet = false;
            OnTimerStopped?.Invoke();
        }

        private void Update()
        {
            if (IsSet && !IsPaused)
            {
                if (Time > 0)
                {
                    Time -= UnityEngine.Time.unscaledDeltaTime;
                }
                else
                {
                    StopTimer();
                    OnTimeout?.Invoke();
                }
            }
        }
    }
}