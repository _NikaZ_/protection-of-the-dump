﻿namespace SelectionSystem
{
    public interface ISelectionHandler : ISelectable
    {
        void Select();
    }
}