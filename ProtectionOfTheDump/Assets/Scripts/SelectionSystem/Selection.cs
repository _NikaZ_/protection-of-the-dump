﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;

namespace SelectionSystem
{
    /// <summary>
    /// Class for selecting a GameObjects with ISelectable component.
    /// </summary>
    public class Selection : MonoBehaviour
    {
        public event Action<ISelectable> OnSelected;
        public event Action<ISelectable> OnDeselected;

        [SerializeField] private Camera _mainCamera;
        [SerializeField] private LayerMask _selectionMask;
        [SerializeField] private PlayerInput _playerInput;

        private ISelectable _selectedObject;
        private IHoverIn _hoverIn;

        public ISelectable SelectedObject => _selectedObject;

        public bool HasSelection => _selectedObject != null;

        private static Vector2 MousePosition => Mouse.current.position.ReadValue();

        private void OnEnable()
        {
            _playerInput.onActionTriggered += OnActionTriggered;
        }

        private void OnDisable()
        {
            _playerInput.onActionTriggered -= OnActionTriggered;
        }

        private void OnActionTriggered(InputAction.CallbackContext callback)
        {
            switch (callback.action.name)
            {
                case "Click":
                    Select(callback);
                    break;
            }
        }

        private void Update()
        {
            if (EventSystem.current.IsPointerOverGameObject())
            {
                return;
            }
            Ray ray = _mainCamera.ScreenPointToRay(MousePosition);
            if (Physics.Raycast(ray, out RaycastHit hit, Mathf.Infinity, _selectionMask))
            {
                IHoverIn hoverIn = hit.collider.GetComponentInParent<IHoverIn>();
                if (hoverIn != null)
                {
                    if (_hoverIn != hoverIn)
                    {
                        if (_hoverIn is IHoverOut hoverOut)
                        {
                            hoverOut.HoverOut();
                        }
                        _hoverIn = hoverIn;
                        hoverIn.HoverIn();
                    }
                    return;
                }
            }
            if (_hoverIn is IHoverOut hoverOut1)
            {
                hoverOut1.HoverOut();
            }
            _hoverIn = null;
        }

        private void Select(InputAction.CallbackContext callback)
        {
            if (!callback.performed)
            {
                return;
            }
            if (EventSystem.current.IsPointerOverGameObject())
            {
                return;
            }
            Ray ray = _mainCamera.ScreenPointToRay(MousePosition);
            if (Physics.Raycast(ray, out RaycastHit hit, Mathf.Infinity, _selectionMask))
            {
                ISelectable selectable = hit.collider.GetComponentInParent<ISelectable>();
                if (selectable != null)
                {
                    Deselect();
                    _selectedObject = selectable;
                    if (selectable is ISelectionHandler selectionHandler)
                    {
                        selectionHandler.Select();
                    }
                    OnSelected?.Invoke(selectable);
                    return;
                } 
            }
            Deselect();
        }

        public void Deselect()
        {
            if (_selectedObject == null)
            {
                return;
            }

            if (_selectedObject is IDeselectionHandler deselectionHandler)
            {
                deselectionHandler.Deselect();
            }
            OnDeselected?.Invoke(_selectedObject);
            _selectedObject = null;
        }
    }
}