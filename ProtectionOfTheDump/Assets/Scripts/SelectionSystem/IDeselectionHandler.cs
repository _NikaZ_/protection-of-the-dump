﻿namespace SelectionSystem
{
    public interface IDeselectionHandler : ISelectable
    {
        void Deselect();
    }
}