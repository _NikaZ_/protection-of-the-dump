﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace Enemies.EnemyNavigation
{
    public class Path : MonoBehaviour
    {
        [SerializeField] private Vector3[] _checkPoints;

        public Vector3[] CheckPoints { get; private set; }

        private void Awake()
        {
            CheckPoints = (from check in _checkPoints select transform.position + check).ToArray();
        }

#if UNITY_EDITOR
        private void OnDrawGizmosSelected()
        {
            if (_checkPoints == null)
            {
                return;
            }
            for (int i = 1; i < _checkPoints.Length; i++)
            {
                Gizmos.DrawLine(transform.position + _checkPoints[i-1], transform.position + _checkPoints[i]);
            }
        }
#endif
    }
}