﻿using UnityEngine;

namespace Enemies.Spawn
{
    [System.Serializable]
    public class WaveEnemy
    {
        [SerializeField] private Enemy _enemy;
        [SerializeField] private int _count;

        public Enemy Enemy => _enemy;

        public int Count => _count;
    }
}