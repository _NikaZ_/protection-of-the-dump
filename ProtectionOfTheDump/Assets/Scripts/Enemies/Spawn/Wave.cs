﻿using UnityEngine;

namespace Enemies.Spawn
{
    [System.Serializable]
    public class Wave
    {
        [SerializeField] private WaveEnemy[] _waveEnemies;

        public WaveEnemy[] WaveEnemies => _waveEnemies;
    }
}