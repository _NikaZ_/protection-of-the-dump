﻿using Enemies.EnemyNavigation;
using UnityEngine;

namespace Enemies.Spawn
{
    [System.Serializable]
    public class SpawnPoint
    {
        [SerializeField] private Transform _transform;
        [SerializeField] private SpawnConfig _config;
        [SerializeField] private Path _path;

        public Transform Transform => _transform;

        public SpawnConfig Config => _config;

        public Path Path => _path;
    }
}