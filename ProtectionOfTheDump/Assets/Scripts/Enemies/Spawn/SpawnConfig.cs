﻿using UnityEngine;

namespace Enemies.Spawn
{
    [CreateAssetMenu(fileName = "NewSpawnConfig", menuName = "Game Configs/Spawn Config")]
    public class SpawnConfig : ScriptableObject
    {
        [SerializeField] private Wave[] _waves;

        public Wave[] Waves => _waves;
    }
}