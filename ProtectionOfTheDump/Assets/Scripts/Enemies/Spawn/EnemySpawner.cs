﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Abilities;
using ResourceManager;
using UnityEngine;
using UnityEngine.AI;
using Zenject;
using Random = UnityEngine.Random;

namespace Enemies.Spawn
{
    public class EnemySpawner : MonoBehaviour
    {
        public event Action<Enemy> OnEnemySpawned;
        public event Action<Enemy> OnEnemyRemoved;

        [SerializeField] private float _dropChance;
        [SerializeField] private SpawnPoint[] _spawnPoints;
        [SerializeField] private float _spawnInterval;
        [SerializeField] private PoolManager _objectPool;
        
        public bool IsSpawning { get; private set; }

        private int _currentWaveIndex;

        private Coroutine _spawningCoroutine;

        private readonly List<Enemy> _enemiesOnScene = new List<Enemy>();

        private ResourceBank _resourceBank;

        public int EnemiesOnSceneCount => _enemiesOnScene.Count;

        private int _maxWaveIndex;
        private AbilityManager _abilityManager;
        public bool IsWavesEnded => _currentWaveIndex >= _maxWaveIndex;

        public void Init(AbilityManager abilityManager)
        {
            _abilityManager = abilityManager;
        }

        private void Awake()
        {
            Dictionary<GameObject, int> prefabs = GetPrefabs(_spawnPoints);
            _maxWaveIndex = _spawnPoints.Max((sp) => sp.Config.Waves.Length);
            

            foreach (KeyValuePair<GameObject,int> prefab in prefabs)
            {
                prefab.Key.SetActive(false);
                _objectPool.warmPool(prefab.Key, prefab.Value);
            }
            
        }

        [Inject]
        public void Construct(ResourceBank resourceBank)
        {
            _resourceBank = resourceBank;
        }

        private static Dictionary<GameObject, int> GetPrefabs(SpawnPoint[] spawnPoints)
        {
            Dictionary<GameObject, int> prefabs = new Dictionary<GameObject, int>();
            foreach (SpawnPoint spawnPoint in spawnPoints)
            {
                Dictionary<GameObject, int> spawnPointPrefabs = GetSpawnPointPrefabs(spawnPoint);
                foreach (KeyValuePair<GameObject,int> spawnPointPrefab in spawnPointPrefabs)
                {
                    if (prefabs.ContainsKey(spawnPointPrefab.Key))
                    {
                        prefabs[spawnPointPrefab.Key] += spawnPointPrefab.Value;
                    }
                    else
                    {
                        prefabs.Add(spawnPointPrefab.Key, spawnPointPrefab.Value);
                    }
                }
            }

            return prefabs;
        }

        private static Dictionary<GameObject, int> GetSpawnPointPrefabs(SpawnPoint spawnPoint)
        {
            Dictionary<GameObject, int> spPrefabs = new Dictionary<GameObject, int>();
            foreach (Wave configWave in spawnPoint.Config.Waves)
            {
                foreach (WaveEnemy enemy in configWave.WaveEnemies)
                {
                    if (spPrefabs.ContainsKey(enemy.Enemy.gameObject))
                    {
                        if (spPrefabs[enemy.Enemy.gameObject] < enemy.Count )
                        {
                            spPrefabs[enemy.Enemy.gameObject] = enemy.Count;
                        }
                    }
                    else
                    {
                        spPrefabs.Add(enemy.Enemy.gameObject, enemy.Count);
                    }
                }
            }

            return spPrefabs;
        }

        public void StartWave()
        {
            if (IsSpawning || IsWavesEnded)
            {
                return;
            }

            _spawningCoroutine = StartCoroutine(SpawningRoutine());

        }

        private IEnumerator SpawningRoutine()
        {
            IsSpawning = true;
            int currentWave = _currentWaveIndex++;

            Dictionary<SpawnPoint, Queue<Enemy>> _queues = new Dictionary<SpawnPoint, Queue<Enemy>>();

            foreach (SpawnPoint spawnPoint in _spawnPoints)
            {
                Queue<Enemy> waveEnemies = new Queue<Enemy>();
                
                foreach (WaveEnemy waveEnemy in spawnPoint.Config.Waves[currentWave].WaveEnemies)
                {
                    for (int i = 0; i < waveEnemy.Count; i++)
                    {
                        waveEnemies.Enqueue(waveEnemy.Enemy);
                    }
                }
                _queues.Add(spawnPoint, waveEnemies);
            }

            while (!_queues.Values.All((queue => queue.Count == 0)))
            {
                yield return new WaitForSeconds(_spawnInterval);
                foreach (KeyValuePair<SpawnPoint,Queue<Enemy>> queue in _queues)
                {
                    if (queue.Value.Count == 0)
                    {
                        continue;
                    }

                    Spawn(queue.Key, queue.Value.Dequeue());
                }
            }
            IsSpawning = false;
        }

        private void Spawn(SpawnPoint spawnPoint, Enemy enemy)
        {
            Enemy enemyInstance = _objectPool.spawnObject(enemy.gameObject).GetComponent<Enemy>(); //Instantiate(enemy.Enemy, spawnPoint.Transform.position, Quaternion.identity);
            _enemiesOnScene.Add(enemyInstance);
            enemyInstance.Init(spawnPoint.Path);
            enemyInstance.transform.position = spawnPoint.Transform.position;
            enemyInstance.gameObject.SetActive(true);
            enemyInstance.OnEnemyPassedPath += OnEnemyPassed;
            enemyInstance.OnEnemyDied += OnEnemyDied;
            if (Random.Range(0f, 1f) < _dropChance)
            {
                enemyInstance.SetAbilityDrop(_abilityManager.GetRandomAbility() as Ability);
            }
            OnEnemySpawned?.Invoke(enemyInstance);
        }

        private void OnEnemyDied(Enemy enemy)
        {
            _resourceBank.Coins += enemy.DeathReward;
            RemoveEnemy(enemy);
        }

        private void OnEnemyPassed(Enemy enemy)
        {
            _resourceBank.PlayerLives.Value -= 1;
            RemoveEnemy(enemy);
        }


        private void RemoveEnemy(Enemy enemy)
        {
            enemy.OnEnemyDied -= OnEnemyDied;
            enemy.OnEnemyPassedPath -= OnEnemyPassed;
            _objectPool.releaseObject(enemy.gameObject);
            _enemiesOnScene.Remove(enemy);
            OnEnemyRemoved?.Invoke(enemy);
        }

        public void StopSpawning()
        {
            StopCoroutine(_spawningCoroutine);
        }
    }
}