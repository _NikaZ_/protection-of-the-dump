using System;
using System.Collections;
using Abilities;
using Enemies.EnemyNavigation;
using GameProperties;
using Obstacles;
using Towers;
using UnityEngine;
using UnityEngine.AI;

namespace Enemies
{
    [RequireComponent(typeof(NavMeshAgent))]
    public class Enemy : MonoBehaviour, ITowerTarget
    {
        public event Action<Enemy> OnEnemyPassedPath;
        public event Action<Enemy> OnEnemyDied;
        
        [SerializeField] private NavMeshAgent _navMeshAgent;
        [SerializeField] private EnemyConfig _enemyConfig;
        [SerializeField] private EnemyDrop _enemyDrop;
        
        [SerializeField] private ObstacleBreaker _obstacleBreaker;

        [SerializeField] private Animator _animator;
        [SerializeField] private float _deathDelay;
        private Health _health;
        private Coroutine _walking;
        private Path _path;
        private static readonly int Death = Animator.StringToHash("Death");

        public int DeathReward => _enemyConfig.DeathReward;


        public void Init(Path path)
        {
            _path = path;
        }

        public void SetAbilityDrop(Ability ability) => _enemyDrop.SetAbilityDrop(ability);
        
        private void OnEnable()
        {
            _health = new Health(_enemyConfig.MaxHealth);
            _navMeshAgent.speed = _enemyConfig.MaxSpeed;
            _navMeshAgent.isStopped = false;
            _health.OnHealthChanged += OnHealthChanged;
            _obstacleBreaker.OnStartBreaking += OnStartBreaking;
            _obstacleBreaker.OnEndBreaking += OnEndBreaking;
            _walking = StartCoroutine(Walking());
        }

        private void OnStartBreaking()
        {
            _navMeshAgent.isStopped = true;
        }
        
        private void OnEndBreaking()
        {
            _navMeshAgent.isStopped = false;
        }

        private void OnDisable()
        {
            _obstacleBreaker.OnStartBreaking -= OnStartBreaking;
            _obstacleBreaker.OnEndBreaking -= OnEndBreaking;
            _health.OnHealthChanged -= OnHealthChanged;
            if (_walking != null)
            {
                StopCoroutine(_walking);
            }
        }
        
        private void OnHealthChanged(int value)
        {
            if (value <= 0)
            {
                _health.OnHealthChanged -= OnHealthChanged;
                _enemyDrop.Drop();
                _enemyDrop.RemoveAbilityDrop();
                
                _animator.SetTrigger(Death);
                _navMeshAgent.isStopped = true;
                StartCoroutine(DeathDelay(_deathDelay));
            }
        }

        IEnumerator DeathDelay(float time)
        {
            yield return new WaitForSeconds(time);
            gameObject.SetActive(false);
            OnEnemyDied?.Invoke(this);
        }

        private IEnumerator Walking()
        {
            foreach (Vector3 checkPoint in _path.CheckPoints)
            {
                _navMeshAgent.SetDestination(checkPoint);
                yield return null;
                while (_navMeshAgent.remainingDistance > _navMeshAgent.stoppingDistance)
                {
                    yield return null;
                }
                yield return null;
            }
            OnEnemyPassedPath?.Invoke(this);
        }
        public void OnTargetAttacked(int damage)
        {
            _health.Value -= damage;
        }

        public bool IsDied => _health.Value <= 0 || !isActiveAndEnabled;
    }
}
