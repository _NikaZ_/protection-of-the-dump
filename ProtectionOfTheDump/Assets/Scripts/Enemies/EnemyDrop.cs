﻿using System;
using Abilities;
using Drop;
using UnityEngine;
using UnityEngine.UI;

namespace Enemies
{
    public class EnemyDrop : MonoBehaviour
    {
        private Ability _ability;
        [SerializeField] private ParticleSystem _abilityImage;
        [SerializeField] private DropItem _dropItem;

        public void SetAbilityDrop(Ability ability)
        {
            _ability = ability ?? throw new ArgumentException();
            _abilityImage.gameObject.SetActive(true);
        }

        public void RemoveAbilityDrop()
        {
            _ability = null;
            _abilityImage.gameObject.SetActive(false);
        }

        public void Drop()
        {
            if (_ability == null)
            {
                return;
            }
            
            DropItem dropItem = Instantiate(_dropItem, transform.position, Quaternion.identity);
            dropItem.gameObject.SetActive(true);
            dropItem.DropPreview = _ability.Preview;
            dropItem.CollectDropCommand = new AbilityDropCommand(_ability);
        }
    }
}