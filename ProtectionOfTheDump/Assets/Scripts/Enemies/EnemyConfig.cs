using UnityEngine;

namespace Enemies
{
    [CreateAssetMenu(fileName = "New Enemy Config", menuName = "Game Configs/Enemy Config")]
    public class EnemyConfig : ScriptableObject
    {
        [SerializeField] private int _maxHealth;
        [SerializeField] private float _maxSpeed;
        [SerializeField] private int _damage;
        [SerializeField] private int _deathReward;

        public int MaxHealth => _maxHealth;

        public float MaxSpeed => _maxSpeed;

        public int Damage => _damage;

        public int DeathReward => _deathReward;
    }
}
