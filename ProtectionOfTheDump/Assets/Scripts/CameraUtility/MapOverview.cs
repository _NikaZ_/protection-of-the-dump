﻿using System;
using UnityEngine;
using UnityEngine.Playables;

namespace CameraUtility
{
    public class MapOverview
    {
        public event Action OnOverviewStop;
        
        private readonly PlayableDirector _playableDirector;

        public MapOverview(PlayableDirector playableDirector)
        {
            _playableDirector = playableDirector;
            _playableDirector.stopped += (director) => OnOverviewStop?.Invoke();
        }

        public void Start()
        {
            _playableDirector.Play();
            
        }

        public void Stop()
        {
            _playableDirector.Stop();
            
        }
    }
}