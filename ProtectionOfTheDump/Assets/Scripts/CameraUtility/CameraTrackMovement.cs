using System;
using Cinemachine;
using UnityEngine;
using UnityEngine.InputSystem;

namespace CameraUtility
{
    public class CameraTrackMovement : MonoBehaviour
    {
        [SerializeField] private CinemachineVirtualCamera _virtualCamera;
        [SerializeField] private float _speed = 1f;
        private CinemachineTrackedDolly _cinemachineTrackedDolly;

        private void Awake()
        {
            _cinemachineTrackedDolly = _virtualCamera.GetCinemachineComponent<CinemachineTrackedDolly>();
        }

        private void Update()
        {
            if (Mouse.current.leftButton.isPressed)
            {
                _cinemachineTrackedDolly.m_PathPosition += -Mouse.current.delta.ReadValue().x * Time.deltaTime * _speed;
                if (_cinemachineTrackedDolly.m_PathPosition < 0)
                {
                    _cinemachineTrackedDolly.m_PathPosition = 0;
                }
                else if (_cinemachineTrackedDolly.m_PathPosition > _cinemachineTrackedDolly.m_Path.PathLength)
                {
                    _cinemachineTrackedDolly.m_PathPosition = _cinemachineTrackedDolly.m_Path.PathLength;
                }
            }
        }
    }
}
