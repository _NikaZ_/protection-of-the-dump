﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;

namespace CameraUtility
{
    public class CameraMovement : MonoBehaviour
    {
        [SerializeField] private PlayerInput _playerInput;
        [SerializeField] private float _speed = 20;
        [Range(0f, 100f)]
        [SerializeField] private float _activeZonePercent = 10;

        [Header("Bounds")]
        [SerializeField] private Vector2 _center;
        [SerializeField] private Vector2 _size;

        private Vector3 _movingDirection;
        private bool _isKeyboardControlling;

        private void Awake()
        {
            _playerInput.onActionTriggered += OnActionTriggered;
        }

        private void Update()
        {
            if (EventSystem.current.IsPointerOverGameObject())
            {
                return;
            }

            Vector2 position = transform.position;
            
            Vector3 delta = new Vector3(transform.position.x, 0, transform.position.z) - new Vector3(_center.x, 0, _center.y);

            Vector3 worldDirection = transform.TransformDirection(_movingDirection);

            if (delta.x > _size.x / 2 && worldDirection.x > 0)
            {
                worldDirection.x = 0;
            }
            else if (delta.x < -_size.x / 2 && worldDirection.x < 0)
            {
                worldDirection.x = 0;
            }
            
            if (delta.z > _size.y / 2 && worldDirection.z > 0)
            {
                worldDirection.z = 0;
            }
            else if (delta.z < -_size.y / 2 && worldDirection.z < 0)
            {
                worldDirection.z = 0;
            }

            _movingDirection = transform.InverseTransformDirection(worldDirection);
            
            transform.Translate(_movingDirection * Time.deltaTime * _speed);

            // if (!_isKeyboardControlling)
            // {
            //     MoveByMouse();
            // }
        }

        private void MoveByMouse()
        {
            Vector2 mousePosition = Mouse.current.position.ReadValue();
            Vector2 screenCenter = new Vector2(Screen.width, Screen.height) / 2;
            
            float xDistance = DistanceToNear(mousePosition.x, screenCenter.x, Screen.width);
            float yDistance = DistanceToNear(mousePosition.y, screenCenter.y, Screen.height);

            float horizontalZone = Screen.width * (_activeZonePercent / 100);
            float verticalZone = Screen.height * (_activeZonePercent / 100);
            if (mousePosition.x < 0 || mousePosition.x > Screen.width || mousePosition.y < 0 || mousePosition.y > Screen.height)
            {
                _movingDirection = new Vector3();
                return;
            }
            if (xDistance < horizontalZone && xDistance > 0 || yDistance < verticalZone && yDistance > 0)
            {
                Vector2 mouseDirection = (mousePosition - screenCenter).normalized;
                float xScale = Mathf.Max((horizontalZone - xDistance) / horizontalZone, 0);
                float yScale = Mathf.Max((verticalZone - yDistance) / verticalZone, 0);
                _movingDirection = new Vector3(mouseDirection.x * xScale, 0, mouseDirection.y * yScale);
            }
            else
            {
                _movingDirection = new Vector3();
            }
        }

        private float DistanceToNear(float position, float center, float size)
        {
            return -(Mathf.Abs(position - center) - size / 2);
        }

        private void OnActionTriggered(InputAction.CallbackContext callback)
        {
            switch (callback.action.name)
            {
                case "Move":
                    MoveByKeyboard(callback);
                    break;
            }
        }

        private void MoveByKeyboard(InputAction.CallbackContext callback)
        {
            if (callback.performed)
            {
                _isKeyboardControlling = true;
            }
            else if (callback.canceled)
            {
                _isKeyboardControlling = false;
            }
            Vector2 direction = callback.ReadValue<Vector2>();
            _movingDirection = new Vector3(direction.x, 0, direction.y).normalized;
        }

#if UNITY_EDITOR
        private void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.green;
            Gizmos.DrawCube(new Vector3(_center.x, 0, _center.y), new Vector3(_size.x, 0, _size.y));
            Gizmos.DrawWireCube(new Vector3(_center.x, 0, _center.y), new Vector3(_size.x, 0, _size.y));
        }
#endif
    }
}