﻿namespace StateMachineSystem
{
    public interface IStatesRoot
    {
        public T GetState<T>() where T : IStateExitable;
    }
}