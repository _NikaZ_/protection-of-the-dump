﻿namespace StateMachineSystem.States
{
    public interface IStateUpdateable
    {
        void Update();
    }
}