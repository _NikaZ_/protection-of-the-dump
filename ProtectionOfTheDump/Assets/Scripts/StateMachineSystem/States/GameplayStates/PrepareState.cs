﻿using Core;
using TimeManagement;
using UnityEngine;

namespace StateMachineSystem.States.GameplayStates
{
    public class PrepareState : IState
    {
        private readonly Timer _timer;
        private readonly float _prepareTime;
        private readonly GameplayStatesRoot _gameplayStatesRoot;
        private IStateMachine _stateMachine;

        public PrepareState(Timer timer, float prepareTime, GameplayStatesRoot gameplayStatesRoot)
        {
            _timer = timer;
            _prepareTime = prepareTime;
            _gameplayStatesRoot = gameplayStatesRoot;
            
        }
        
        public void Enter(IStateMachine stateMachine)
        {
            _stateMachine = stateMachine;
            _timer.StartTimer(_prepareTime);
            _timer.OnTimeout += OnTimeout;
            if (!Bootstrapper.Instance.AudioManager.IsPlaying("idle"))
            {
                Bootstrapper.Instance.AudioManager.Play("idle");
            }
        }

        private void OnTimeout()
        {
            _stateMachine.ChangeState(_gameplayStatesRoot.GetState<RaidState>());
        }

        public void Exit()
        {
            _timer.OnTimeout -= OnTimeout;
        }
    }
}