﻿using Enemies.Spawn;
using TimeManagement;
using UnityEngine;

namespace StateMachineSystem.States.GameplayStates
{
    public class PauseState : IState
    {
        private readonly Timer _timer;

        public PauseState(Timer timer)
        {
            _timer = timer;
        }
        
        public void Enter(IStateMachine stateMachine)
        {
            _timer.PauseTimer();
            Time.timeScale = 0;
        }

        public void Exit()
        {
            _timer.ResumeTimer();
            Time.timeScale = 1;
        }
    }
}