﻿using System;
using System.Collections.Generic;
using CameraUtility;
using Enemies.Spawn;
using ResourceManager;
using StateMachineSystem.States.GameplayStates.Parameters;
using StateMachineSystem.States.GameStates;
using TimeManagement;

namespace StateMachineSystem.States.GameplayStates
{
    public class GameplayStatesRoot : IStatesRoot
    {
        private readonly Dictionary<Type, IStateExitable> _states;

        public GameplayStatesRoot(ResourceBank resourceBank, EnemySpawner enemySpawner, Timer timer, float prepareTime, MapOverviewStateParameters mapOverviewStateParameters)
        {
            _states = new Dictionary<Type, IStateExitable>()
            {
                [typeof(PrepareState)] = new PrepareState(timer, prepareTime, this),
                [typeof(RaidState)] = new RaidState(resourceBank, enemySpawner, this, timer),
                [typeof(PauseState)] = new PauseState(timer),
                [typeof(MapOverviewState)] = new MapOverviewState(mapOverviewStateParameters, this),
                [typeof(FirstPrepareState)] = new FirstPrepareState(),
                [typeof(WinState)] = new WinState(resourceBank),
                [typeof(LoseState)] = new LoseState()
            };
        }
        
        public T GetState<T>() where T : IStateExitable
        {
            if (_states.TryGetValue(typeof(T), out IStateExitable state))
            {
                return (T)state;
            }

            throw new KeyNotFoundException($"{typeof(T).FullName} not found");
        }
    }
}