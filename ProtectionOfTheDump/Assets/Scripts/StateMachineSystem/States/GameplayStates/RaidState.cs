﻿using Core;
using Enemies;
using Enemies.Spawn;
using ResourceManager;
using TimeManagement;
using UnityEngine;

namespace StateMachineSystem.States.GameplayStates
{
    public class RaidState : IState
    {
        private readonly ResourceBank _resourceBank;
        private readonly EnemySpawner _enemySpawner;
        private readonly GameplayStatesRoot _gameplayStatesRoot;
        private readonly Timer _timer;
        private IStateMachine _stateMachine;
        

        public RaidState(ResourceBank resourceBank, EnemySpawner enemySpawner, GameplayStatesRoot gameplayStatesRoot, Timer timer)
        {
            _resourceBank = resourceBank;
            _enemySpawner = enemySpawner;
            _gameplayStatesRoot = gameplayStatesRoot;
            _timer = timer;
            
        }
        
        public void Enter(IStateMachine stateMachine)
        {
            _stateMachine = stateMachine;
            _enemySpawner.OnEnemyRemoved += OnEnemyRemoved;
            if (_enemySpawner.EnemiesOnSceneCount > 0)
            {
                return;
            }
            _enemySpawner.StartWave();
            _timer.StopTimer();
            if (!Bootstrapper.Instance.AudioManager.IsPlaying("raid"))
            {
                Bootstrapper.Instance.AudioManager.Play("raid");
            }
            
            Debug.Log("Enter Raid");
        }

        private void OnEnemyRemoved(Enemy enemy)
        {
            Debug.Log("OnEnemy removed");
            Debug.Log($"Player lives: {_resourceBank.PlayerLives.Value}");
            if (_resourceBank.PlayerLives.Value <= 0)
            {
                _stateMachine.ChangeState(_gameplayStatesRoot.GetState<LoseState>());
                Debug.Log("Exit to Lose");
                return;
            }
            Debug.Log($"Enemies On Scene Count: {_enemySpawner.EnemiesOnSceneCount}");
            Debug.Log($"Is spawning: {_enemySpawner.IsSpawning}");
            if (_enemySpawner.EnemiesOnSceneCount == 0 && !_enemySpawner.IsSpawning)
            {
                Debug.Log($"Is waves ended: {_enemySpawner.IsWavesEnded}");
                if (_enemySpawner.IsWavesEnded)
                {
                    _stateMachine.ChangeState(_gameplayStatesRoot.GetState<WinState>());
                    Debug.Log("Exit to Win");
                    return;
                }
                _stateMachine.ChangeState(_gameplayStatesRoot.GetState<PrepareState>());
                Debug.Log("Exit to Prepare");
            }
        }

        public void Exit()
        {
            _enemySpawner.OnEnemyRemoved -= OnEnemyRemoved;
            Debug.Log("Exit Raid");
        }
    }
}