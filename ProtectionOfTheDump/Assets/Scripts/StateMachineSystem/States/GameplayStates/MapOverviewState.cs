﻿using CameraUtility;
using SelectionSystem;
using StateMachineSystem.States.GameplayStates.Parameters;
using UI.Views;
using UnityEngine;

namespace StateMachineSystem.States.GameplayStates
{
    public class MapOverviewState : IState
    {

        private readonly MapOverview _mapOverview;
        private readonly GameplayStatesRoot _gameplayStatesRoot;
        private readonly CameraMovement _cameraMovement;
        private readonly Selection _selection;

        private IStateMachine _stateMachine;

        public MapOverviewState(MapOverviewStateParameters mapOverviewStateParameters, GameplayStatesRoot gameplayStatesRoot)
        {
            _mapOverview = mapOverviewStateParameters.MapOverview;
            _gameplayStatesRoot = gameplayStatesRoot;
            _cameraMovement = mapOverviewStateParameters.CameraMovement;
            _selection = mapOverviewStateParameters.Selection;
        }

        public void Enter(IStateMachine stateMachine)
        {
            _mapOverview.Start();
            _stateMachine = stateMachine;
            _mapOverview.OnOverviewStop += OnOverviewStop;
            _cameraMovement.enabled = false;
            _selection.enabled = false;
        }

        private void OnOverviewStop()
        {
            _stateMachine.ChangeState(_gameplayStatesRoot.GetState<FirstPrepareState>());
        }

        public void Exit()
        {
            _mapOverview.OnOverviewStop -= OnOverviewStop;
            _mapOverview.Stop();
            _cameraMovement.enabled = true;
            _selection.enabled = true;
        }
    }
}