﻿using CameraUtility;
using SelectionSystem;
using UI.Views;

namespace StateMachineSystem.States.GameplayStates.Parameters
{
    public class MapOverviewStateParameters
    {
        private readonly MapOverview _mapOverview;
        private readonly CameraMovement _cameraMovement;
        private readonly Selection _selection;

        public MapOverview MapOverview => _mapOverview;

        public CameraMovement CameraMovement => _cameraMovement;

        public Selection Selection => _selection;


        public MapOverviewStateParameters(MapOverview mapOverview, CameraMovement cameraMovement, Selection selection)
        {
            _mapOverview = mapOverview;
            _cameraMovement = cameraMovement;
            _selection = selection;
        }
    }
}