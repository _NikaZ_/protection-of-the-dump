﻿using UnityEngine;
using ResourceManager;
using Core;

namespace StateMachineSystem.States.GameplayStates
{
    public class WinState : IState
    {
        ResourceBank _resourceBank;

        public WinState(ResourceBank resourceBank)
        {
            _resourceBank = resourceBank;
        }

        public void Enter(IStateMachine stateMachine)
        {
            int rating = (int) ((float) _resourceBank.PlayerLives.Value / _resourceBank.PlayerLives.MaxValue * 3);
            int level = Bootstrapper.Instance.Game.CurrentLevel;
            int lastRating = Bootstrapper.Instance.SaveManager.GetInteger($"level_{level}", 0);
            if (rating > lastRating){
                Bootstrapper.Instance.SaveManager.SetInteger($"level_{level}", rating);
            }
            Bootstrapper.Instance.AudioManager.Play("win");
        }

        public void Exit()
        {
            
        }
    }
}