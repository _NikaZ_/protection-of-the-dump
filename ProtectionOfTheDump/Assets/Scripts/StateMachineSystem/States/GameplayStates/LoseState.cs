﻿using Core;

namespace StateMachineSystem.States.GameplayStates
{
    public class LoseState : IState
    {
        
        
        public void Enter(IStateMachine stateMachine)
        {
            Bootstrapper.Instance.AudioManager.Play("lose");
        }

        public void Exit()
        {
            
        }
    }
}