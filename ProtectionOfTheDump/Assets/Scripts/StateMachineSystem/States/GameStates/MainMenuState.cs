﻿using Core;
using Maps;
using UI.Controllers;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace StateMachineSystem.States.GameStates
{
    public class MainMenuState : IState
    {
        private readonly MapManager _mapManager;
        private MainMenu _mainMenu;

        public MainMenuState(MapManager mapManager)
        {
            _mapManager = mapManager;
            
        }

        public void Enter(IStateMachine stateMachine)
        {
            _mapManager.LoadMainMenu();
            Bootstrapper.Instance.AudioManager.Play("menu");
            //_mainMenu.OpenMenu();
        }

        public void Exit()
        {
            Debug.Log("Main menu exit");
        }
    }
}