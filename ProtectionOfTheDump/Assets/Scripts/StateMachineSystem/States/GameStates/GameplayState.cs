﻿using Core;
using Maps;

namespace StateMachineSystem.States.GameStates
{
    public class GameplayState : IState
    {
        private MapManager _mapManager;
        
        public GameplayState(MapManager mapManager)
        {
            _mapManager = mapManager;
        }
        
        public void Enter(IStateMachine stateMachine)
        {
            _mapManager.LoadCurrentLevel();
            if (!Bootstrapper.Instance.AudioManager.IsPlaying("idle"))
            {
                Bootstrapper.Instance.AudioManager.Play("idle");
            }
        }

        public void Exit()
        {
            Bootstrapper.Instance.SaveManager.Save();
        }
    }
}