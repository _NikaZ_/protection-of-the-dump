﻿using System;
using System.Collections.Generic;
using Maps;
using ResourceManager;
using UnityEngine;
using Zenject;

namespace StateMachineSystem.States.GameStates
{
    public class GameStatesRoot : IStatesRoot
    {
        private readonly Dictionary<Type, IStateExitable> _states;
        public GameStatesRoot(MapManager mapManager)
        {
            _states = new Dictionary<Type, IStateExitable>()
            {
                [typeof(GameplayState)] = new GameplayState(mapManager),
                [typeof(MainMenuState)] = new MainMenuState(mapManager)
            };
        }
        public T GetState<T>() where T : IStateExitable
        {
            if (_states.TryGetValue(typeof(T), out IStateExitable state))
            {
                return (T)state;
            }

            throw new KeyNotFoundException($"{typeof(T).FullName} not found");
        }
    }
}