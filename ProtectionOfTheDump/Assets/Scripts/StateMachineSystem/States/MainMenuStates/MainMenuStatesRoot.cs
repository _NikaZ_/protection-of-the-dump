﻿using System;
using System.Collections.Generic;

namespace StateMachineSystem.States.MainMenuStates
{
    public class MainMenuStatesRoot : IStatesRoot
    {
        private readonly Dictionary<Type, IStateExitable> _states = new Dictionary<Type, IStateExitable>()
        {
            [typeof(LevelSelectionState)] = new LevelSelectionState(),
            [typeof(MenuState)] = new MenuState(),
            [typeof(OptionsState)] = new OptionsState()
        };

        public T GetState<T>() where T : IStateExitable
        {
            if (_states.TryGetValue(typeof(T), out IStateExitable state))
            {
                return (T)state;
            }

            throw new KeyNotFoundException($"{typeof(T).FullName} not found");
        }
    }
}