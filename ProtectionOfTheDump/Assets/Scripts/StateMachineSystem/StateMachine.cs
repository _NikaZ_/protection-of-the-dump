using System;

namespace StateMachineSystem
{
    public class StateMachine : IStateMachine
    {
        public event Action<IStateExitable> OnStateChanged;
        
        private IStateExitable _currentState;

        public IStateExitable CurrentState => _currentState;

        public void ChangeState(IStateExitable state)
        {
            _currentState?.Exit();
            _currentState = state;
            if (state is IStateEnterable stateEnterable)
            {
                stateEnterable.Enter(this);
            }
            OnStateChanged?.Invoke(_currentState);
        }
    }
}
