﻿using System;
using StateMachineSystem.States;
using UnityEngine;

namespace StateMachineSystem
{
    public class MonoStateMachine : MonoBehaviour, IStateMachine
    {
        private IStateExitable _currentState;

        public void ChangeState(IStateExitable state)
        {
            _currentState?.Exit();
            _currentState = state;
            if (state is IStateEnterable stateEnterable)
            {
                stateEnterable.Enter(this);
            }
        }

        public void Update()
        {
            if (_currentState is IStateUpdateable updateable)
            {
                updateable.Update();
            }
        }
    }

    public interface IStateMachine
    {
        void ChangeState(IStateExitable exitable);
    }
}