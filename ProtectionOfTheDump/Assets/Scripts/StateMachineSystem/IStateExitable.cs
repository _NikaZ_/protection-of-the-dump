﻿namespace StateMachineSystem
{
    public interface IStateExitable
    {
        void Exit();
    }
}