﻿namespace StateMachineSystem
{
    public interface IStateEnterable
    {
        void Enter(IStateMachine stateMachine);
    }
}