using System;
using SelectionSystem;
using UnityEngine;
using UnityEngine.InputSystem;

namespace ObjectPlacement
{
    public class ObjectPlacer : MonoBehaviour
    {

        public event Action OnPlacingCanceled;
        
        [SerializeField] private Camera _raycastCamera;
        [SerializeField] private PlayerInput _input;
        
        private IPlaceableObject _placeableObject;

        public bool IsPlacing => _placeableObject != null;

        private void Awake()
        {
            _input.onActionTriggered += OnActionTriggered;
            //Selection.OnSelected += OnSelected;
        }

        private void OnDestroy()
        {
            _input.onActionTriggered -= OnActionTriggered;
            //Selection.OnSelected -= OnSelected;
        }

        private void OnSelected(ISelectable obj)
        {
            StopPlacing();
        }

        private void OnActionTriggered(InputAction.CallbackContext callback)
        {
            if (!callback.performed)
            {
                return;
            }
            switch (callback.action.name)
            {
                case "Place":
                    EndPlacing();
                    break;
                case "Cancel":
                    if (IsPlacing)
                    {
                        StopPlacing();
                        OnPlacingCanceled?.Invoke();
                    }
                    break;
            }
        }

        public void PlaceObject(IPlaceableObject placeableObject)
        {
            if (_placeableObject != null)
            {
                return;
            }
            _placeableObject = placeableObject;
            _placeableObject.OnStartPlacing();
        }

        private void StopPlacing()
        {
            if (_placeableObject == null)
            {
                return;
            }
            _placeableObject.OnPlacingCanceled();
            _placeableObject = null;
        }
        
        private void EndPlacing()
        {
            if (_placeableObject == null)
            {
                return;
            }
            Ray ray = _raycastCamera.ScreenPointToRay(Mouse.current.position.ReadValue());
            if (Physics.Raycast(ray, out RaycastHit hit, Mathf.Infinity, _placeableObject.PlaceableLayerMask))
            {
                _placeableObject.OnEndPlacing();
                _placeableObject = null;
                return;
            }
            StopPlacing();
        }

        private void Update()
        {
            if (_placeableObject == null)
            {
                return;
            }
            
            Ray ray = _raycastCamera.ScreenPointToRay(Mouse.current.position.ReadValue());

            if (Physics.Raycast(ray, out RaycastHit hit, Mathf.Infinity, _placeableObject.PlaceableLayerMask))
            {
                _placeableObject.OnPlacing(true, hit.point);
            }
            else
            {
                _placeableObject.OnPlacing(false, Vector3.zero);
            }
        }
    }

    public interface IPlaceableObject
    {
        public LayerMask PlaceableLayerMask { get; }
        public void OnStartPlacing();
        public void OnPlacing(bool canPlace, Vector3 point);
        public void OnEndPlacing();

        public void OnPlacingCanceled();
    }
}
