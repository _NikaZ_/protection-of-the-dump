using System;
using System.Collections;
using Enemies;
using ObjectPlacement;
using UnityEngine;

namespace Obstacles
{
    public class ObstacleBreaker : MonoBehaviour
    {
        public event Action OnStartBreaking;
        public event Action OnEndBreaking;
        
        [SerializeField] private EnemyConfig _enemyConfig;
        [SerializeField] private LayerMask _layerMask;

        [SerializeField] private float _radius;

        private BreakableObstacle _breakableObstacle;

        private Coroutine _obstacleHitCoroutine;

        private void OnEnable()
        {
            _obstacleHitCoroutine = StartCoroutine(ObstacleHit());
        }

        private void OnDisable()
        {
            if (_obstacleHitCoroutine != null)
            {
                StopCoroutine(_obstacleHitCoroutine);
            }
        }

        IEnumerator ObstacleHit()
        {
            do
            {
                if (_breakableObstacle == null)
                {
                    Collider[] result = new Collider[1];
                    Physics.OverlapSphereNonAlloc(transform.position, _radius, result, _layerMask);
                    if (result[0] != null)
                    {
                        _breakableObstacle = result[0].GetComponent<BreakableObstacle>();
                        _breakableObstacle.OnBreak += OnBreak;
                        Debug.Log("Start breaking");
                        OnStartBreaking?.Invoke();
                    }
                }
                else
                {
                    _breakableObstacle.Hit(_enemyConfig.Damage);
                }

                yield return new WaitForSeconds(1);
            } while (_obstacleHitCoroutine != null);
        }

        private void OnBreak(BreakableObstacle breakableObstacle)
        {
            breakableObstacle.OnBreak -= OnBreak;
            breakableObstacle.gameObject.SetActive(false);
            _breakableObstacle = null;
            OnEndBreaking?.Invoke();
        }
    }
}
