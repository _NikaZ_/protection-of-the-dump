using System;
using GameProperties;
using ObjectPlacement;
using UnityEngine;
using UnityEngine.AI;

namespace Obstacles
{
    public class BreakableObstacle : MonoBehaviour, IPlaceableObject
    {
        public event Action<BreakableObstacle> OnBreak ;
        public event Action<BreakableObstacle> OnPlacingCancel;
        
        [SerializeField] private int _maxHealth;
        [SerializeField] private LayerMask _placeableLayerMask;
        [SerializeField] private NavMeshObstacle _navMeshObstacle;
        [SerializeField] private Collider _collider;

        private Health _health;
        private void Awake()
        {
            
        }

        public void Hit(int damage)
        {
            _health.Value -= damage;
            if (_health.Value <= 0)
            {
                OnBreak?.Invoke(this);
            }
        }

        public LayerMask PlaceableLayerMask => _placeableLayerMask;

        public void OnStartPlacing()
        {
            _navMeshObstacle.enabled = false;
            _collider.enabled = false;
        }

        public void OnPlacing(bool canPlace, Vector3 point)
        {
            gameObject.SetActive(canPlace);
            transform.position = point;
        }

        public void OnEndPlacing()
        {
            gameObject.SetActive(true);
            _navMeshObstacle.enabled = true;
            _collider.enabled = true;
            _health = new Health(_maxHealth);
        }

        public void OnPlacingCanceled()
        {
            OnPlacingCancel?.Invoke(this);
        }
    }
}
