﻿using System;
using ObjectPlacement;
using UnityEngine;

namespace Obstacles
{
    public class CheeseObstacleManager : MonoBehaviour
    {
        public event Action OnPlacingCanceled;
        
        [SerializeField] private BreakableObstacle _cheesePrefab;
        [SerializeField] private PoolManager _obstaclePoolManager;
        [SerializeField] private ObjectPlacer _objectPlacer;
        [SerializeField] private int _poolInitialSize = 3;

        public bool IsPlacing => _objectPlacer.IsPlacing;
        
        private void Awake()
        {
            _obstaclePoolManager.warmPool(_cheesePrefab.gameObject, _poolInitialSize, false);
            _objectPlacer.OnPlacingCanceled += OnPlacingCancel;
        }

        private void OnDestroy()
        {
            _objectPlacer.OnPlacingCanceled -= OnPlacingCancel;
        }

        private void OnPlacingCancel()
        {
            OnPlacingCanceled?.Invoke();
        }

        public void PlaceObstacle()
        {
            if (IsPlacing)
            {
                return;
            }
            BreakableObstacle obstacle = _obstaclePoolManager.spawnObject(_cheesePrefab.gameObject).GetComponent<BreakableObstacle>();
            obstacle.gameObject.SetActive(true);
            obstacle.OnBreak += ReturnToPool;
            obstacle.OnPlacingCancel += ReturnToPool;
            _objectPlacer.PlaceObject(obstacle);
        }

        private void ReturnToPool(BreakableObstacle obj)
        {
            obj.OnBreak -= ReturnToPool;
            obj.OnPlacingCancel -= ReturnToPool;
            obj.gameObject.SetActive(false);
            _obstaclePoolManager.releaseObject(obj.gameObject);
        }
    }
}