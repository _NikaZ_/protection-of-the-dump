﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Audio;

namespace AudioManagement
{
    public class AudioManager : MonoBehaviour
    {
        public event Action<AudioStoppedEventArgs> OnAudioStopped;
        
        [SerializeField] private List<Clip> _clips;
        [SerializeField] private AudioSource _template;

        private List<Clip> _playingClips = new List<Clip>();

        public void Init()
        {
            foreach (Clip clip in _clips)
            {
                AudioSource audioSource = gameObject.AddComponent<AudioSource>();
                if (_template != null)
                {
                    Copy(audioSource, _template);
                }

                audioSource.playOnAwake = false;
                audioSource.clip = clip.Audio;
                audioSource.loop = clip.Loop;
                audioSource.volume = clip.Volume;
                audioSource.pitch = clip.Pitch;
                audioSource.outputAudioMixerGroup = clip.MixerGroup;
                clip.AudioSource = audioSource;
                if (clip.PlayOnAwake)
                {
                    Play(clip.Name);
                }
            }
        }

        private void Update()
        {
            for (var i = 0; i < _playingClips.Count; i++)
            {
                var playingClip = _playingClips[i];
                if (!playingClip.AudioSource.isPlaying)
                {
                    _playingClips.Remove(playingClip);
                    OnAudioStopped?.Invoke(new AudioStoppedEventArgs(playingClip.Name, playingClip.Audio, true));
                }
            }
        }

        private void Copy(AudioSource audioSource, AudioSource template)
        {
            audioSource.mute = template.mute;
            audioSource.priority = template.priority;
            audioSource.spread = template.spread;
            audioSource.bypassEffects = template.bypassEffects;
            audioSource.dopplerLevel = template.dopplerLevel;
            audioSource.maxDistance = template.maxDistance;
            audioSource.minDistance = template.minDistance;
            audioSource.panStereo = template.panStereo;
            audioSource.rolloffMode = template.rolloffMode;
            audioSource.spatialBlend = template.spatialBlend;
            audioSource.bypassListenerEffects = template.bypassListenerEffects;
            audioSource.bypassReverbZones = template.bypassReverbZones;
            audioSource.reverbZoneMix = template.reverbZoneMix;
        }

        public void Play(string name)
        {
            Clip clip = _clips.Find((clip1 => clip1.Name == name));
            if (clip == null)
            {
                Debug.LogWarning($"Can't find clip \"{name}\"");
                return;
            }

            if (!string.IsNullOrEmpty(clip.Group))
            {
                foreach (Clip groupClip in _clips.FindAll((clip1 => clip1.Group == clip.Group)))
                {
                    groupClip.AudioSource.Stop();
                    OnAudioStopped?.Invoke(new AudioStoppedEventArgs(clip.Name, clip.Audio, true));
                }
            }

            if (!_playingClips.Contains(clip))
            {
                _playingClips.Add(clip);
            }
            clip.AudioSource.Play();
        }

        public void Stop(string name)
        {
            Clip clip = _playingClips.Find((clip1 => clip1.Name == name));
            if (clip == null)
            {
                Debug.LogWarning($"Clip \"{name}\" is not playing");
            }
            _playingClips.Remove(clip);
            clip.AudioSource.Stop();
            OnAudioStopped?.Invoke(new AudioStoppedEventArgs(clip.Name, clip.Audio, false));
        }

        public void Pause(string name)
        {
            Clip clip = _playingClips.Find((clip1 => clip1.Name == name));
            if (clip == null)
            {
                Debug.LogWarning($"Clip \"{name}\" is not playing");
            }
            clip.AudioSource.Pause();
        }

        public bool IsPlaying(string name)
        {
            return _playingClips.Any(clip => clip.Name == name);
        }
    }

    public class AudioStoppedEventArgs : EventArgs
    {
        public string Name { get; private set; }
        public AudioClip Clip { get; private set; }
        /// <summary>
        /// true if clip ended automatically. false if clip stopped by Stop() method.
        /// </summary>
        public bool ClipEnded { get; private set; }

        public AudioStoppedEventArgs(string name, AudioClip clip, bool clipEnded)
        {
            Name = name;
            Clip = clip;
            ClipEnded = clipEnded;
        }
    }

    [Serializable]
    public class Clip
    {
        public string Name = "";
        public string Group = "";
        public AudioClip Audio;
        public AudioMixerGroup MixerGroup;
        public bool Loop;
        public bool PlayOnAwake;
        [Range(0, 1)]
        public float Volume = 1;
        [Range(-3, 3)]
        public float Pitch = 1;
        
        [HideInInspector] public AudioSource AudioSource;
    }
}