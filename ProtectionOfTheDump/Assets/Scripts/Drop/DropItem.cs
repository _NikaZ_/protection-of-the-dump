using System;
using Core;
using UnityEngine;

namespace Drop
{
    public class DropItem : MonoBehaviour
    {
        private Camera _camera;
        [SerializeField] private SpriteRenderer _dropImage;

        public ICommand CollectDropCommand { get; set; }
        public Sprite DropPreview
        {
            get => _dropImage.sprite;
            set => _dropImage.sprite = value;
        }
        
        private void OnEnable()
        {
            _camera = Camera.main;
        }

        private void Update()
        {
            Vector3 target = _camera.ScreenToWorldPoint(new Vector3(-10, -10, 5));
            Vector3 direction = target - transform.position;
            transform.Translate(direction.normalized * Time.deltaTime * 10, Space.World);

            if (Vector3.Distance(target, transform.position) < 0.1f)
            {
                gameObject.SetActive(false);
                CollectDropCommand.Execute();
            }
        }
    }
}
