﻿using Abilities;
using Core;

namespace Drop
{
    public class AbilityDropCommand : ICommand
    {
        private readonly Ability _ability;

        public AbilityDropCommand(Ability ability)
        {
            _ability = ability;
        }
        
        public void Execute()
        {
            _ability.Count += 1;
        }
    }
}