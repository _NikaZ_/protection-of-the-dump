﻿using ResourceManager;
using UnityEngine;
using Utility;

namespace BuildingSystem
{
    public interface IBuilding
    {
        public string DisplayName { get; }
        public string Description { get; }
        
        public Sprite Preview { get; }
        void OnBuild(ReadonlyTransform origin, ResourceBank resourceBank);
    }
}