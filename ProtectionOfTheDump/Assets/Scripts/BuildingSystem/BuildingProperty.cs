﻿using UnityEngine;

namespace BuildingSystem
{
    public struct BuildingProperty
    {
        public string Name;
        public string Message;
    }
}