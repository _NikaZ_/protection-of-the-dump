﻿using System;
using JetBrains.Annotations;
using ResourceManager;
using SelectionSystem;
using UnityEngine;
using Utility;
using Zenject;

namespace BuildingSystem
{
    public class Platform : MonoBehaviour, ISelectable
    {
        [SerializeField] private Transform _buildingOrigin;
        
        public event Action<IBuilding> OnBuildingPlaced;
        public event Action<IRemovableBuilding> OnBuildingRemoved;
        
        private IBuilding _currentBuilding;
        public IBuilding CurrentBuilding => _currentBuilding;

        [Inject] private ResourceBank _resourceBank;

        public void PlaceBuilding([NotNull] IBuilding building)
        {
            _currentBuilding = building;
            building.OnBuild(new ReadonlyTransform(_buildingOrigin), _resourceBank);
            OnBuildingPlaced?.Invoke(_currentBuilding);
        }

        public bool RemoveBuilding()
        {
            if (!(_currentBuilding is IRemovableBuilding removableBuilding)) return false;
            removableBuilding.Remove();
            _currentBuilding = null;
            OnBuildingRemoved?.Invoke(removableBuilding);
            return true;

        }
    }
}