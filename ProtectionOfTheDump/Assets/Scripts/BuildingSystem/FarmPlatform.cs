﻿using System;
using Core;
using Farming;
using UnityEngine;

namespace BuildingSystem
{
    public class FarmPlatform : Platform
    {
        [SerializeField] private Farm _farmPrefab;
        [SerializeField] private GameplayBootstrapper _bootstrapper;

        private void OnEnable()
        {
            if (CurrentBuilding != null) return;
            var farm = Instantiate(_farmPrefab);
            farm.Init(_bootstrapper.Gameplay);
            PlaceBuilding(farm);
        }
    }
}