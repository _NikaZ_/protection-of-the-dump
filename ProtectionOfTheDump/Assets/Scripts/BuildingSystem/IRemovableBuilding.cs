﻿namespace BuildingSystem
{
    public interface IRemovableBuilding : IBuilding
    {
        void Remove();
        int RemoveCost { get; }
        int NonDiscountRemoveCost { get; }
    }
}