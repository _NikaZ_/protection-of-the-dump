using UnityEngine;

namespace Attributes
{
    [System.AttributeUsage(System.AttributeTargets.Field)]
    public class ScenePathAttribute : PropertyAttribute
    {
        private readonly bool _inBuildRequire;

        public ScenePathAttribute(bool inBuildRequire = true)
        {
            _inBuildRequire = inBuildRequire;
        }

        public bool InBuildRequire => _inBuildRequire;
    }
}
