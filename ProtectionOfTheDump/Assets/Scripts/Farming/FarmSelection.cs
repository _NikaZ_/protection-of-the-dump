﻿using System;
using Towers;
using UnityEngine;

namespace Farming
{
    public class FarmSelection
    {
        public event Action<Farm> OnFarmSelected;
        public event Action<Farm> OnFarmDeselected;
        
        private Farm _selectedFarm;

        public Farm SelectedFarm => _selectedFarm;

        public void SelectFarm(Farm farm)
        {
            _selectedFarm = farm;
            OnFarmSelected?.Invoke(farm);
        }

        public void Deselect()
        {
            if (_selectedFarm == null)
            {
                return;
            }
            OnFarmDeselected?.Invoke(_selectedFarm);
            _selectedFarm = null;
        }
    }
}