﻿using Towers;
using UnityEngine;

namespace Farming
{
    public class FarmModelSwitch : MonoBehaviour
    {
        [SerializeField] private Farm _farm;
        [SerializeField] private GameObject[] _towerModels;

        private GameObject _currentModel;

        private void Awake()
        {
            _farm.Upgraded += OnTowerUpgrade;
            foreach (var towerModel in _towerModels)
            {
                towerModel.SetActive(false);
            }
            SetTowerModel(_farm.Level);
        }

        private void OnTowerUpgrade(OnUpgradeEventArgs upgradeEventArgs)
        {
            SetTowerModel(upgradeEventArgs.Level);
        }

        private void SetTowerModel(int id)
        {
            if (_currentModel != null)
            {
                _currentModel.SetActive(false);
            }
        
            _currentModel = _towerModels.Length > id ? _towerModels[id] : null;
        
            if (_currentModel != null)
            {
                _currentModel.SetActive(true);
            }
        
        }
    }
}