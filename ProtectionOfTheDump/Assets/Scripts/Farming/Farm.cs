using System;
using System.Collections.Generic;
using System.Linq;
using BuildingSystem;
using Core;
using GameProperties;
using ResourceManager;
using SelectionSystem;
using StateMachineSystem;
using StateMachineSystem.States.GameplayStates;
using Towers;
using UnityEngine;
using Utility;
using Zenject;
using BuildingProperty = Towers.BuildingProperty;

namespace Farming
{
    public class Farm : MonoBehaviour, IUpgradable, IBuilding, IBuildingProperties
    {
        public event Action<OnUpgradeEventArgs> Upgraded;

        [SerializeField] private Sprite _preview;
        [SerializeField] private string _displayName;
        [SerializeField] private string _description;
        
        [SerializeField] private Level _level;
        [SerializeField] private int _coinPerWave = 10;
        [SerializeField] private int _coinPerWaveIncrease = 10;
        [SerializeField] private int[] _upgradeCost;
        private TowerPropertySprites _towerPropertySprites;

        private ResourceBank _resourceBank;
        private Gameplay _gameplay;

        public int CoinsPerWave => _coinPerWave + _coinPerWaveIncrease * _level.Value;

        public string DisplayName => _displayName;
        public string Description => _description;
        public Sprite Preview => _preview;


        [Inject]
        public void Construct(ResourceBank resourceBank)
        {
            _resourceBank = resourceBank;
        }

        public void Init(Gameplay gameplay)
        {
            _gameplay = gameplay;
            _gameplay.StateMachine.OnStateChanged += OnStateChanged;
            _towerPropertySprites = Resources.Load<TowerPropertySprites>("TowerPropertySprites");
        }

        private void OnStateChanged(IStateExitable state)
        {
            if (state is PrepareState)
            {
                _resourceBank.Coins += CoinPerWave;
            }
        }

        private int CoinPerWave => _coinPerWave + _coinPerWaveIncrease * _level.Value;

        private void OnDestroy()
        {
            _gameplay.StateMachine.OnStateChanged -= OnStateChanged;
        }
        public int Level => _level.Value;
        public int MaxLevel => _level.MaxValue;
        public bool Upgrade()
        {
            if (!CanUpgrade())
            {
                return false;
            }
            int upgradeCost = UpgradeCost;
            _level.Upgrade();
            _resourceBank.Coins -= upgradeCost;
            _resourceBank.DiscountManager.SetDiscount(0);
            Upgraded?.Invoke(new OnUpgradeEventArgs(_level.Value, upgradeCost, UpgradeCost, _level.MaxValue));
            return true;
        }

        public int GetNonDiscountUpgradeCost(int level)
        {
            if (_upgradeCost == null || _upgradeCost.Length == 0)
            {
                return 0;
            }

            if (level >= _upgradeCost.Length)
            {
                return _upgradeCost.Last();
            }

            return _upgradeCost[level];
        }
        public int NonDiscountUpgradeCost
        {
            get => GetNonDiscountUpgradeCost(Level);
        }
        
        public int GetUpgradeCost(int level)
        {
            return (int) Mathf.Round(GetNonDiscountUpgradeCost(level) * (1 - _resourceBank.DiscountManager.Discount));
        }
        
        public int UpgradeCost
        {
            get => GetUpgradeCost(Level);
        }


        public bool CanUpgrade()
        {
            return _resourceBank.Coins >= UpgradeCost && _level.Value < _level.MaxValue;
        }


        public void OnBuild(ReadonlyTransform origin, ResourceBank resourceBank)
        {
            origin.AddChild(transform);
            var transform1 = transform;
            transform1.localPosition = Vector3.zero;
            transform1.localRotation = Quaternion.identity;
            transform1.localScale = Vector3.one;
            _resourceBank = resourceBank;
        }

        public IEnumerable<BuildingProperty> GetBuildingProperties()
        {
            return new[]
            {
                new BuildingProperty() { Sprite = _towerPropertySprites.Coin, Message = $"{CoinPerWave}" }
            };
        }
    }
}
