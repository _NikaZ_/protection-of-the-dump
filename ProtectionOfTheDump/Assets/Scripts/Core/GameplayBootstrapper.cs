using System;
using Abilities;
using CameraUtility;
using Enemies.Spawn;
using Farming;
using Obstacles;
using ResourceManager;
using SelectionSystem;
using ShoppingSystem;
using StateMachineSystem.States.GameplayStates;
using StateMachineSystem.States.GameplayStates.Parameters;
using TimeManagement;
using Towers;
using UI.Controllers;
using UI.Views;
using UI.Views.AbilitiesViews;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Playables;
using Zenject;

namespace Core
{
    public class GameplayBootstrapper : MonoBehaviour
    {
        [Header("Gameplay settings")] [SerializeField]
        private GameConfig _gameConfig;

        [SerializeField] private EnemySpawner _enemySpawner;
        [SerializeField] private FarmManager _farmManager;
        [SerializeField] private Timer _prepareTimer;
        [SerializeField] private CameraMovement _cameraMovement;
        [SerializeField] private Selection _selection;
        [SerializeField] private AbilityConfig _abilityConfig;
        [SerializeField] private CheeseObstacleManager _cheeseObstacleManager;
        [SerializeField] private TowerBoostGiver _boostGiver;
        
        [Header("UI")] 
        [SerializeField] private TowerPlatformSettingsView _towerPlatformSettingsView;
        [SerializeField] private TowerShopView _towerShopView;
        [SerializeField] private TowerParametersView _towerParametersView;
        [SerializeField] private TimerView _prepareTimerView;
        [SerializeField] private StartRaidView _startRaidView;
        [SerializeField] private AbilitiesView _abilitiesView;
        [SerializeField] private PauseView _pauseView;
        [SerializeField] private WinPanelView _winPanelView;
        [SerializeField] private LoseView _loseView;

        [Header("Input")] 
        [SerializeField] private PlayerInput _playerInput;

        [Header("Other")] 
        [SerializeField] private PlayableDirector _mapOverviewTimeline;
        
        private TowerPlatformSettingsController _towerPlatformSettingsController;
        private TowerShopController _towerShopController;
        private TowerParametersController _towerParametersController;
        private TimerController _prepareTimerController;
        private StartRaidController _startRaidController;
        private PauseController _pauseController;
        private WinPanelController _winPanelController;
        private LoseController _loseController;

        private Selection _towerPlatformsSelection;
        private ResourceBank _resourceBank;

        private Shop _shop;
        private TowerShop _towerShop;

        private Gameplay _gameplay;
        private AbilitiesController _abilitiesController;

        public Gameplay Gameplay => _gameplay;

        [Inject]
        public void Construct(ResourceBank resourceBank, Selection towerPlatformsSelection)
        {
            _towerPlatformsSelection = towerPlatformsSelection;
            _resourceBank = resourceBank;
        }

        private void Awake()
        {
            _resourceBank.Init(_gameConfig.Coins, _gameConfig.MaxHealth);
            FarmSelection farmSelection = new FarmSelection();
            _towerPlatformSettingsController = new TowerPlatformSettingsController(_towerPlatformSettingsView, _selection);
            _shop = new Shop(_resourceBank);
            _towerShop = new TowerShop(_shop, _selection);
            _towerShopController = new TowerShopController(_towerShopView, _resourceBank, _towerShop);
            _towerParametersController = new TowerParametersController(_towerParametersView, _selection, _resourceBank);
            

            MapOverview mapOverview = new MapOverview(_mapOverviewTimeline);

            MapOverviewStateParameters mapOverviewStateParameters = new MapOverviewStateParameters(mapOverview, _cameraMovement, _selection);
            GameplayStatesRoot gameplayStatesRoot = new GameplayStatesRoot(_resourceBank, _enemySpawner, _prepareTimer, _gameConfig.PrepareTime, mapOverviewStateParameters);
            _gameplay = new Gameplay(gameplayStatesRoot);
            _gameplay.StartGame();
            _playerInput.onActionTriggered += OnActionTriggered;

            _prepareTimerController = new TimerController(_prepareTimerView, _prepareTimer, _gameplay);

            _startRaidController = new StartRaidController(_startRaidView, _gameplay);

            AbilityManager abilityManager = new AbilityManager(_resourceBank, _abilityConfig, _cheeseObstacleManager, _boostGiver);
            
            _enemySpawner.Init(abilityManager);
            _abilitiesController = new AbilitiesController(_abilitiesView, abilityManager);

            _pauseController = new PauseController(_pauseView, _gameplay);

            _winPanelController = new WinPanelController(_winPanelView, _gameplay, _resourceBank);
            _loseController = new LoseController(_loseView, _gameplay);
        }

        private void OnActionTriggered(InputAction.CallbackContext obj)
        {
            if (obj.action.name == "OpenClose" && obj.performed)
            {
                if (_gameplay.IsPaused)
                {
                    _gameplay.Resume();
                }
                else
                {
                    _gameplay.Pause();
                }
                
            }
        }

        private void OnDestroy()
        {
            _gameplay.Resume();
            _gameplay.OnDestroy();
        }
    }
}
