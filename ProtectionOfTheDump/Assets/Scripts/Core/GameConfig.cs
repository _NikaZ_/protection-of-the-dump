using UnityEngine;

namespace Core
{
    [CreateAssetMenu(fileName = "New Game Config", menuName = "Game Configs/Game Config")]
    public class GameConfig : ScriptableObject
    {
        [SerializeField] private float _prepareTime;
        [SerializeField] private int _maxHealth;
        [SerializeField] private int _coins;

        public float PrepareTime => _prepareTime;

        public int MaxHealth => _maxHealth;

        public int Coins => _coins;
    }
}
