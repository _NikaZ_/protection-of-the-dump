using System;
using StateMachineSystem;
using StateMachineSystem.States.MainMenuStates;

namespace Core
{
    public class MainMenu
    {
        
        private readonly StateMachine _stateMachine;
        private readonly MainMenuStatesRoot _statesRoot;

        public StateMachine StateMachine => _stateMachine;
        
        public MainMenu()
        {
            _stateMachine = new StateMachine();
            _statesRoot = new MainMenuStatesRoot();
        }

        public void OpenLevelSelection()
        {
            _stateMachine.ChangeState(_statesRoot.GetState<LevelSelectionState>());
        }

        public void OpenMenu()
        {
            _stateMachine.ChangeState(_statesRoot.GetState<MenuState>());
        }

        public void OpenOptions()
        {
            _stateMachine.ChangeState(_statesRoot.GetState<OptionsState>());
        }
    }
}
