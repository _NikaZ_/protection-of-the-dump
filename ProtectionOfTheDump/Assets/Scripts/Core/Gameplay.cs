﻿using System;
using Enemies.Spawn;
using StateMachineSystem;
using StateMachineSystem.States.GameplayStates;

namespace Core
{
    public class Gameplay
    {
        public event Action<bool> OnPauseStateChanged;
        
        private readonly GameplayStatesRoot _statesRoot;
        private readonly StateMachine _stateMachine;

        private IStateExitable _beforePauseState;

        public StateMachine StateMachine => _stateMachine;

        public bool IsPaused { get; private set; }
        public Gameplay(GameplayStatesRoot statesRoot)
        {
            _statesRoot = statesRoot;
            _stateMachine = new StateMachine();
        }

        public void StartGame()
        {
           _stateMachine.ChangeState(_statesRoot.GetState<MapOverviewState>());
        }

        public void Pause()
        {
            if (IsPaused || !CanPause())
            {
                return;
            }
            _beforePauseState = _stateMachine.CurrentState;
            _stateMachine.ChangeState(_statesRoot.GetState<PauseState>());
            IsPaused = true;
            OnPauseStateChanged?.Invoke(IsPaused);
        }

        public void Resume()
        {
            if (!IsPaused)
            {
                return;
            }
            _stateMachine.ChangeState(_beforePauseState);
            IsPaused = false;
            OnPauseStateChanged?.Invoke(IsPaused);
        }

        public void StartRaid()
        {
            if (!CanStartRaid())
            {
                return;
            }
            _stateMachine.ChangeState(_statesRoot.GetState<RaidState>());
        }

        public bool CanPause()
        {
            return _stateMachine.CurrentState is FirstPrepareState || _stateMachine.CurrentState is PrepareState ||
                   _stateMachine.CurrentState is RaidState;
        }
        
        public bool CanStartRaid()
        {
            return _stateMachine.CurrentState is FirstPrepareState || _stateMachine.CurrentState is PrepareState;
        }

        public void OnDestroy()
        {
            _stateMachine.ChangeState(null);
        }
    }
}