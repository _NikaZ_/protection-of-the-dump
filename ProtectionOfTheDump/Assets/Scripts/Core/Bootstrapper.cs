using System;
using AudioManagement;
using Maps;
using Options;
using SaveSystem;
using UnityEngine;
using UnityEngine.Audio;

namespace Core
{

    public class Bootstrapper : MonoBehaviour
    {
        private Game _game;
        [SerializeField] private MapManager _mapManager;
        [SerializeField] private SaveManager _saveManager;
        [SerializeField] private AudioMixer _audioMixer;
        [SerializeField] private AudioManager _audioManager;
        

        private static Bootstrapper _instance;
        public Game Game => _game;
        public SaveManager SaveManager => _saveManager;
        public static Bootstrapper Instance => _instance;

        public AudioManager AudioManager => _audioManager;
        public OptionsManager OptionsManager { get; private set; }

        private void Awake()
        {
            if (_instance == null)
            {
                _instance = this;
                DontDestroyOnLoad(this);
            }
            else
            {
                Destroy(gameObject);
                return;
            }
            _saveManager.Init();
            _audioManager.Init();
            _game = new Game(_mapManager);
            OptionsManager = new OptionsManager(_audioMixer);
        }

        private void Start()
        {
            OptionsManager.UpdateMixer();
        }

        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
        private static void Bootstrap()
        {
            GameObject bootstrapper = Instantiate(Resources.Load<GameObject>("Bootstrapper"));
            if (bootstrapper == null)
            {
                throw new InvalidOperationException("Failed to load Bootstrapper.prefab");
            }
        }
    }
}
