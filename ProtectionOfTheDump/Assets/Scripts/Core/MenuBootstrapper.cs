﻿using System;
using Options;
using UI.Controllers;
using UI.Views;
using UnityEngine;

namespace Core
{
    public class MenuBootstrapper : MonoBehaviour
    {
        [SerializeField] private MenuView _menuView;
        [SerializeField] private LevelSelectionView _levelSelectionView;
        [SerializeField] private OptionsView _optionsView;
        
        private MenuController _menuController;
        private LevelSelectionController _levelSelectionController;
        private OptionsController _optionsController;
        private MainMenu _mainMenu;
        private Game _game;

        private void Awake()
        {
            _game = Bootstrapper.Instance.Game;
            
            _mainMenu = new MainMenu();
            
            _menuController = new MenuController(_game, _mainMenu, _menuView);
            _levelSelectionController = new LevelSelectionController(_levelSelectionView, _mainMenu, _game);
            _optionsController = new OptionsController(Bootstrapper.Instance.OptionsManager, _optionsView, _mainMenu);
            
            _mainMenu.OpenMenu();
        }
    }
}