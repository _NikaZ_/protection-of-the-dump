using Maps;
using StateMachineSystem;
using StateMachineSystem.States.GameStates;
using UnityEngine;

namespace Core
{
    public class Game
    {
        private readonly MapManager _mapManager;
        private StateMachine _stateMachine;
        private GameStatesRoot _statesRoot;

        public int LevelsCount => _mapManager.LevelsCount;
        public int CurrentLevel => _mapManager.CurrentLevel;

        public Game(MapManager mapManager)
        {
            _mapManager = mapManager;
            Debug.Log("Game initialization");
            _stateMachine = new StateMachine();
            _statesRoot = new GameStatesRoot(mapManager);
            _stateMachine.ChangeState(_statesRoot.GetState<MainMenuState>());
        }

        public void LoadMap(int id)
        {
            _mapManager.CurrentLevel = id;
            _stateMachine.ChangeState(_statesRoot.GetState<GameplayState>());
        }
        
        public void LoadMap(int id, bool forceLoad)
        {
            _mapManager.CurrentLevel = id;
            _stateMachine.ChangeState(_statesRoot.GetState<GameplayState>());
        }

        public void LoadMainMenu()
        {
            _stateMachine.ChangeState(_statesRoot.GetState<MainMenuState>());
        }

        public void Quit()
        {
            Bootstrapper.Instance.SaveManager.Save();
            Application.Quit();
        }
    }
}
