﻿using System.Collections.Generic;
using UnityEngine;

namespace ResourceManager
{
    public class DataLoader
    {
        public T[] LoadAllData<T>(string path) where T : Object
        {
            return Resources.LoadAll<T>(path);
        }

        public T LoadData<T>(string path) where T : Object
        {
            return Resources.Load<T>(path);
        }
    }
}