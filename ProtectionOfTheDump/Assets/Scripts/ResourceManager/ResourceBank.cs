﻿using System;
using GameProperties;
using ShoppingSystem;
using Towers;
using UnityEngine;

namespace ResourceManager
{
    public class ResourceBank
    {
        public event Action<int> OnCoinsCountChanged;

        private readonly DataLoader _dataLoader;
        private Tower[] _towers;

        private int _coins;
        private Health _playerLives;

        public DiscountManager DiscountManager { get; }

        public int Coins
        {
            get => _coins;
            set
            {
                if (value < 0)
                {
                    throw new ArgumentOutOfRangeException();
                }
                _coins = value;
                OnCoinsCountChanged?.Invoke(value);
            }
        }
        
        public void Init(int coins, int health)
        {
            Coins = coins;
            _playerLives = new Health(health);
        }
        
        public Health PlayerLives
        {
            get => _playerLives;
        }

        public Tower[] Towers
        {
            get
            {
                var towers = new Tower[_towers.Length];
                _towers.CopyTo(towers, 0);
                return towers;
            }
        }

        public ResourceBank()
        {
            Debug.Log("Resource bank initialization");
            _dataLoader = new DataLoader();
            _towers = _dataLoader.LoadAllData<Tower>("Towers/");
            DiscountManager = new DiscountManager();
        }
    }
}