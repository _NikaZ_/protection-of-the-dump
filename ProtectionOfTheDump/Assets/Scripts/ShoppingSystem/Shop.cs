﻿using System;
using ResourceManager;
using UnityEngine;

namespace ShoppingSystem
{
    public class Shop
    {
        public event Action<IProduct> OnBought;

        private readonly ResourceBank _resourceBank;
        public float CostMultiplier { get; set; } = 1;

        public Shop(ResourceBank resourceBank)
        {
            _resourceBank = resourceBank;
        }

        public bool Buy(IProduct product)
        {
            var cost = GetProductCost(product);
            if (_resourceBank.Coins - cost < 0) return false;
            _resourceBank.Coins -= cost;
            OnBought?.Invoke(product);
            return true;

        }

        public int GetProductCost(IProduct product)
        {
            return (int)Mathf.Round(product.Cost * CostMultiplier);
        }
    }
}