﻿using BuildingSystem;
using ResourceManager;
using SelectionSystem;
using Towers;
using UnityEngine;

namespace ShoppingSystem
{
    public class TowerShop
    {
        private readonly Shop _shop;
        private readonly Selection _selection;

        public Shop Shop => _shop;

        public TowerShop(Shop shop, Selection selection)
        {
            _shop = shop;
            _selection = selection;
        }

        public bool BuyTower(Tower tower)
        {
            if (_selection.SelectedObject is Platform platform && _shop.Buy(tower.TowerConfig))
            {
                platform.PlaceBuilding(Object.Instantiate(tower));
                return true;
            }

            return false;
        }
    }
}