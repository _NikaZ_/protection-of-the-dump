﻿namespace ShoppingSystem
{
    public interface IProduct
    {
        int Cost { get; }
    }
}