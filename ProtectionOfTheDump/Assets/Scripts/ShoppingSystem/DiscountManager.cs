using System;

namespace ShoppingSystem
{
    public class DiscountManager
    {
        public event Action<float> OnDiscountSet;
        
        private float _discount;

        public float Discount => _discount;
        public bool HasDiscount => _discount > 0;
        
        public void SetDiscount(float discount)
        {
            if (discount < 0 || discount > 1)
            {
                return;
            }
            
            _discount = discount;
            OnDiscountSet?.Invoke(discount);
        }
    }
}
