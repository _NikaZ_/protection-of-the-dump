﻿using System;

namespace GameProperties
{
    public class Health
    {
        public event Action<int> OnHealthChanged;

        private int _maxValue;
        private int _value;

        public int Value
        {
            get => _value;
            set
            {
                _value = value > _maxValue ? _maxValue : value;
                OnHealthChanged?.Invoke(_value);
            }
        }

        public int MaxValue
        {
            get => _maxValue;
            set
            {
                _maxValue = value;
                Value = value > _maxValue ? _maxValue : _value;
            }
        }

        public Health(int maxValue, int value)
        {
            _maxValue = maxValue;
            _value = _value > maxValue ? maxValue : value;
        }

        public Health(int maxValue)
        {
            _maxValue = maxValue;
            _value = maxValue;
        }
    }
}