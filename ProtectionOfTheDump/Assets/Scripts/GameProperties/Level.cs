﻿using System;
using UnityEngine;

namespace GameProperties
{
    [Serializable]
    public class Level
    {
        public event Action<int> OnLevelChanged;
        
        private int _level;
        [SerializeField] private int _maxLevel;

        public int Value => _level;

        public int MaxValue => _maxLevel;

        public bool Upgrade(int levels = 1)
        {
            if (_level + levels <= _maxLevel)
            {
                _level++;
                OnLevelChanged?.Invoke(_level);
                return true;
            }

            return false;
        }

        public Level(int maxLevel)
        {
            _maxLevel = maxLevel;
        }
    }
}