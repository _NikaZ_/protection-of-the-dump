﻿using System;
using Towers;

namespace GameProperties
{
    public interface IUpgradable
    {
        public event Action<OnUpgradeEventArgs> Upgraded;
        
        public int Level { get; }
        public int MaxLevel { get; }
        public bool Upgrade();
        public bool CanUpgrade();

        public int UpgradeCost { get; }
        public int NonDiscountUpgradeCost { get; }
    }
}